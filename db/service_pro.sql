-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 19, 2017 at 11:51 AM
-- Server version: 5.5.44-MariaDB
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `service_pro`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(255) NOT NULL,
  `q_id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `service_id` int(255) NOT NULL,
  `q_order` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1908 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `q_id`, `name`, `service_id`, `q_order`) VALUES
(1, 5, 'Morning', 5, 0),
(2, 5, 'Evening', 5, 0),
(3, 6, 'Wedding', 7, 0),
(4, 6, 'Birthday', 7, 0),
(5, 6, 'Engagement', 7, 0),
(6, 6, 'Reception', 7, 0),
(7, 7, 'Evening', 7, 0),
(8, 7, 'Late night', 7, 0),
(9, 8, 'below 20 years', 7, 0),
(10, 8, '20 to 40 years', 7, 0),
(11, 8, '40 to 60 years', 7, 0),
(12, 8, 'all type of guests', 7, 0),
(13, 9, 'Flat', 6, 0),
(14, 9, 'House', 6, 0),
(15, 11, 'Furniture and painting', 6, 0),
(16, 11, 'Only furniture', 6, 0),
(17, 11, 'Only kitchen', 6, 0),
(18, 11, 'Wall painting', 6, 0),
(19, 11, 'Entire home', 6, 0),
(20, 13, 'Premarital photography', 8, 0),
(21, 13, 'Only marriage day', 8, 0),
(22, 13, 'Marriage and reception', 8, 0),
(23, 13, 'premarital marage day and reception all ', 8, 0),
(24, 14, 'Makeup only', 9, 0),
(25, 14, 'Makeup and hair style', 9, 0),
(26, 14, 'Makeup hairstyle and hair spa', 9, 0),
(27, 15, 'I want to move or install a new shower or bath', 11, 0),
(28, 15, 'I want to move or install a new shower or bath', 11, 0),
(29, 15, 'Cabinets / Vanity *', 11, 0),
(30, 16, 'Shower / tub combo', 12, 0),
(31, 16, 'Soaking', 12, 0),
(32, 16, 'Whirlpool / jetted tub', 12, 0),
(33, 16, 'Free-standing (clawfoot, legged)', 12, 0),
(34, 17, 'Built-In (alcove, often tub / shower combo)', 13, 0),
(35, 17, 'Free-standing (clawfoot, legged)', 13, 0),
(36, 17, 'Soaking (Garden, Roman, etc)', 13, 0),
(37, 17, 'Specialty (Whirlpool, special needs)', 13, 0),
(38, 18, 'Solid Colors', 13, 0),
(39, 18, 'Granite Look', 13, 0),
(40, 18, 'Marble Look', 13, 0),
(41, 18, 'Tile Look', 13, 0),
(42, 19, 'Yes', 13, 0),
(43, 19, 'No', 13, 0),
(44, 19, 'Would like to learn more', 13, 0),
(45, 20, 'Soap dishes', 13, 0),
(46, 20, 'Grab bars', 13, 0),
(47, 20, 'Single level faucets', 13, 0),
(48, 21, '', 14, 0),
(49, 22, 'Free-standing shower', 14, 0),
(50, 22, 'Steam shower', 14, 0),
(51, 22, 'Shower / tub combo', 14, 0),
(52, 22, 'Dual shower', 14, 0),
(53, 23, 'Shower', 14, 0),
(54, 23, 'Plumbing fixtures', 14, 0),
(55, 23, 'Tile', 14, 0),
(56, 23, 'Is this project part of a larger remodel?', 14, 0),
(57, 23, 'What kind of location is this?', 14, 0),
(58, 24, 'Home/Residence', 14, 0),
(59, 24, 'Business', 14, 0),
(60, 25, '', 15, 0),
(61, 25, '', 15, 0),
(62, 26, 'New structure', 15, 0),
(63, 26, 'Interior remodel of a few rooms', 15, 0),
(64, 26, 'Complete remodel (with or without addition)', 15, 0),
(65, 27, 'House', 15, 0),
(66, 27, 'Residential outbuilding (garage, etc)', 15, 0),
(67, 27, 'Retail store', 15, 0),
(68, 28, 'New structure', 15, 0),
(69, 28, 'Interior remodel of a few rooms', 15, 0),
(70, 28, 'Complete remodel (with or without addition)', 15, 0),
(71, 28, 'Addition (one story)', 15, 0),
(72, 28, 'Addition (two or more stories)', 15, 0),
(73, 29, 'House', 15, 0),
(74, 29, 'Residential outbuilding (garage, etc)', 15, 0),
(75, 29, 'Retail store', 15, 0),
(76, 29, 'Commercial office', 15, 0),
(77, 29, 'Restaurant', 15, 0),
(78, 29, 'Condominium, apartment or town house', 15, 0),
(79, 30, 'Design drawings', 15, 0),
(80, 30, 'Project evaluation', 15, 0),
(81, 30, 'Review construction', 15, 0),
(82, 30, 'Permit assistance', 15, 0),
(83, 30, 'Technical construction documents', 15, 0),
(84, 30, 'Assist with contractor selection', 15, 0),
(85, 30, 'Would like recommendation', 15, 0),
(86, 31, '', 16, 0),
(87, 32, 'Contemporary bathtub', 16, 0),
(88, 32, 'Antique bathtub', 16, 0),
(89, 32, 'Shower stall', 16, 0),
(90, 32, 'Shower Pan', 16, 0),
(91, 32, 'Kitchen appliance(s)', 16, 0),
(92, 32, 'Whirlpool tub', 16, 0),
(93, 32, 'Bathroom sink', 16, 0),
(94, 32, 'Kitchen sink', 16, 0),
(95, 32, 'Wall tile', 16, 0),
(96, 32, 'Countertop tile', 16, 0),
(97, 32, ' Countertop', 16, 0),
(98, 32, 'Other', 16, 0),
(99, 33, 'Repair cracks and chips', 16, 0),
(100, 33, 'Resurface in new color', 16, 0),
(101, 33, 'Resurface in same color', 16, 0),
(102, 33, 'Match to other features in room', 16, 0),
(103, 33, 'Other', 16, 0),
(104, 34, '', 18, 0),
(105, 35, '', 20, 0),
(106, 36, 'Install new pre-made cabinets', 20, 0),
(107, 36, 'Install new custom cabinets', 20, 0),
(108, 36, 'Reface existing cabinets', 20, 0),
(109, 36, 'Repair existing cabinets', 20, 0),
(110, 36, 'Refinish existing cabinets', 20, 0),
(111, 37, 'Yes', 20, 0),
(112, 37, 'No', 20, 0),
(113, 38, 'Kitchen', 20, 0),
(114, 38, 'Closet', 20, 0),
(115, 38, 'Garage', 20, 0),
(116, 38, 'Bathroom', 20, 0),
(117, 38, 'Basement', 20, 0),
(118, 38, 'Other', 20, 0),
(119, 39, '', 21, 0),
(120, 40, 'Kitchen', 21, 0),
(121, 40, 'Bedroom(s)', 21, 0),
(122, 40, 'Laundry/Utility room', 21, 0),
(123, 40, 'Bathroom(s)', 21, 0),
(124, 40, 'Family room', 21, 0),
(125, 40, 'Closet(s)', 21, 0),
(126, 40, 'Other', 21, 0),
(127, 41, 'Painted wood', 21, 0),
(128, 41, 'Laminate', 21, 0),
(129, 41, 'Stained wood', 21, 0),
(130, 41, 'Don''t Know', 21, 0),
(131, 41, 'Other', 21, 0),
(132, 42, 'Add extra cabinet(s)', 21, 0),
(133, 42, 'Eating counter', 21, 0),
(134, 42, 'Appliance panels', 21, 0),
(135, 42, 'Pull-out shelves', 21, 0),
(136, 42, 'New countertops', 21, 0),
(137, 42, 'Crown molding', 21, 0),
(138, 42, 'Wet bar', 21, 0),
(139, 42, 'Lazy Susan', 21, 0),
(140, 42, 'Adjustable shelves', 21, 0),
(141, 42, 'Other', 21, 0),
(142, 43, '', 22, 0),
(143, 44, 'Install new pre-made cabinets', 22, 0),
(144, 44, 'Install new custom cabinets', 22, 0),
(145, 44, 'Reface existing cabinets', 22, 0),
(146, 44, 'Repair existing cabinets', 22, 0),
(147, 44, 'Refinish existing cabinets', 22, 0),
(148, 45, 'Yes', 22, 0),
(149, 45, 'No', 22, 0),
(150, 46, 'Kitchen', 22, 0),
(151, 46, 'Closet', 22, 0),
(152, 46, 'Garage', 22, 0),
(153, 46, 'Bathroom', 22, 0),
(154, 46, 'Basement', 22, 0),
(155, 46, 'Other', 22, 0),
(156, 47, '', 23, 0),
(157, 48, 'Entertainment center', 23, 0),
(158, 48, 'Custom storage', 23, 0),
(159, 48, 'Wall Unit', 23, 0),
(160, 48, 'Bookshelves', 23, 0),
(161, 48, 'Seating', 23, 0),
(162, 49, 'Kitchen', 23, 0),
(163, 49, 'Family room', 23, 0),
(164, 49, 'Entertainment center', 23, 0),
(165, 49, 'Bathroom', 23, 0),
(166, 49, 'Home office', 23, 0),
(167, 49, 'room Bedroom', 23, 0),
(168, 49, 'Other Location', 23, 0),
(169, 50, 'Adjustable shelves', 23, 0),
(170, 50, 'Crown molding', 23, 0),
(171, 50, 'Pull-out shelves', 23, 0),
(172, 50, 'Pantry cabinet', 23, 0),
(173, 50, 'Island', 23, 0),
(174, 50, 'Lazy Susan', 23, 0),
(175, 50, 'Eating counter', 23, 0),
(176, 50, 'Desk/computer area', 23, 0),
(177, 50, 'Other', 23, 0),
(178, 51, '', 24, 0),
(179, 52, 'Kitchen', 24, 0),
(180, 52, 'Bedroom(s)', 24, 0),
(181, 52, 'Dining room', 24, 0),
(182, 52, 'Bathroom(s)', 24, 0),
(183, 52, 'Garage/work area', 24, 0),
(184, 52, 'Hallway', 24, 0),
(185, 52, 'Family room', 24, 0),
(186, 52, 'Living room', 24, 0),
(187, 52, 'Laundry room', 24, 0),
(188, 52, 'Other', 24, 0),
(189, 53, 'Paint', 24, 0),
(190, 53, 'Change color of stain', 24, 0),
(191, 53, 'Lighten as much as possible', 24, 0),
(192, 53, 'Make new and old cabinets match', 24, 0),
(193, 53, 'Restore existing color', 24, 0),
(194, 53, 'Change color of paint', 24, 0),
(195, 53, 'Strip paint and stain', 24, 0),
(196, 53, 'Distressed look', 24, 0),
(197, 53, 'Apply special finish', 24, 0),
(198, 53, 'Not sure, need advice', 24, 0),
(199, 54, 'Painted wood', 24, 0),
(200, 54, 'Laminate', 24, 0),
(201, 54, 'Stained wood', 24, 0),
(202, 54, 'Don''t Know', 24, 0),
(203, 54, 'Other', 24, 0),
(204, 55, '', 25, 0),
(205, 56, 'Shelves are sagging or broken', 25, 0),
(206, 56, 'Drawer has a broken bottom', 25, 0),
(207, 56, 'Drawer has a broken front', 25, 0),
(208, 56, 'Hinge is broken', 25, 0),
(209, 56, 'Drawers do not glide smoothly', 25, 0),
(210, 56, 'Drawer has a broken guide', 25, 0),
(211, 56, 'Door is warped', 25, 0),
(212, 57, 'Painted wood', 25, 0),
(213, 57, 'Laminate', 25, 0),
(214, 57, 'Don''t Know', 25, 0),
(215, 57, 'Stained wood', 25, 0),
(216, 57, 'Steel', 25, 0),
(217, 58, '', 26, 0),
(218, 59, 'Install new pre-made cabinets', 26, 0),
(219, 59, 'Install new custom cabinets', 26, 0),
(220, 59, 'Reface existing cabinets', 26, 0),
(221, 59, 'Repair existing cabinets', 26, 0),
(222, 59, 'Refinish existing cabinets', 26, 0),
(223, 60, 'Yes', 26, 0),
(224, 60, 'No', 26, 0),
(225, 61, 'Kitchen', 26, 0),
(226, 61, 'Closet', 26, 0),
(227, 61, 'Garage', 26, 0),
(228, 61, 'Bathroom', 26, 0),
(229, 61, 'Basement', 26, 0),
(230, 61, 'Other', 26, 0),
(231, 62, '', 27, 0),
(232, 63, 'Install or replace countertop', 27, 0),
(233, 63, 'Repair existing countertop', 27, 0),
(234, 64, 'Yes', 27, 0),
(235, 64, 'No', 27, 0),
(236, 65, 'Backsplash', 27, 0),
(237, 65, 'Cooktop cutouts', 27, 0),
(238, 65, 'Radius counter', 27, 0),
(239, 65, 'Sink cutouts', 27, 0),
(240, 65, 'Custom shape', 27, 0),
(241, 66, '', 28, 0),
(242, 67, 'Install or replace countertop', 28, 0),
(243, 67, 'Repair existing countertop', 28, 0),
(244, 68, 'Yes', 28, 0),
(245, 68, 'No', 28, 0),
(246, 69, 'Granite', 28, 0),
(247, 69, 'Quartz', 28, 0),
(248, 69, 'Granite Marble', 28, 0),
(249, 69, 'Want recommendation', 28, 0),
(250, 70, 'Backsplash', 28, 0),
(251, 70, 'Custom shape', 28, 0),
(252, 70, 'New sink', 28, 0),
(253, 70, 'Cooktop cutouts', 28, 0),
(254, 70, 'Radius corners', 28, 0),
(255, 71, '', 29, 0),
(256, 72, 'Contemporary bathtub', 29, 0),
(257, 72, 'Antique bathtub', 29, 0),
(258, 72, 'Shower stall', 29, 0),
(259, 72, 'Shower Pan', 29, 0),
(260, 72, 'Kitchen appliance(s)', 29, 0),
(261, 72, 'Bathroom sink', 29, 0),
(262, 72, 'Whirlpool tub', 29, 0),
(263, 72, 'Kitchen sink', 29, 0),
(264, 72, 'Wall tile', 29, 0),
(265, 72, 'Countertop tile', 29, 0),
(266, 72, 'Countertop', 29, 0),
(267, 72, 'tub Other', 29, 0),
(268, 73, 'Repair cracks and chips', 29, 0),
(269, 73, 'Resurface in new color', 29, 0),
(270, 73, 'Resurface in same color', 29, 0),
(271, 73, 'Match to other features in room', 29, 0),
(272, 73, 'Other', 29, 0),
(273, 74, '', 30, 0),
(274, 75, 'Install or replace countertop', 30, 0),
(275, 75, 'Repair existing countertop', 30, 0),
(276, 76, 'Yes', 30, 0),
(277, 76, 'No', 30, 0),
(278, 77, 'Solid surfacing (e.g. Corian)', 30, 0),
(279, 77, 'Wood (e.g. butcher block)', 30, 0),
(280, 77, 'Would like recommendation', 30, 0),
(281, 77, 'Stainless Steel', 30, 0),
(282, 77, 'Concrete', 30, 0),
(283, 78, 'Backsplash', 30, 0),
(284, 78, 'Inset designs', 30, 0),
(285, 78, 'Cooktop cutouts', 30, 0),
(286, 78, 'Integral Sink', 30, 0),
(287, 78, 'Sink cutouts', 30, 0),
(288, 80, 'Replace existing tile', 31, 0),
(289, 80, 'Repair existing tile', 31, 0),
(290, 80, 'Replace existing non-tile surface', 31, 0),
(291, 80, 'Tile for new construction', 31, 0),
(292, 81, 'Yes', 31, 0),
(293, 81, 'No', 31, 0),
(294, 82, 'Floor', 31, 0),
(295, 82, 'Tub/Shower', 31, 0),
(296, 82, 'Backsplash', 31, 0),
(297, 82, 'Floor Wall', 31, 0),
(298, 82, 'Countertop', 31, 0),
(299, 83, 'Glazed ceramic tile', 31, 0),
(300, 83, 'Porcelain tile', 31, 0),
(301, 83, 'Ceramic mosaic tile', 31, 0),
(302, 83, 'Special or custom tile', 31, 0),
(303, 83, 'Want recommendation', 31, 0),
(304, 84, '', 32, 0),
(305, 85, 'Replace existing tile', 32, 0),
(306, 85, 'Repair existing tile', 32, 0),
(307, 85, 'Replace existing non-tile surface', 32, 0),
(308, 85, 'Tile for new construction', 32, 0),
(309, 86, 'Yes', 32, 0),
(310, 86, 'No', 32, 0),
(311, 87, 'Marble', 32, 0),
(312, 87, 'Sandstone', 32, 0),
(313, 87, 'Slate', 32, 0),
(314, 87, 'Soapstone', 32, 0),
(315, 87, 'Granite', 32, 0),
(316, 87, 'Limestone', 32, 0),
(317, 87, 'Quartz', 32, 0),
(318, 87, 'Travertine', 32, 0),
(319, 88, 'Floor', 32, 0),
(320, 88, 'Stairs', 32, 0),
(321, 88, 'Patio', 32, 0),
(322, 88, 'Swimming Pool', 32, 0),
(323, 88, 'Countertop', 32, 0),
(324, 88, 'Walls', 32, 0),
(325, 88, 'Fireplace', 32, 0),
(326, 89, 'Yes', 32, 0),
(327, 89, 'No', 32, 0),
(328, 90, '', 33, 0),
(329, 91, 'Home/Residence', 33, 0),
(330, 91, 'Business', 33, 0),
(331, 92, 'Yes', 33, 0),
(332, 92, 'No', 33, 0),
(333, 93, 'Repair or replace an old switch, fixture or outlet', 33, 0),
(334, 93, 'Troubleshoot an electrical problem', 33, 0),
(335, 93, 'Install new switches, fixtures or outlets', 33, 0),
(336, 93, 'Move switches, fixtures or outlets', 33, 0),
(337, 94, 'Ceiling fan or chandelier', 33, 0),
(338, 94, 'Interior lights', 33, 0),
(339, 94, 'Exterior lights', 33, 0),
(340, 94, 'Bath or attic exhaust fan', 33, 0),
(341, 94, 'chandelier Switches', 33, 0),
(342, 94, 'Outlets', 33, 0),
(343, 94, 'Other fixtures', 33, 0),
(344, 95, '', 34, 0),
(345, 96, 'Home/Residence', 34, 0),
(346, 96, 'Business', 34, 0),
(347, 97, 'Complete wiring for addition or remodel', 34, 0),
(348, 97, 'Electrical panel upgrade only', 34, 0),
(349, 97, 'Update or add a few fixtures, outlets or switches', 34, 0),
(350, 98, 'Kitchen', 34, 0),
(351, 98, 'Bedroom(s)', 34, 0),
(352, 98, 'Office', 34, 0),
(353, 98, 'Garage or Basement', 34, 0),
(354, 98, 'Bathroom(s)', 34, 0),
(355, 98, 'Living, Family, or Dining rooms', 34, 0),
(356, 98, 'Laundry or utility room', 34, 0),
(357, 98, 'Patio or outdoors', 34, 0),
(358, 98, 'Other', 34, 0),
(359, 99, 'Switches (standard)', 34, 0),
(360, 99, 'Floor outlets', 34, 0),
(361, 99, 'Surge protection', 34, 0),
(362, 99, 'Telephone/modem line', 34, 0),
(363, 99, 'Specialty lighting', 34, 0),
(364, 99, 'Attic/Whole house fans', 34, 0),
(365, 99, 'Wall outlets', 34, 0),
(366, 99, 'Dedicated circuits', 34, 0),
(367, 99, 'Doorbell', 34, 0),
(368, 99, 'Lightning protection', 34, 0),
(369, 99, 'Ceiling fixtures/fans', 34, 0),
(370, 99, 'fans Spa', 34, 0),
(371, 99, 'Other', 34, 0),
(372, 101, 'To improve/add lighting in existing home', 35, 0),
(373, 101, 'Remodel/Addition', 35, 0),
(374, 101, 'Add lighting outdoors', 35, 0),
(375, 101, 'New home construction', 35, 0),
(376, 102, 'Why do you need a lighting designer?', 35, 0),
(377, 102, 'Specific lighting needs (e.g. landscape, art or sculpture)', 35, 0),
(378, 102, 'Assist with fixture type selection', 35, 0),
(379, 102, 'Other', 35, 0),
(380, 103, '', 36, 0),
(381, 104, 'Home/Residence', 36, 0),
(382, 104, 'Business', 36, 0),
(383, 105, 'Yes', 36, 0),
(384, 105, 'No', 36, 0),
(385, 106, 'Yes', 36, 0),
(386, 106, 'No', 36, 0),
(387, 106, 'I don''t know', 36, 0),
(388, 106, 'I haven''t determined the installation site yet', 36, 0),
(389, 107, '', 37, 0),
(390, 108, 'Electrical panel upgrade only', 37, 0),
(391, 108, 'Update or add a few fixtures, outlets or switches', 37, 0),
(392, 108, 'Complete wiring for addition or remodel', 37, 0),
(393, 109, 'Home/Residence', 37, 0),
(394, 109, 'Business', 37, 0),
(395, 110, '', 38, 0),
(396, 111, 'New install - addition or new construction', 38, 0),
(397, 111, 'New install - existing space', 38, 0),
(398, 111, 'Service existing system', 38, 0),
(399, 111, 'Augment existing system', 38, 0),
(400, 112, 'Security system', 38, 0),
(401, 112, 'Home theater', 38, 0),
(402, 112, 'Door intercom', 38, 0),
(403, 112, 'Lighting', 38, 0),
(404, 112, 'Phone system', 38, 0),
(405, 112, 'Heating/cooling', 38, 0),
(406, 112, 'Video cameras', 38, 0),
(407, 112, 'Motorized drapes', 38, 0),
(408, 112, 'Sprinklers', 38, 0),
(409, 112, 'Pool/Spa', 38, 0),
(410, 113, '', 39, 0),
(411, 114, 'Replace existing tile', 39, 0),
(412, 114, 'Repair existing tile', 39, 0),
(413, 114, 'Replace existing non-tile surface', 39, 0),
(414, 114, 'Tile for new construction', 39, 0),
(415, 115, 'Yes', 39, 0),
(416, 115, 'No', 39, 0),
(417, 116, 'Floor', 39, 0),
(418, 116, 'Tub/Shower', 39, 0),
(419, 116, 'Backsplash', 39, 0),
(420, 116, 'Wall', 39, 0),
(421, 116, 'Countertop', 39, 0),
(422, 117, 'Glazed ceramic tile', 39, 0),
(423, 117, 'Porcelain tile', 39, 0),
(424, 117, 'Ceramic mosaic tile', 39, 0),
(425, 117, 'Special or custom tile', 39, 0),
(426, 117, 'Want recommendation', 39, 0),
(427, 118, '', 40, 0),
(428, 119, 'Quotes - I''m ready for competing quotes', 40, 0),
(429, 119, 'Consultation - I want to explore options with Wood Flooring Pros', 40, 0),
(430, 119, '', 40, 0),
(431, 120, 'Install wood flooring', 40, 0),
(432, 120, 'Refinish existing wood flooring', 40, 0),
(433, 120, 'Repair existing wood flooring', 40, 0),
(434, 121, 'Natural wood', 40, 0),
(435, 121, 'Want recommendation', 40, 0),
(436, 121, 'Wood laminate', 40, 0),
(437, 122, 'Yes', 40, 0),
(438, 122, 'No', 40, 0),
(439, 123, 'Multiple rooms', 40, 0),
(440, 123, 'Living or dining room', 40, 0),
(441, 123, 'Bedroom(s)', 40, 0),
(442, 123, 'rooms Kitchen', 40, 0),
(443, 123, 'Bathroom', 40, 0),
(444, 123, 'Other living space', 40, 0),
(445, 125, 'Quotes - I''m ready for competing quotes', 41, 0),
(446, 125, 'Consultation - I want to explore options with Wood Flooring Pros', 41, 0),
(447, 126, 'Water damage', 41, 0),
(448, 126, 'Splits', 41, 0),
(449, 126, 'Cracks', 41, 0),
(450, 126, 'Don''t know', 41, 0),
(451, 126, 'Cupping', 41, 0),
(452, 126, 'Splits Gaps', 41, 0),
(453, 126, 'Pet stains', 41, 0),
(454, 127, 'Living room', 41, 0),
(455, 127, 'Kitchen', 41, 0),
(456, 127, 'Stair Landing', 41, 0),
(457, 127, 'Family room', 41, 0),
(458, 127, 'Bathroom(s)', 41, 0),
(459, 127, 'Dining room', 41, 0),
(460, 127, 'Hall', 41, 0),
(461, 127, 'Stairs', 41, 0),
(462, 127, 'Bedroom(s)', 41, 0),
(463, 128, 'Natural', 41, 0),
(464, 128, 'Medium stain', 41, 0),
(465, 128, 'Bleached', 41, 0),
(466, 128, 'Want recommendation', 41, 0),
(467, 128, 'Light stain', 41, 0),
(468, 128, 'Dark stain', 41, 0),
(469, 128, 'Match other woodwork', 41, 0),
(470, 129, '', 42, 0),
(471, 130, 'Quotes - I''m ready for competing quotes', 42, 0),
(472, 130, 'Consultation - I want to explore options with Carpet Pros', 42, 0),
(473, 131, 'Home/Residence', 42, 0),
(474, 131, 'Business', 42, 0),
(475, 132, 'Yes', 42, 0),
(476, 132, 'No', 42, 0),
(477, 133, 'Replace existing carpet', 42, 0),
(478, 133, 'Remodel/Addition', 42, 0),
(479, 133, 'Replace other kind of flooring', 42, 0),
(480, 133, 'New Construction', 42, 0),
(481, 134, 'Bedroom(s)', 42, 0),
(482, 134, 'Dining room', 42, 0),
(483, 134, 'Kitchen', 42, 0),
(484, 134, 'Entry or hallway', 42, 0),
(485, 134, 'Family or living room', 42, 0),
(486, 134, 'Basement', 42, 0),
(487, 135, '', 43, 0),
(488, 136, 'Yes', 43, 0),
(489, 136, 'No', 43, 0),
(490, 137, 'Simulated hardwood', 43, 0),
(491, 137, 'Simulated tile', 43, 0),
(492, 137, 'Simulated stone or marble', 43, 0),
(493, 138, 'Living room', 43, 0),
(494, 138, 'Kitchen', 43, 0),
(495, 138, 'Stair Landing', 43, 0),
(496, 138, 'Bedroom(s)', 43, 0),
(497, 138, 'Family room', 43, 0),
(498, 138, 'Dining room', 43, 0),
(499, 138, 'Kitchen Hallway', 43, 0),
(500, 138, 'Entry', 43, 0),
(501, 138, 'Bathroom(s)', 43, 0),
(502, 138, 'room Stairs', 43, 0),
(503, 139, '', 44, 0),
(504, 140, 'Replace existing tile', 44, 0),
(505, 140, 'Repair existing tile', 44, 0),
(506, 140, 'Replace existing non-tile surface', 44, 0),
(507, 140, 'Tile for new construction', 44, 0),
(508, 141, 'Yes', 44, 0),
(509, 141, 'No', 44, 0),
(510, 142, 'Marble', 44, 0),
(511, 142, 'Sandstone', 44, 0),
(512, 142, 'Slate', 44, 0),
(513, 142, 'Soapstone', 44, 0),
(514, 142, 'Granite', 44, 0),
(515, 142, 'Limestone', 44, 0),
(516, 142, 'Quartz', 44, 0),
(517, 142, 'Travertine', 44, 0),
(518, 143, 'Floor', 44, 0),
(519, 143, 'Stairs', 44, 0),
(520, 143, 'Patio', 44, 0),
(521, 143, 'Swimming Pool', 44, 0),
(522, 143, 'Countertop', 44, 0),
(523, 143, 'Stairs Walls', 44, 0),
(524, 143, 'Fireplace', 44, 0),
(525, 144, 'Yes', 44, 0),
(526, 144, 'No', 44, 0),
(527, 145, '', 45, 0),
(528, 146, 'Sheet vinyl', 45, 0),
(529, 146, 'Vinyl tiles', 45, 0),
(530, 147, 'Yes', 45, 0),
(531, 147, 'No', 45, 0),
(532, 149, 'Bathtub', 46, 0),
(533, 149, 'Shower', 46, 0),
(534, 149, 'Combination tub/shower', 46, 0),
(535, 149, 'Steam shower', 46, 0),
(536, 150, 'Clear', 46, 0),
(537, 150, 'Glass block', 46, 0),
(538, 150, 'Clear Obscured', 46, 0),
(539, 150, 'block Etched', 46, 0),
(540, 150, 'Other', 46, 0),
(541, 151, 'Sliding by-pass doors', 46, 0),
(542, 151, 'Hinged door with 2 adjacent fixed glass panels', 46, 0),
(543, 151, 'Hinged door with adjacent fixed glass panel', 46, 0),
(544, 151, 'Hinged door (alone)', 46, 0),
(545, 151, 'Other', 46, 0),
(546, 152, 'Frameless (3/8" or 1/2" glass)', 46, 0),
(547, 152, 'Aluminum frame (1/4" glass)', 46, 0),
(548, 152, 'Other', 46, 0),
(549, 154, 'Install or Replace Mirror', 47, 0),
(550, 154, 'Replace Cracked Mirror', 47, 0),
(551, 154, 'Mirror needs to be resilvered', 47, 0),
(552, 154, 'Repair Mirror Frame', 47, 0),
(553, 155, 'Beveled edge', 47, 0),
(554, 155, 'Full length', 47, 0),
(555, 155, 'Wall-size', 47, 0),
(556, 155, 'Eased edge', 47, 0),
(557, 155, 'Vanity/makeup', 47, 0),
(558, 155, 'Sliding closet door', 47, 0),
(559, 156, 'Yes', 47, 0),
(560, 156, 'No', 47, 0),
(561, 158, 'Yes', 48, 0),
(562, 159, 'Yes', 48, 0),
(563, 159, 'No', 48, 0),
(564, 160, 'Install New Glass', 48, 0),
(565, 160, 'Water leaking around glass', 48, 0),
(566, 160, 'Broken latch', 48, 0),
(567, 160, 'Crank Malfunction', 48, 0),
(568, 160, 'Need Screens', 48, 0),
(569, 160, 'Window is jammed', 48, 0),
(570, 160, 'Deteriorated wood', 48, 0),
(571, 160, 'Glass is loose', 48, 0),
(572, 160, 'Moisture between panes of glass', 48, 0),
(573, 160, 'Block and tackle (pulley)', 48, 0),
(574, 160, 'Window Sticks', 48, 0),
(575, 160, 'Hinge', 48, 0),
(576, 160, 'Water damage around frame', 48, 0),
(577, 161, 'Fixed (non-opening picture window)', 48, 0),
(578, 161, 'Bow', 48, 0),
(579, 161, 'Garden Window', 48, 0),
(580, 161, 'Double-hung (both halves open)', 48, 0),
(581, 161, 'Storm', 48, 0),
(582, 161, 'Casement', 48, 0),
(583, 161, 'Sliding Glass Door', 48, 0),
(584, 161, 'Single-hung (lower half opens)', 48, 0),
(585, 161, 'Bay', 48, 0),
(586, 161, 'French Door', 48, 0),
(587, 161, '', 48, 0),
(588, 162, 'Aluminum', 48, 0),
(589, 162, 'Vinyl', 48, 0),
(590, 162, 'Wood', 48, 0),
(591, 162, 'Wood Clad', 48, 0),
(592, 164, 'Interior wall/partition', 49, 0),
(593, 164, 'Exterior wall/partition', 49, 0),
(594, 164, 'Wall between interior and exterior', 49, 0),
(595, 164, 'Shower stall', 49, 0),
(596, 164, 'Other', 49, 0),
(597, 165, 'Traditional glass block', 49, 0),
(598, 165, 'Maximum light', 49, 0),
(599, 165, 'Patterned design', 49, 0),
(600, 165, 'Colored glass/acrylic', 49, 0),
(601, 165, 'Curved wall', 49, 0),
(602, 165, 'Custom design', 49, 0),
(603, 165, 'Don''t Know', 49, 0),
(604, 165, 'Acrylic block', 49, 0),
(605, 165, 'Maximum privacy', 49, 0),
(606, 165, 'Frosted glass/acrylic', 49, 0),
(607, 165, 'Vent in solid window/wall', 49, 0),
(608, 165, 'Arched top', 49, 0),
(609, 165, 'Other', 49, 0),
(610, 167, 'Door or glass panel leaks', 50, 0),
(611, 167, 'Glass needs replacing', 50, 0),
(612, 167, 'Sliding door(s) are off track', 50, 0),
(613, 167, 'Wall behind frame is soft, moldy', 50, 0),
(614, 167, 'Door does not shut properly', 50, 0),
(615, 167, 'Glass is loose', 50, 0),
(616, 167, 'Frame pulling out of wall', 50, 0),
(617, 167, 'Water damage', 50, 0),
(618, 168, 'Bathtub', 50, 0),
(619, 168, 'Shower', 50, 0),
(620, 168, 'Combination bathtub/shower', 50, 0),
(621, 168, 'Steam shower', 50, 0),
(622, 170, 'Install or Replace Mirror', 51, 0),
(623, 170, 'Replace Cracked Mirror', 51, 0),
(624, 170, 'Mirror needs to be resilvered', 51, 0),
(625, 170, 'Repair Mirror Frame', 51, 0),
(626, 171, 'Which of these describes the mirror needing repair?', 51, 0),
(627, 171, 'Glued to wall', 51, 0),
(628, 171, 'Channel mount', 51, 0),
(629, 171, 'Eased edge', 51, 0),
(630, 171, 'Clip mount', 51, 0),
(631, 171, 'Framed', 51, 0),
(632, 173, 'Exterior window', 52, 0),
(633, 173, 'Interior window', 52, 0),
(634, 173, 'Cabinet or bookcase door(s)', 52, 0),
(635, 173, 'Shower doors', 52, 0),
(636, 173, 'Framed hanging window', 52, 0),
(637, 173, 'Entry door insert', 52, 0),
(638, 173, ' Skylight', 52, 0),
(639, 173, 'Mirror', 52, 0),
(640, 173, 'Other', 52, 0),
(641, 174, 'Colored (stained) glass', 52, 0),
(642, 174, 'Copper foil glass', 52, 0),
(643, 174, 'Etched glass', 52, 0),
(644, 174, 'Iridescent glass', 52, 0),
(645, 174, 'Insulated unit (triple-glazed thermo-paned)', 52, 0),
(646, 174, 'Overlay glass', 52, 0),
(647, 174, 'Leaded glass', 52, 0),
(648, 174, 'Beveled glass', 52, 0),
(649, 174, 'Fused glass', 52, 0),
(650, 174, 'Slumped glass', 52, 0),
(651, 174, 'Would like recommendation', 52, 0),
(652, 175, 'Yes, I have a idea/theme to discuss with designer', 52, 0),
(653, 175, 'No, I need a consultation with designer', 52, 0),
(654, 177, 'Yes', 53, 0),
(655, 177, 'No', 53, 0),
(656, 178, 'Improperly installed', 53, 0),
(657, 178, 'Broken/bent lead or copper foil', 53, 0),
(658, 178, 'Broken/cracked glass', 53, 0),
(659, 178, 'Piece is coming apart', 53, 0),
(660, 179, 'Exterior window', 53, 0),
(661, 179, 'Entry door insert', 53, 0),
(662, 179, 'Shower doors', 53, 0),
(663, 179, 'Framed hanging window', 53, 0),
(664, 179, 'Cabinet or bookcase door(s)', 53, 0),
(665, 179, 'Mirror', 53, 0),
(666, 179, 'Interior window', 53, 0),
(667, 179, 'Skylight', 53, 0),
(668, 179, 'Other', 53, 0),
(669, 181, 'Yes', 54, 0),
(670, 181, 'No', 54, 0),
(671, 182, 'Free-standing (portable)', 54, 0),
(672, 182, 'Built-in (in-ground)', 54, 0),
(673, 183, 'Indoor hot tub/spa', 54, 0),
(674, 183, 'Outdoor hot tub/spa', 54, 0),
(675, 184, 'Exercise swim jets', 54, 0),
(676, 184, 'Stereo and integrated speakers', 54, 0),
(677, 184, 'Bubbles', 54, 0),
(678, 184, 'Ozonator', 54, 0),
(679, 184, 'Programmable filtration cycles', 54, 0),
(680, 184, 'Pivoting seats', 54, 0),
(681, 184, 'jets Lights', 54, 0),
(682, 184, 'Cushioned head rests', 54, 0),
(683, 184, 'Tub freeze protection', 54, 0),
(684, 184, 'Chemical feeders', 54, 0),
(685, 184, 'Remote control', 54, 0),
(686, 184, 'Deck', 54, 0),
(687, 184, 'Don''t Know', 54, 0),
(688, 185, '', 55, 0),
(689, 186, 'Free-standing (portable)', 55, 0),
(690, 186, 'Built-in (in-ground)', 55, 0),
(691, 187, 'Leaking', 55, 0),
(692, 187, 'Jets not working', 55, 0),
(693, 187, 'Frozen water', 55, 0),
(694, 187, 'Tub is dirty', 55, 0),
(695, 187, 'Cover needs replacing', 55, 0),
(696, 187, 'Heater not working', 55, 0),
(697, 187, 'Filtration system needs cleaning', 55, 0),
(698, 187, 'No power to system', 55, 0),
(699, 187, 'Lights not working', 55, 0),
(700, 187, 'Surface is damaged', 55, 0),
(701, 187, 'Other', 55, 0),
(702, 189, 'Furnished and occupied', 56, 0),
(703, 189, 'Furnished and unoccupied', 56, 0),
(704, 189, 'Empty of furnishings and unoccupied', 56, 0),
(705, 190, 'Whole house', 56, 0),
(706, 190, 'Bathroom(s)', 56, 0),
(707, 190, 'Living areas', 56, 0),
(708, 190, 'Stairways', 56, 0),
(709, 190, 'Kitchen', 56, 0),
(710, 190, 'Bedroom(s)', 56, 0),
(711, 190, 'Hallways', 56, 0),
(712, 191, 'Walls', 56, 0),
(713, 191, 'Walls and ceilings', 56, 0),
(714, 191, 'Ceilings', 56, 0),
(715, 191, 'Other', 56, 0),
(716, 193, 'Wall(s)', 57, 0),
(717, 193, 'Floor', 57, 0),
(718, 193, 'Trim/Doors/Moldings', 57, 0),
(719, 193, 'Column', 57, 0),
(720, 193, 'Ceiling', 57, 0),
(721, 193, 'Countertop', 57, 0),
(722, 193, 'Moldings Fireplace', 57, 0),
(723, 193, 'Furniture', 57, 0),
(724, 193, 'Other', 57, 0),
(725, 194, 'Yes, I have an example to copy', 57, 0),
(726, 194, 'Yes, I can describe what I want', 57, 0),
(727, 194, 'No, I need a consultation for suggestions/options', 57, 0),
(728, 194, 'Other', 57, 0),
(729, 196, 'Contemporary bathtub', 58, 0),
(730, 196, 'Antique bathtub', 58, 0),
(731, 196, 'Shower stall', 58, 0),
(732, 196, 'Shower Pan', 58, 0),
(733, 196, 'Kitchen appliance(s)', 58, 0),
(734, 196, 'Whirlpool tub', 58, 0),
(735, 196, 'Bathroom sink', 58, 0),
(736, 196, 'Kitchen sink', 58, 0),
(737, 196, 'Wall tile', 58, 0),
(738, 196, 'Countertop tile', 58, 0),
(739, 196, ' Countertop', 58, 0),
(740, 196, 'Other', 58, 0),
(741, 197, 'Repair cracks and chips', 58, 0),
(742, 197, 'Resurface in new color', 58, 0),
(743, 197, 'Resurface in same color', 58, 0),
(744, 197, 'Match to other features in room', 58, 0),
(745, 197, 'Other', 58, 0),
(746, 199, 'New Construction', 59, 0),
(747, 199, 'Repaint', 59, 0),
(748, 199, 'Restain', 59, 0),
(749, 200, 'Siding', 59, 0),
(750, 200, 'Doors', 59, 0),
(751, 200, 'Shutters', 59, 0),
(752, 200, 'Masonry (brick/stone)', 59, 0),
(753, 200, 'Siding Trim', 59, 0),
(754, 200, 'Doors Stucco', 59, 0),
(755, 200, 'Shutters Fence', 59, 0),
(756, 200, ' Other', 59, 0),
(757, 201, 'Peeling or flaking paint', 59, 0),
(758, 201, 'Chalking', 59, 0),
(759, 201, 'Chipped stucco', 59, 0),
(760, 201, 'Trim pulling away from house', 59, 0),
(761, 201, 'Bare wood', 59, 0),
(762, 201, 'Chalking Cracks', 59, 0),
(763, 201, 'Rusty nails', 59, 0),
(764, 201, 'Water damage', 59, 0),
(765, 203, 'Wall(s)', 60, 0),
(766, 203, 'Floor', 60, 0),
(767, 203, ' Ceiling', 60, 0),
(768, 203, 'Other', 60, 0),
(769, 204, 'Have a basic idea of what I want', 60, 0),
(770, 204, 'Design drawings', 60, 0),
(771, 204, 'want Sketches', 60, 0),
(772, 204, 'Need design support', 60, 0),
(773, 206, 'Apply texture to unfinished drywall for paint', 61, 0),
(774, 206, 'Repair/Patch drywall', 61, 0),
(775, 206, 'Remove popcorn acoustic ceiling spray', 61, 0),
(776, 206, 'Paint also needed', 61, 0),
(777, 206, 'Match new drywall to existing walls/ceiling', 61, 0),
(778, 206, 'Prepare for wallpaper/special finish', 61, 0),
(779, 206, 'Create faux effect', 61, 0),
(780, 206, 'needed Other', 61, 0),
(781, 207, 'Wall(s)', 61, 0),
(782, 207, 'Ceiling(s)', 61, 0),
(783, 207, 'Other', 61, 0),
(784, 209, 'Kitchen', 63, 0),
(785, 209, 'Countertop', 63, 0),
(786, 209, 'Tub / shower or surrounding area', 63, 0),
(787, 209, 'Floor', 63, 0),
(788, 209, 'Bathroom', 63, 0),
(789, 209, 'Backsplash', 63, 0),
(790, 209, 'Wall', 63, 0),
(791, 209, 'Outdoors', 63, 0),
(792, 209, 'Other', 63, 0),
(793, 210, 'Ceramic tile', 63, 0),
(794, 210, 'Natural stone', 63, 0),
(795, 210, 'Travertine', 63, 0),
(796, 210, 'Don''t know', 63, 0),
(797, 210, 'Porcelain tile', 63, 0),
(798, 210, 'Marble', 63, 0),
(799, 210, 'Other', 63, 0),
(800, 212, 'Replace existing tile', 64, 0),
(801, 212, 'Repair existing tile', 64, 0),
(802, 212, 'Replace existing non-tile surface', 64, 0),
(803, 212, 'Tile for new construction', 64, 0),
(804, 213, 'Yes', 64, 0),
(805, 213, 'No', 64, 0),
(806, 214, 'Floor', 64, 0),
(807, 214, 'Tub/Shower', 64, 0),
(808, 214, 'Backsplash', 64, 0),
(809, 214, 'Wall', 64, 0),
(810, 214, 'Shower Countertop', 64, 0),
(811, 215, 'Glazed ceramic tile', 64, 0),
(812, 215, 'Porcelain tile', 64, 0),
(813, 215, 'Ceramic mosaic tile', 64, 0),
(814, 215, 'Special or custom tile', 64, 0),
(815, 215, 'Want recommendation', 64, 0),
(816, 217, 'Replace existing tile', 65, 0),
(817, 217, 'Repair existing tile', 65, 0),
(818, 217, 'Replace existing non-tile surface', 65, 0),
(819, 217, 'Tile for new construction', 65, 0),
(820, 218, 'Yes', 65, 0),
(821, 218, 'No', 65, 0),
(822, 219, 'Marble', 65, 0),
(823, 219, 'Sandstone', 65, 0),
(824, 219, 'Slate', 65, 0),
(825, 219, 'Soapstone', 65, 0),
(826, 219, 'Granite', 65, 0),
(827, 219, 'Limestone', 65, 0),
(828, 219, 'Quartz', 65, 0),
(829, 219, 'Travertine', 65, 0),
(830, 220, 'Floor', 65, 0),
(831, 220, 'Stairs', 65, 0),
(832, 220, 'Patio', 65, 0),
(833, 220, 'Swimming Pool', 65, 0),
(834, 220, 'Countertop', 65, 0),
(835, 220, 'Stairs Walls', 65, 0),
(836, 220, 'Fireplace', 65, 0),
(837, 221, 'Yes', 65, 0),
(838, 221, 'No', 65, 0),
(839, 223, 'Floor', 66, 0),
(840, 223, 'Outdoors', 66, 0),
(841, 223, 'Countertop', 66, 0),
(842, 223, 'Bathroom', 66, 0),
(843, 223, 'Tub / shower or surrounding area', 66, 0),
(844, 223, 'Kitchen', 66, 0),
(845, 223, 'Wall', 66, 0),
(846, 223, 'Backsplash', 66, 0),
(847, 224, 'Marble', 66, 0),
(848, 224, 'Granite', 66, 0),
(849, 224, 'Travertine', 66, 0),
(850, 224, 'Limestone', 66, 0),
(851, 224, 'Don''t know', 66, 0),
(852, 224, 'Other', 66, 0),
(853, 226, 'Clean / Seal grout or tile', 67, 0),
(854, 226, 'Stone or marble polishing', 67, 0),
(855, 226, 'Replace or repair grout', 67, 0),
(856, 227, 'Kitchen', 67, 0),
(857, 227, 'Countertops', 67, 0),
(858, 227, 'Tub / shower or surrounding area', 67, 0),
(859, 227, 'Floors', 67, 0),
(860, 227, 'Kitchen Bathroom', 67, 0),
(861, 227, 'Countertops Backsplash', 67, 0),
(862, 227, 'Walls', 67, 0),
(863, 227, 'Outdoors', 67, 0),
(864, 229, 'Cracked', 68, 0),
(865, 229, 'Chipped', 68, 0),
(866, 229, 'Worn', 68, 0),
(867, 229, 'Grout has mold/mildew', 68, 0),
(868, 229, 'Grout damaged or gone', 68, 0),
(869, 229, 'Grout needs sealing', 68, 0),
(870, 229, 'Need caulking', 68, 0),
(871, 229, 'Cracked Scratched', 68, 0),
(872, 229, 'Loose', 68, 0),
(873, 229, 'Tile discoloration', 68, 0),
(874, 229, 'Grout discoloration', 68, 0),
(875, 229, 'Grout needs cleaning', 68, 0),
(876, 229, 'Want to change grout color', 68, 0),
(877, 229, 'Don''t Know', 68, 0),
(878, 229, 'Other', 68, 0),
(879, 230, 'Countertop', 68, 0),
(880, 230, 'Walls', 68, 0),
(881, 230, 'Stairs', 68, 0),
(882, 230, 'Backsplash', 68, 0),
(883, 230, 'Floor', 68, 0),
(884, 230, 'Tub/shower stall', 68, 0),
(885, 230, 'Swimming Pool', 68, 0),
(886, 230, 'Other', 68, 0),
(887, 231, 'Yes', 68, 0),
(888, 231, 'No', 68, 0),
(889, 233, 'Cracked', 69, 0),
(890, 233, 'Chipped', 69, 0),
(891, 233, 'Worn', 69, 0),
(892, 233, 'Dull/Hazy', 69, 0),
(893, 233, 'Need polished/sealed', 69, 0),
(894, 233, 'Grout damaged or gone', 69, 0),
(895, 233, 'Grout needs cleaned', 69, 0),
(896, 233, 'Want to change grout color', 69, 0),
(897, 233, 'Scratched', 69, 0),
(898, 233, 'Loose', 69, 0),
(899, 233, 'Discolored/Stained', 69, 0),
(900, 233, 'Need cleaned', 69, 0),
(901, 233, 'Grout discoloration', 69, 0),
(902, 233, 'Grout has mold/mildew', 69, 0),
(903, 233, 'Grout needs sealed', 69, 0),
(904, 233, 'Other', 69, 0),
(905, 234, 'Countertop', 69, 0),
(906, 234, 'Floor', 69, 0),
(907, 234, 'Stairs', 69, 0),
(908, 234, 'Swimming Pool', 69, 0),
(909, 234, 'Backsplash', 69, 0),
(910, 234, 'Wall', 69, 0),
(911, 234, 'Fireplace', 69, 0),
(912, 234, 'Other', 69, 0),
(913, 235, 'Yes', 69, 0),
(914, 235, 'No', 69, 0),
(915, 237, 'New construction/Addition', 70, 0),
(916, 237, 'Replacing old ceiling', 70, 0),
(917, 238, 'Standard', 70, 0),
(918, 238, 'Sound absorbing', 70, 0),
(919, 238, 'Fire rated', 70, 0),
(920, 238, 'Designer', 70, 0),
(921, 238, 'Insulation', 70, 0),
(922, 240, 'Yes', 71, 0),
(923, 240, 'No', 71, 0),
(924, 241, 'Wood framing', 71, 0),
(925, 241, 'Metal framing', 71, 0),
(926, 241, 'Don''t Know', 71, 0),
(927, 242, 'Entire house as part of new home construction', 71, 0),
(928, 242, 'Addition onto an existing home', 71, 0),
(929, 242, 'Put a pitch on a roof', 71, 0),
(930, 242, 'Outbuilding detached from the home', 71, 0),
(931, 242, 'Second story addition', 71, 0),
(932, 242, 'Interior framing for remodel project', 71, 0),
(933, 242, 'Other', 71, 0),
(934, 244, 'Need to remove an existing lighting fixture and install a ceiling fan', 72, 0),
(935, 244, 'Need to remove an existing lighting fixture and install a ceiling fan', 72, 0),
(936, 244, 'Need to replace existing ceiling fan', 72, 0),
(937, 245, 'Yes', 72, 0),
(938, 245, 'No', 72, 0),
(939, 246, 'Wall switch', 72, 0),
(940, 246, 'Remote control', 72, 0),
(941, 246, 'Pull string', 72, 0),
(942, 246, 'Don''t know', 72, 0),
(943, 247, 'Economy grade', 72, 0),
(944, 247, 'Medium grade', 72, 0),
(945, 247, 'Premium grade', 72, 0),
(946, 247, 'Don''t Know', 72, 0),
(947, 247, '', 72, 0),
(948, 249, 'Standard drywall installation (hang, tape, sand, and texture)', 73, 0),
(949, 249, 'Install moisture proof drywall (green board)', 73, 0),
(950, 249, 'Tape and sand joints only', 73, 0),
(951, 249, 'Spray texture only', 73, 0),
(952, 249, 'Install (hang) drywall only', 73, 0),
(953, 249, 'Install Wonderboard or Durarock (backer board for tile)', 73, 0),
(954, 249, 'Tape, sand, and texture only', 73, 0),
(955, 249, 'Replace popcorn acoustic ceiling spray with texture to match walls', 73, 0),
(956, 249, 'Other', 73, 0),
(957, 249, 'Don''t Know', 73, 0),
(958, 249, '', 73, 0),
(959, 250, 'Living room', 73, 0),
(960, 250, 'Kitchen', 73, 0),
(961, 250, 'Entry', 73, 0),
(962, 250, 'Bathroom(s)', 73, 0),
(963, 250, 'Office', 73, 0),
(964, 250, 'Stairwells', 73, 0),
(965, 250, 'Dining room', 73, 0),
(966, 250, 'Hallway', 73, 0),
(967, 250, 'Bedroom(s)', 73, 0),
(968, 250, 'Family room', 73, 0),
(969, 250, 'Ceiling(s) only', 73, 0),
(970, 250, 'Garage or storage room', 73, 0),
(971, 250, 'Other', 73, 0),
(972, 251, 'No finish', 73, 0),
(973, 251, 'Smooth finish (no texture) to paint', 73, 0),
(974, 251, 'Simple texture (orange peel, knockdown)', 73, 0),
(975, 251, 'Texture to match existing drywall', 73, 0),
(976, 251, 'Don''t Know', 73, 0),
(977, 251, 'Fire tape only (garage, storage, work room)', 73, 0),
(978, 251, 'Smooth finish to apply wallpaper/faux painting techniques', 73, 0),
(979, 251, 'Complex texture (hand troweled)', 73, 0),
(980, 251, 'Would also like paint', 73, 0),
(981, 251, 'Other', 73, 0),
(982, 252, '', 74, 0),
(983, 253, 'Brick', 74, 0),
(984, 253, 'Stone', 74, 0),
(985, 253, 'Interlocking concrete blocks', 74, 0),
(986, 253, 'Concrete block (cinder block)', 74, 0),
(987, 253, 'Concrete block with stucco veneer', 74, 0),
(988, 253, 'Concrete block with brick or stone veneer', 74, 0),
(989, 254, 'Retaining wall', 74, 0),
(990, 254, 'Decorative wall', 74, 0),
(991, 254, 'Protective wall', 74, 0),
(992, 254, 'Other', 74, 0),
(993, 255, 'Excavation', 74, 0),
(994, 255, 'Drainage', 74, 0),
(995, 255, 'Waterproofing', 74, 0),
(996, 255, 'Unsure', 74, 0),
(997, 256, '', 75, 0),
(998, 257, 'Master Bedroom', 75, 0),
(999, 257, 'Hallway', 75, 0),
(1000, 257, 'Garage', 75, 0),
(1001, 257, 'Laundry / utility room', 75, 0),
(1002, 257, 'Bedroom', 75, 0),
(1003, 257, 'Bathroom', 75, 0),
(1004, 257, 'Entryway / foyer', 75, 0),
(1005, 257, 'Basement', 75, 0),
(1006, 257, 'Other', 75, 0),
(1007, 258, 'Built-in shelves', 75, 0),
(1008, 258, 'Lighting', 75, 0),
(1009, 258, 'Wall texture', 75, 0),
(1010, 258, 'Closet rod for hanging clothes', 75, 0),
(1011, 258, 'Mirror', 75, 0),
(1012, 258, 'Other', 75, 0),
(1013, 259, 'Yes', 75, 0),
(1014, 259, 'No', 75, 0),
(1015, 261, 'Interior wall/partition', 76, 0),
(1016, 261, 'Exterior wall/partition', 76, 0),
(1017, 261, 'Wall between interior and exterior', 76, 0),
(1018, 261, 'Shower stall', 76, 0),
(1019, 261, 'Other', 76, 0),
(1020, 262, 'Traditional glass block', 76, 0),
(1021, 262, 'Maximum light', 76, 0),
(1022, 262, 'Patterned design', 76, 0),
(1023, 262, 'Colored glass/acrylic', 76, 0),
(1024, 262, 'Curved wall', 76, 0),
(1025, 262, 'Custom design', 76, 0),
(1026, 262, 'Don''t Know', 76, 0),
(1027, 262, 'Acrylic block', 76, 0),
(1028, 262, 'Maximum privacy', 76, 0),
(1029, 262, 'Frosted glass/acrylic', 76, 0),
(1030, 262, 'Vent in solid window/wall', 76, 0),
(1031, 262, 'Arched top', 76, 0),
(1032, 262, 'Other', 76, 0),
(1033, 263, 'Traditional glass block', 76, 0),
(1034, 263, 'Maximum light', 76, 0),
(1035, 263, 'Patterned design', 76, 0),
(1036, 263, 'Colored glass/acrylic', 76, 0),
(1037, 263, 'Curved wall', 76, 0),
(1038, 263, 'Custom design', 76, 0),
(1039, 263, 'Don''t Know', 76, 0),
(1040, 263, 'Acrylic block', 76, 0),
(1041, 263, 'Maximum privacy', 76, 0),
(1042, 263, 'Frosted glass/acrylic', 76, 0),
(1043, 263, 'Vent in solid window/wall', 76, 0),
(1044, 263, 'Arched top', 76, 0),
(1045, 263, 'Other', 76, 0),
(1046, 265, 'Window casing', 77, 0),
(1047, 265, 'Floor molding', 77, 0),
(1048, 265, 'Ceiling molding - picture and crow', 77, 0),
(1049, 265, 'Stair system', 77, 0),
(1050, 265, 'Cabinetry', 77, 0),
(1051, 265, 'Laminate or wood countertops', 77, 0),
(1052, 265, 'Door casing', 77, 0),
(1053, 265, 'Wall molding - paneling, wainscoting and chair rail', 77, 0),
(1054, 265, 'Crown molding', 77, 0),
(1055, 265, 'Fireplace mantel', 77, 0),
(1056, 265, 'Shelving', 77, 0),
(1057, 265, 'Other', 77, 0),
(1058, 266, 'Interior doors', 77, 0),
(1059, 266, 'Windows', 77, 0),
(1060, 266, 'Exterior doors', 77, 0),
(1061, 266, 'None', 77, 0),
(1062, 267, 'Yes', 77, 0),
(1063, 267, 'No', 77, 0),
(1064, 267, 'Don''t Know', 77, 0),
(1065, 268, '', 78, 0),
(1066, 269, 'Smooth finish (no texture)', 78, 0),
(1067, 269, 'Simple texture', 78, 0),
(1068, 269, 'Complex texture', 78, 0),
(1069, 269, 'Want recommendation', 78, 0),
(1070, 270, 'Kitchen', 78, 0),
(1071, 270, 'Bedroom(s)', 78, 0),
(1072, 270, 'Hallway', 78, 0),
(1073, 270, 'Garage or storage room', 78, 0),
(1074, 270, 'Dining room', 78, 0),
(1075, 270, 'Family/Living room', 78, 0),
(1076, 270, 'Bathroom(s)', 78, 0),
(1077, 270, 'Other', 78, 0),
(1078, 272, 'Retaining wall', 79, 0),
(1079, 272, 'Decorative wall', 79, 0),
(1080, 272, 'Protective wall', 79, 0),
(1081, 272, 'Other', 79, 0),
(1082, 273, 'Special color additives', 79, 0),
(1083, 273, 'Waterproofing', 79, 0),
(1084, 273, 'Brick or stone veneer', 79, 0),
(1085, 273, 'Old concrete removal', 79, 0),
(1086, 273, 'Textured finish', 79, 0),
(1087, 273, 'Stucco coating', 79, 0),
(1088, 273, 'Drainage', 79, 0),
(1089, 275, 'Yes', 80, 0),
(1090, 275, 'No', 80, 0),
(1091, 276, 'Repair cracks', 80, 0),
(1092, 276, 'Repair holes in surface', 80, 0),
(1093, 276, 'Remove existing wallpaper', 80, 0),
(1094, 276, 'Repair moisture damage', 80, 0),
(1095, 276, 'Remove peeling paint', 80, 0),
(1096, 276, 'Repair popped nails', 80, 0),
(1097, 276, 'Smooth textured drywall', 80, 0),
(1098, 276, 'Don''t Know', 80, 0),
(1099, 276, 'Other', 80, 0),
(1100, 278, 'Door installation / replacement', 82, 0),
(1101, 278, 'Door repair', 82, 0),
(1102, 280, 'Door installation / replacement', 82, 0),
(1103, 280, 'Door repair', 82, 0),
(1104, 281, 'Exterior', 82, 0),
(1105, 281, 'Sliding', 82, 0),
(1106, 281, 'Garage', 82, 0),
(1107, 281, 'Secrurity gate', 82, 0),
(1108, 281, 'Interior', 82, 0),
(1109, 281, 'Storm door', 82, 0),
(1110, 281, 'Glass shower door', 82, 0),
(1111, 282, 'Yes', 82, 0),
(1112, 282, 'No', 82, 0),
(1113, 283, 'Home/Residence', 82, 0),
(1114, 283, 'Business', 82, 0),
(1115, 283, '', 82, 0),
(1116, 285, 'Replace an old one of the same type', 83, 0),
(1117, 285, 'Replace an old one with different type', 83, 0),
(1118, 285, 'Install where one did not previously exist', 83, 0),
(1119, 285, 'Repair existing door', 83, 0),
(1120, 286, 'Interior', 83, 0),
(1121, 286, 'Exterior', 83, 0),
(1122, 286, 'Exterior sliding door', 83, 0),
(1123, 287, 'Yes', 83, 0),
(1124, 287, 'No', 83, 0),
(1125, 288, 'Yes', 83, 0),
(1126, 288, 'No', 83, 0),
(1127, 291, 'Yes', 84, 0),
(1128, 291, 'No', 84, 0),
(1129, 292, 'Add a new egress window', 84, 0),
(1130, 292, 'Replace or modify an existing egress window', 84, 0),
(1131, 292, 'Other', 84, 0),
(1132, 292, '', 84, 0),
(1133, 293, 'Yes', 84, 0),
(1134, 293, 'No', 84, 0),
(1135, 294, 'Energy efficient windows', 84, 0),
(1136, 294, 'Escape ladders', 84, 0),
(1137, 294, 'Window well covers', 84, 0),
(1138, 296, 'I need to purchase skylight(s)', 85, 0),
(1139, 296, 'I need a contractor to install skylight(s) in an existing opening', 85, 0),
(1140, 296, 'I need a contractor to cut an opening in an existing roof, reframe, and install skylight(s)', 85, 0),
(1141, 296, 'Other', 85, 0),
(1142, 297, 'Asphalt shingle', 85, 0),
(1143, 297, 'Traditional tile', 85, 0),
(1144, 297, 'Metal roof', 85, 0),
(1145, 297, 'Don''t Know', 85, 0),
(1146, 297, 'Wood shake or shingle', 85, 0),
(1147, 297, 'Lightweight tile', 85, 0),
(1148, 297, 'Tar & gravel, single ply or sprayed foam', 85, 0),
(1149, 297, 'Other', 85, 0),
(1150, 299, 'Exterior window', 86, 0),
(1151, 299, 'Interior window', 86, 0),
(1152, 299, 'Cabinet or bookcase door(s)', 86, 0),
(1153, 299, 'Shower doors', 86, 0),
(1154, 299, 'Framed hanging window', 86, 0),
(1155, 299, 'Entry door insert', 86, 0),
(1156, 299, 'Skylight', 86, 0),
(1157, 299, 'Mirror', 86, 0),
(1158, 299, 'Other', 86, 0),
(1159, 300, 'Colored (stained) glass', 86, 0),
(1160, 300, 'Copper foil glass', 86, 0),
(1161, 300, 'Etched glass', 86, 0),
(1162, 300, 'Iridescent glass', 86, 0),
(1163, 300, 'Insulated unit (triple-glazed thermo-paned)', 86, 0),
(1164, 300, 'Overlay glass', 86, 0),
(1165, 300, 'Leaded glass', 86, 0),
(1166, 300, 'Beveled glass', 86, 0),
(1167, 300, 'Fused glass', 86, 0),
(1168, 300, 'Slumped glass', 86, 0),
(1169, 300, 'Would like recommendation', 86, 0),
(1170, 301, 'Yes, I have a idea/theme to discuss with designer', 86, 0),
(1171, 301, 'No, I need a consultation with designer', 86, 0),
(1172, 303, 'New Window(s) - Installation', 87, 0),
(1173, 303, 'Repair Window Glass', 87, 0),
(1174, 303, 'Storm Window(s) - Installation', 87, 0),
(1175, 303, 'Repair Window Frame', 87, 0),
(1176, 304, 'Yes', 87, 0),
(1177, 304, 'No', 87, 0),
(1178, 306, 'Walk in closet', 89, 0),
(1179, 306, 'Reach in closet', 89, 0),
(1180, 306, 'Laundry room', 89, 0),
(1181, 306, 'Kitchen/Pantry', 89, 0),
(1182, 306, 'Media room', 89, 0),
(1183, 306, 'Home office', 89, 0),
(1184, 306, 'Basement', 89, 0),
(1185, 306, 'Family room', 89, 0),
(1186, 307, 'Shelving', 89, 0),
(1187, 307, 'Baskets', 89, 0),
(1188, 307, 'Hanging storage', 89, 0),
(1189, 307, 'Drawers', 89, 0),
(1190, 307, 'Cabinets', 89, 0),
(1191, 307, 'Want recommendation', 89, 0),
(1192, 309, 'Walk in closet', 90, 0),
(1193, 309, 'Reach in closet', 90, 0),
(1194, 309, 'Laundry room', 90, 0),
(1195, 309, 'Kitchen/Pantry', 90, 0),
(1196, 309, 'Media room', 90, 0),
(1197, 309, 'Home office', 90, 0),
(1198, 309, 'Basement', 90, 0),
(1199, 309, 'Family room', 90, 0),
(1200, 310, 'Shelving', 90, 0),
(1201, 310, 'Baskets', 90, 0),
(1202, 310, 'Hanging storage', 90, 0),
(1203, 310, 'Drawers', 90, 0),
(1204, 310, 'Cabinets', 90, 0),
(1205, 310, 'Want recommendation', 90, 0),
(1206, 312, 'Entertainment center', 91, 0),
(1207, 312, 'Custom storage', 91, 0),
(1208, 312, 'Wall Unit', 91, 0),
(1209, 312, 'Bookshelves', 91, 0),
(1210, 312, 'Seating', 91, 0),
(1211, 313, 'Kitchen', 91, 0),
(1212, 313, 'Family room', 91, 0),
(1213, 313, 'Entertainment center', 91, 0),
(1214, 313, 'Bathroom', 91, 0),
(1215, 313, 'Home office', 91, 0),
(1216, 313, 'Bedroom', 91, 0),
(1217, 313, 'Other Location', 91, 0),
(1218, 314, 'Adjustable shelves', 91, 0),
(1219, 314, 'Crown molding', 91, 0),
(1220, 314, 'Pull-out shelves', 91, 0),
(1221, 314, 'Pantry cabinet', 91, 0),
(1222, 314, 'Island', 91, 0),
(1223, 314, 'Lazy Susan', 91, 0),
(1224, 314, 'Eating counter', 91, 0),
(1225, 314, 'Desk/computer area', 91, 0),
(1226, 314, 'Other', 91, 0),
(1227, 316, 'Master Bedroom', 92, 0),
(1228, 316, 'Hallway', 92, 0),
(1229, 316, 'Garage', 92, 0),
(1230, 316, 'Laundry / utility room', 92, 0),
(1231, 316, 'Other', 92, 0),
(1232, 316, 'Bedroom', 92, 0),
(1233, 316, 'Bathroom', 92, 0),
(1234, 316, 'Entryway / foyer', 92, 0),
(1235, 316, 'Basement', 92, 0),
(1236, 317, 'Built-in shelves', 92, 0),
(1237, 317, 'Lighting', 92, 0),
(1238, 317, 'Wall texture', 92, 0),
(1239, 317, 'Closet rod for hanging clothes', 92, 0),
(1240, 317, 'Mirror', 92, 0),
(1241, 317, 'Other', 92, 0),
(1242, 318, 'Yes', 92, 0),
(1243, 318, 'No', 92, 0),
(1244, 320, 'Install new pre-made cabinets', 93, 0),
(1245, 320, 'Install new custom cabinets', 93, 0),
(1246, 320, 'Reface existing cabinets', 93, 0),
(1247, 320, 'Repair existing cabinets', 93, 0),
(1248, 320, 'Refinish existing cabinets', 93, 0),
(1249, 321, 'Yes', 93, 0),
(1250, 321, 'No', 93, 0),
(1251, 322, 'Kitchen', 93, 0),
(1252, 322, 'Closet', 93, 0),
(1253, 322, 'Garage', 93, 0),
(1254, 322, 'Bathroom', 93, 0),
(1255, 322, 'Basement', 93, 0),
(1256, 322, 'Other', 93, 0),
(1257, 324, 'Interior covered with carpet', 94, 0),
(1258, 324, 'Interior painted', 94, 0),
(1259, 324, 'Curved', 94, 0),
(1260, 324, 'Landing mid-stair', 94, 0),
(1261, 324, 'Interior stained', 94, 0),
(1262, 324, 'Straight', 94, 0),
(1263, 324, 'Spiral', 94, 0),
(1264, 324, 'Exterior', 94, 0),
(1265, 324, 'Other', 94, 0),
(1266, 325, 'Open on one side (railing on one side, wall on the other)', 94, 0),
(1267, 325, 'Open on two sides (railings on both sides)', 94, 0),
(1268, 325, 'Open on no sides (railings on no sides, just a hand rail)', 94, 0),
(1269, 325, 'Other', 94, 0),
(1270, 327, 'Interior', 95, 0),
(1271, 327, 'Straight', 95, 0),
(1272, 327, 'Flared', 95, 0),
(1273, 327, 'Spiral', 95, 0),
(1274, 327, 'Closed riser', 95, 0),
(1275, 327, 'Exterior', 95, 0),
(1276, 327, 'Curved', 95, 0),
(1277, 327, 'Circular', 95, 0),
(1278, 327, 'Landing mid-stair', 95, 0),
(1279, 327, 'Open risers (no risers)', 95, 0),
(1280, 328, 'Open on one side (railing on one side, wall on the other)', 95, 0),
(1281, 328, 'Open on two sides (railings on both sides)', 95, 0),
(1282, 328, 'Open on no sides (railings on no sides, just a hand rail)', 95, 0),
(1283, 328, 'Other', 95, 0),
(1284, 329, 'Raw steel', 95, 0),
(1285, 329, 'Aluminum', 95, 0),
(1286, 329, 'Galvanized steel', 95, 0),
(1287, 329, 'Bronze', 95, 0),
(1288, 329, 'Powder coated', 95, 0),
(1289, 329, 'Wood treads', 95, 0),
(1290, 329, 'Don''t Know', 95, 0),
(1291, 329, 'Wrought iron', 95, 0),
(1292, 329, 'Stainless Steel', 95, 0),
(1293, 329, 'Brass', 95, 0),
(1294, 329, 'Painted', 95, 0),
(1295, 329, 'Wooden handrail', 95, 0),
(1296, 329, 'Other', 95, 0),
(1297, 330, 'Less than 3 ft.', 95, 0),
(1298, 330, '5-7 ft.', 95, 0),
(1299, 330, 'Other', 95, 0),
(1300, 330, '3-5 ft.', 95, 0),
(1301, 330, 'More than 7 ft.', 95, 0),
(1302, 332, 'Interior', 96, 0),
(1303, 332, 'Straight', 96, 0),
(1304, 332, 'Flared', 96, 0),
(1305, 332, 'Spiral', 96, 0),
(1306, 332, 'Closed riser', 96, 0),
(1307, 332, 'Exterior', 96, 0),
(1308, 332, 'Curved', 96, 0),
(1309, 332, 'Circular', 96, 0),
(1310, 332, 'Landing mid-stair', 96, 0),
(1311, 332, 'Open risers (no risers)', 96, 0),
(1312, 332, 'Other', 96, 0),
(1313, 333, 'Unstable staircase', 96, 0),
(1314, 333, 'New railing needed', 96, 0),
(1315, 333, 'New tread(s) needed', 96, 0),
(1316, 333, 'Re-paint', 96, 0),
(1317, 333, 'Railing pulling out of wall', 96, 0),
(1318, 333, 'Baluster(s) needed', 96, 0),
(1319, 333, 'Re-weld', 96, 0),
(1320, 333, 'Other', 96, 0),
(1321, 334, 'Raw steel', 96, 0),
(1322, 334, 'Aluminum', 96, 0),
(1323, 334, 'Galvanized steel', 96, 0),
(1324, 334, 'Bronze', 96, 0),
(1325, 334, 'Powder coated', 96, 0),
(1326, 334, 'Wood treads', 96, 0),
(1327, 334, 'Don''t Know', 96, 0),
(1328, 334, 'Wrought iron', 96, 0),
(1329, 334, 'Stainless Steel', 96, 0),
(1330, 334, 'Brass', 96, 0),
(1331, 334, 'Painted', 96, 0),
(1332, 334, 'Wooden handrail', 96, 0),
(1333, 334, 'Other', 96, 0),
(1334, 336, 'Yes', 97, 0),
(1335, 336, 'No', 97, 0),
(1336, 337, 'Interior', 97, 0),
(1337, 337, 'Exterior', 97, 0),
(1338, 337, 'Both', 97, 0),
(1339, 338, 'Stairs creak', 97, 0),
(1340, 338, 'Railing pulling out of wall', 97, 0),
(1341, 338, 'New tread(s) needed', 97, 0),
(1342, 338, 'Balusters are broken', 97, 0),
(1343, 338, 'Replace carpeted stairs w/runner and tread caps', 97, 0),
(1344, 338, 'Unstable staircase', 97, 0),
(1345, 338, 'New railing needed', 97, 0),
(1346, 338, 'More balusters needed', 97, 0),
(1347, 338, 'Repalce carpeted treads w/hardwood veneer', 97, 0),
(1348, 338, 'Other', 97, 0),
(1349, 340, 'Install or completely replace exterior trim', 98, 0),
(1350, 340, 'Repair or partially replace exterior trim', 98, 0),
(1351, 341, 'Wood', 98, 0),
(1352, 341, 'Aluminum wrap', 98, 0),
(1353, 341, 'Vinyl', 98, 0),
(1354, 341, 'Fiber cement', 98, 0),
(1355, 341, 'Don''t Know', 98, 0),
(1356, 341, 'Other', 98, 0),
(1357, 342, 'Stucco', 98, 0),
(1358, 342, 'Vinyl', 98, 0),
(1359, 342, 'Asbestos', 98, 0),
(1360, 342, 'Wood panel', 98, 0),
(1361, 342, 'Aluminum', 98, 0),
(1362, 342, 'Brick', 98, 0),
(1363, 342, 'Wood Shingle', 98, 0),
(1364, 342, 'Steel', 98, 0),
(1365, 342, 'Stone', 98, 0),
(1366, 342, 'Other', 98, 0),
(1367, 344, 'Leak through window, wall or into basement', 99, 0),
(1368, 344, 'Trim is twisted, warped, cracked, or split', 99, 0),
(1369, 344, 'Trim is loose or hanging out of position', 99, 0),
(1370, 344, 'Trim is soft, rotten, blistering or peeling', 99, 0),
(1371, 344, 'Other', 99, 0),
(1372, 345, 'Wood', 99, 0),
(1373, 345, 'Vinyl', 99, 0),
(1374, 345, 'Aluminum', 99, 0),
(1375, 345, 'Steel', 99, 0),
(1376, 346, 'Stucco', 99, 0),
(1377, 346, 'Wood Shingle', 99, 0),
(1378, 346, 'Aluminum', 99, 0),
(1379, 346, 'Asbestos', 99, 0),
(1380, 346, 'Wood panel', 99, 0),
(1381, 346, 'Vinyl', 99, 0),
(1382, 346, 'Steel', 99, 0),
(1383, 347, '', 100, 0),
(1384, 347, '', 100, 0),
(1385, 348, 'Standard (square edge)', 100, 0),
(1386, 348, 'Half round', 100, 0),
(1387, 348, 'Want recommendation', 100, 0),
(1388, 348, 'Want recommendation', 100, 0),
(1389, 348, 'Other', 100, 0),
(1390, 349, '4"', 100, 0),
(1391, 349, '5" (Standard)', 100, 0),
(1392, 350, '4"', 100, 0),
(1393, 350, '5" (Standard)', 100, 0),
(1394, 350, '6"', 100, 0),
(1395, 350, 'Want recommendation', 100, 0),
(1396, 350, '', 100, 0),
(1397, 352, 'Gutter is sagging or broken loose', 101, 0),
(1398, 352, 'Leaking from seams', 101, 0),
(1399, 352, 'Gutter is dented', 101, 0),
(1400, 352, 'Rotted gutter material', 101, 0),
(1401, 352, 'Water not draining due to improper slope', 101, 0);
INSERT INTO `answers` (`id`, `q_id`, `name`, `service_id`, `q_order`) VALUES
(1402, 352, 'Downspout or gutter is clogged, disconnected or broken', 101, 0),
(1403, 352, 'Hole in gutter', 101, 0),
(1404, 352, 'Add or move downspout', 101, 0),
(1405, 352, 'Other', 101, 0),
(1406, 354, 'Replace existing siding', 102, 0),
(1407, 354, 'Siding for a new addition', 102, 0),
(1408, 354, 'Repair section(s) of siding', 102, 0),
(1409, 354, 'Siding for a new home', 102, 0),
(1410, 356, 'There''s a leak through the wall', 103, 0),
(1411, 356, 'Siding is missing', 103, 0),
(1412, 356, 'Wood is soft, has powdery residue or is rotten (dryrot)', 103, 0),
(1413, 356, 'Damaged soffit or fascia', 103, 0),
(1414, 356, 'Siding is loose or hanging out of position', 103, 0),
(1415, 356, 'Siding is warped, cracked or split', 103, 0),
(1416, 356, 'Missing, broken or rusting flashing', 103, 0),
(1417, 356, 'Mildew or fungus is growing on the siding', 103, 0),
(1418, 356, 'Other', 103, 0),
(1419, 358, 'Rectangular / Square', 104, 0),
(1420, 358, 'Octagon (8-sided)', 104, 0),
(1421, 358, 'Oval', 104, 0),
(1422, 358, 'Not sure - Want recommendation', 104, 0),
(1423, 359, 'None-it will rest on existing deck/patio', 104, 0),
(1424, 359, 'Raised deck with lattice', 104, 0),
(1425, 359, 'Poured concrete slab', 104, 0),
(1426, 359, 'Raised solid deck', 104, 0),
(1427, 359, 'Other', 104, 0),
(1428, 360, 'Traditional square picket walls', 104, 0),
(1429, 360, 'Skylight', 104, 0),
(1430, 360, 'Swing', 104, 0),
(1431, 360, 'Paint', 104, 0),
(1432, 360, 'Door', 104, 0),
(1433, 360, 'Ceiling Fan', 104, 0),
(1434, 360, 'Plumbing', 104, 0),
(1435, 360, 'Stereo, TV, cable, etc.', 104, 0),
(1436, 360, 'Solid walls', 104, 0),
(1437, 360, 'Bench(es)', 104, 0),
(1438, 360, 'Bar', 104, 0),
(1439, 360, 'Screens', 104, 0),
(1440, 360, 'Glass windows', 104, 0),
(1441, 360, 'Electricity', 104, 0),
(1442, 360, 'Other', 104, 0),
(1443, 362, 'Storage', 105, 0),
(1444, 362, 'Workshop', 105, 0),
(1445, 362, 'Office', 105, 0),
(1446, 362, 'Guest House', 105, 0),
(1447, 362, 'Barn', 105, 0),
(1448, 362, 'Playhouse', 105, 0),
(1449, 362, 'Garage', 105, 0),
(1450, 362, 'Other', 105, 0),
(1451, 363, 'Wood', 105, 0),
(1452, 363, 'Steel', 105, 0),
(1453, 363, 'Siding & roofing to match home', 105, 0),
(1454, 363, 'Don''t Know', 105, 0),
(1455, 363, 'Aluminum', 105, 0),
(1456, 363, 'Vinyl siding', 105, 0),
(1457, 363, 'Other', 105, 0),
(1458, 364, 'Loft', 105, 0),
(1459, 364, 'Concrete slab floor', 105, 0),
(1460, 364, 'Plumbing', 105, 0),
(1461, 364, 'Roll-up doors', 105, 0),
(1462, 364, 'Insulation', 105, 0),
(1463, 364, 'Flooring or carpet', 105, 0),
(1464, 364, 'Animal stalls', 105, 0),
(1465, 364, 'Cabinets', 105, 0),
(1466, 364, 'Plywood floor on joists', 105, 0),
(1467, 364, 'Electricity', 105, 0),
(1468, 364, 'Windows', 105, 0),
(1469, 364, 'Drywall', 105, 0),
(1470, 364, 'Painted interior', 105, 0),
(1471, 364, 'Deck or porch', 105, 0),
(1472, 364, 'Ramps', 105, 0),
(1473, 364, 'Built-in shelving', 105, 0),
(1474, 365, 'Predesigned building package', 105, 0),
(1475, 365, 'Custom building to be constructed onsite', 105, 0),
(1476, 365, 'Don''t Know', 105, 0),
(1477, 365, 'Custom designed building package', 105, 0),
(1478, 365, 'Other', 105, 0),
(1479, 367, 'General repairs', 106, 0),
(1480, 367, 'Add or move walls', 106, 0),
(1481, 367, 'Add plumbing', 106, 0),
(1482, 367, 'Repair roofing', 106, 0),
(1483, 367, 'Remodel or enlarge', 106, 0),
(1484, 367, 'Add electricity', 106, 0),
(1485, 367, 'Repair siding', 106, 0),
(1486, 367, 'Add or move windows', 106, 0),
(1487, 367, 'Other', 106, 0),
(1488, 368, 'Storage', 106, 0),
(1489, 368, 'Garage', 106, 0),
(1490, 368, 'Playhouse', 106, 0),
(1491, 368, 'Barn', 106, 0),
(1492, 368, 'Workshop', 106, 0),
(1493, 368, 'Guest House', 106, 0),
(1494, 368, 'Other', 106, 0),
(1495, 368, 'Office', 106, 0),
(1496, 369, 'Wood -- traditionally constructed', 106, 0),
(1497, 369, 'Steel', 106, 0),
(1498, 369, 'Pre-manufactured', 106, 0),
(1499, 369, 'Vinyl siding', 106, 0),
(1500, 369, 'Aluminum', 106, 0),
(1501, 369, 'Steel framed', 106, 0),
(1502, 369, 'Assembled from kit', 106, 0),
(1503, 369, 'Custom', 106, 0),
(1504, 369, 'Don''t Know', 106, 0),
(1505, 369, 'Other', 106, 0),
(1506, 371, 'Sand and apply new sealing', 107, 0),
(1507, 371, 'Sand and repaint', 107, 0),
(1508, 371, 'Add extra pieces/options', 107, 0),
(1509, 371, 'Sand and restain', 107, 0),
(1510, 371, 'Repair/replace damaged parts', 107, 0),
(1511, 371, 'Add/repair electrical or cable', 107, 0),
(1512, 371, 'Other', 107, 0),
(1513, 373, 'Replace poly film', 108, 0),
(1514, 373, 'Replace broken glass', 108, 0),
(1515, 373, 'Replace polycarbonate', 108, 0),
(1516, 373, 'Add/replace shade cloth', 108, 0),
(1517, 373, 'Adjust doors/windows', 108, 0),
(1518, 373, 'Add/adjust heat', 108, 0),
(1519, 373, 'Add/adjust fans', 108, 0),
(1520, 373, 'Need help installing greenhouse kit', 108, 0),
(1521, 373, 'Add second layer of poly film', 108, 0),
(1522, 373, 'Replace fiberglass', 108, 0),
(1523, 373, 'Replace old glazing with polycarbonate', 108, 0),
(1524, 373, 'Add insulation', 108, 0),
(1525, 373, 'Adjust/replace vents', 108, 0),
(1526, 373, 'Add/adjust mister', 108, 0),
(1527, 373, 'Add/adjust lights', 108, 0),
(1528, 373, 'Other', 108, 0),
(1529, 375, 'Brick or Stone Oven', 109, 0),
(1530, 375, 'Cooktop', 109, 0),
(1531, 375, 'Island', 109, 0),
(1532, 375, 'Pergola', 109, 0),
(1533, 375, 'Sink', 109, 0),
(1534, 375, 'Cabinets', 109, 0),
(1535, 375, 'Gas Stove', 109, 0),
(1536, 375, 'Natural Gas Barbeque', 109, 0),
(1537, 375, 'Refrigerator', 109, 0),
(1538, 375, 'Wet Bar', 109, 0),
(1539, 376, 'Standard', 109, 0),
(1540, 376, 'Brick', 109, 0),
(1541, 376, 'Ceramic Tile', 109, 0),
(1542, 376, 'Stone', 109, 0),
(1543, 378, 'Yes', 110, 0),
(1544, 378, 'No', 110, 0),
(1545, 379, 'Bent frame', 110, 0),
(1546, 379, 'Needs to be secured', 110, 0),
(1547, 379, 'Cracked concrete', 110, 0),
(1548, 379, 'Need new floats', 110, 0),
(1549, 379, 'Expansion needed', 110, 0),
(1550, 379, 'Options needed', 110, 0),
(1551, 379, 'Raising and lowering mechanism doesn''t work', 110, 0),
(1552, 379, 'Wood has dry rot', 110, 0),
(1553, 379, 'Surface is damaged', 110, 0),
(1554, 379, 'Need new tires', 110, 0),
(1555, 379, 'Reconfiguration needed', 110, 0),
(1556, 379, 'Other', 110, 0),
(1557, 380, 'Permanent', 110, 0),
(1558, 380, 'Standing', 110, 0),
(1559, 380, 'Suspended', 110, 0),
(1560, 380, 'Portable', 110, 0),
(1561, 380, 'Floating', 110, 0),
(1562, 380, 'Other', 110, 0),
(1563, 382, 'Yes', 111, 0),
(1564, 382, 'No', 111, 0),
(1565, 383, 'Wood framing', 111, 0),
(1566, 383, 'Metal framing', 111, 0),
(1567, 383, 'Don''t Know', 111, 0),
(1568, 384, 'Entire house as part of new home construction', 111, 0),
(1569, 384, 'Addition onto an existing home', 111, 0),
(1570, 384, 'Put a pitch on a roof', 111, 0),
(1571, 384, 'Outbuilding detached from the home', 111, 0),
(1572, 384, 'Second story addition', 111, 0),
(1573, 384, 'Interior framing for remodel project', 111, 0),
(1574, 384, 'Other', 111, 0),
(1575, 386, 'Floor', 112, 0),
(1576, 386, 'Load-bearing interior walls', 112, 0),
(1577, 386, 'Wall sheathing', 112, 0),
(1578, 386, 'Roof framing', 112, 0),
(1579, 386, 'Non-bearing interior walls', 112, 0),
(1580, 386, 'Exterior walls', 112, 0),
(1581, 386, 'Ceiling joists', 112, 0),
(1582, 386, 'Stairs', 112, 0),
(1583, 386, 'Other', 112, 0),
(1584, 387, 'Yes', 112, 0),
(1585, 387, 'No', 112, 0),
(1586, 387, 'Don''t Know', 112, 0),
(1587, 389, 'Master Bedroom', 113, 0),
(1588, 389, 'Hallway', 113, 0),
(1589, 389, 'Garage', 113, 0),
(1590, 389, 'Laundry / utility room', 113, 0),
(1591, 389, 'Bedroom', 113, 0),
(1592, 389, 'Bathroom', 113, 0),
(1593, 389, 'Entryway / foyer', 113, 0),
(1594, 389, 'Basement', 113, 0),
(1595, 389, 'Other', 113, 0),
(1596, 390, 'Built-in shelves', 113, 0),
(1597, 390, 'Lighting', 113, 0),
(1598, 390, 'Wall texture', 113, 0),
(1599, 390, 'Closet rod for hanging clothes', 113, 0),
(1600, 390, 'Mirror', 113, 0),
(1601, 390, 'Other', 113, 0),
(1602, 391, 'Yes', 113, 0),
(1603, 391, 'No', 113, 0),
(1604, 393, 'Deck', 114, 0),
(1605, 393, 'Roof and one floor above', 114, 0),
(1606, 393, 'Roof structure only', 114, 0),
(1607, 393, 'Roof and two floors above', 114, 0),
(1608, 393, 'Don''t Know', 114, 0),
(1609, 393, 'Other', 114, 0),
(1610, 394, 'Above doorway or window opening', 114, 0),
(1611, 394, 'At fireplace opening', 114, 0),
(1612, 394, 'Supporting floor structure', 114, 0),
(1613, 394, 'Above garage door opening', 114, 0),
(1614, 394, 'Supporting hot tub on a deck', 114, 0),
(1615, 394, 'In place of a load-bearing wall', 114, 0),
(1616, 394, 'Other', 114, 0),
(1617, 395, 'Yes', 114, 0),
(1618, 395, 'No', 114, 0),
(1619, 397, 'New construction or addition', 115, 0),
(1620, 397, 'Remodel/retrofit', 115, 0),
(1621, 398, 'Deck', 115, 0),
(1622, 398, 'Roof and one floor above', 115, 0),
(1623, 398, 'Roof structure only', 115, 0),
(1624, 398, 'Roof and two floors above', 115, 0),
(1625, 398, 'Don''t Know', 115, 0),
(1626, 398, 'Other', 115, 0),
(1627, 399, 'Above doorway or window opening', 115, 0),
(1628, 399, 'At fireplace opening', 115, 0),
(1629, 399, 'Supporting floor structure', 115, 0),
(1630, 399, 'Above garage door opening', 115, 0),
(1631, 399, 'Supporting hot tub on a deck', 115, 0),
(1632, 399, 'In place of a load-bearing wall', 115, 0),
(1633, 399, 'Other', 115, 0),
(1634, 401, 'Window casing', 116, 0),
(1635, 401, 'Floor molding', 116, 0),
(1636, 401, 'Ceiling molding - picture and crow', 116, 0),
(1637, 401, 'Stair system', 116, 0),
(1638, 401, 'Cabinetry', 116, 0),
(1639, 401, 'Laminate or wood countertops', 116, 0),
(1640, 401, 'Door casing', 116, 0),
(1641, 401, 'Wall molding - paneling, wainscoting and chair rail', 116, 0),
(1642, 401, 'Crown molding', 116, 0),
(1643, 401, 'Fireplace mantel', 116, 0),
(1644, 401, 'Shelving', 116, 0),
(1645, 401, 'Other', 116, 0),
(1646, 402, 'Interior doors', 116, 0),
(1647, 402, 'Windows', 116, 0),
(1648, 402, 'Exterior doors', 116, 0),
(1649, 402, 'None', 116, 0),
(1650, 403, 'Yes', 116, 0),
(1651, 403, 'No', 116, 0),
(1652, 403, 'Don''t Know', 116, 0),
(1653, 405, 'Install or completely replace exterior trim', 117, 0),
(1654, 405, 'Repair or partially replace exterior trim', 117, 0),
(1655, 406, 'Wood', 117, 0),
(1656, 406, 'Aluminum wrap', 117, 0),
(1657, 406, 'Vinyl', 117, 0),
(1658, 406, 'Fiber cement', 117, 0),
(1659, 406, 'Don''t Know', 117, 0),
(1660, 406, 'Other', 117, 0),
(1661, 407, 'Stucco', 117, 0),
(1662, 407, 'Vinyl', 117, 0),
(1663, 407, 'Asbestos', 117, 0),
(1664, 407, 'Wood panel', 117, 0),
(1665, 407, 'Aluminum', 117, 0),
(1666, 407, 'Brick', 117, 0),
(1667, 407, 'Wood Shingle', 117, 0),
(1668, 407, 'Steel', 117, 0),
(1669, 407, 'Stone', 117, 0),
(1670, 407, 'Other', 117, 0),
(1671, 409, 'Leak through window, wall or into basement', 118, 0),
(1672, 409, 'Trim is twisted, warped, cracked, or split', 118, 0),
(1673, 409, 'Trim is loose or hanging out of position', 118, 0),
(1674, 409, 'Trim is soft, rotten, blistering or peeling', 118, 0),
(1675, 409, 'Other', 118, 0),
(1676, 410, 'Wood', 118, 0),
(1677, 410, 'Vinyl', 118, 0),
(1678, 410, 'Aluminum', 118, 0),
(1679, 410, 'Steel', 118, 0),
(1680, 411, 'Stucco', 118, 0),
(1681, 411, 'Wood Shingle', 118, 0),
(1682, 411, 'Aluminum', 118, 0),
(1683, 411, 'Asbestos', 118, 0),
(1684, 411, 'Wood panel', 118, 0),
(1685, 411, 'Vinyl', 118, 0),
(1686, 411, 'Steel', 118, 0),
(1687, 413, 'Window casing', 119, 0),
(1688, 413, 'Wall molding (paneling, wainscoting, and chair rail)', 119, 0),
(1689, 413, 'Floor molding (baseboard, base shoe, and base cap)', 119, 0),
(1690, 413, 'Cabinetry', 119, 0),
(1691, 413, 'Railing', 119, 0),
(1692, 413, 'Door casing', 119, 0),
(1693, 413, 'Ceiling molding (picture and crown)', 119, 0),
(1694, 413, 'Fireplace mantel', 119, 0),
(1695, 413, 'Crown molding', 119, 0),
(1696, 413, 'Several small interior finish carpentry repair projects', 119, 0),
(1697, 413, 'Other', 119, 0),
(1698, 414, 'Attached remodel or construction', 119, 0),
(1699, 414, 'Accident', 119, 0),
(1700, 414, 'Water damage', 119, 0),
(1701, 414, 'Weather', 119, 0),
(1702, 414, 'Wear and tear', 119, 0),
(1703, 414, 'Pests', 119, 0),
(1704, 414, 'Other', 119, 0),
(1705, 416, 'Replace existing door', 120, 0),
(1706, 416, 'Install where one did not previously exist', 120, 0),
(1707, 416, 'Repair existing door', 120, 0),
(1708, 417, 'Interior', 120, 0),
(1709, 417, 'Exterior', 120, 0),
(1710, 417, 'Exterior sliding door', 120, 0),
(1711, 418, 'Yes', 120, 0),
(1712, 418, 'No', 120, 0),
(1713, 419, 'Yes', 120, 0),
(1714, 419, 'No', 120, 0),
(1715, 421, 'Replace an old one of the same type', 121, 0),
(1716, 421, 'Replace an old one with different type', 121, 0),
(1717, 421, 'Install where one did not previously exist', 121, 0),
(1718, 421, 'Repair existing door', 121, 0),
(1719, 422, 'Interior', 121, 0),
(1720, 422, 'Exterior', 121, 0),
(1721, 422, 'Exterior sliding door', 121, 0),
(1722, 423, 'Yes', 121, 0),
(1723, 423, 'No', 121, 0),
(1724, 424, 'Yes', 121, 0),
(1725, 424, 'No', 121, 0),
(1726, 426, 'Door installation / replacement', 122, 0),
(1727, 426, 'Door repair', 122, 0),
(1728, 427, 'Exterior', 122, 0),
(1729, 427, 'Sliding', 122, 0),
(1730, 427, 'Garage', 122, 0),
(1731, 427, 'Secrurity gate', 122, 0),
(1732, 427, 'Interior', 122, 0),
(1733, 427, 'Storm door', 122, 0),
(1734, 427, 'Glass shower door', 122, 0),
(1735, 428, 'Yes', 122, 0),
(1736, 428, 'No', 122, 0),
(1737, 429, 'Home/Residence', 122, 0),
(1738, 429, 'Business', 122, 0),
(1739, 431, 'Home/Residence', 123, 0),
(1740, 431, 'Business', 123, 0),
(1741, 432, 'Yes', 123, 0),
(1742, 432, 'No', 123, 0),
(1743, 433, 'Interior', 123, 0),
(1744, 433, 'French', 123, 0),
(1745, 433, 'Storm and/or screen', 123, 0),
(1746, 433, 'Wood composite', 123, 0),
(1747, 433, 'Steel', 123, 0),
(1748, 433, 'Glass', 123, 0),
(1749, 433, 'Exterior', 123, 0),
(1750, 433, 'Sliding', 123, 0),
(1751, 433, 'Wood', 123, 0),
(1752, 433, 'Vinyl', 123, 0),
(1753, 433, 'Fiberglass', 123, 0),
(1754, 433, 'Other', 123, 0),
(1755, 434, 'Lockset', 123, 0),
(1756, 434, 'Deadbolt', 123, 0),
(1757, 434, 'Screen', 123, 0),
(1758, 434, 'Frame', 123, 0),
(1759, 434, 'Difficult to open', 123, 0),
(1760, 434, 'Door won''t stay closed', 123, 0),
(1761, 434, 'Door is splitting', 123, 0),
(1762, 434, 'Weather stripping', 123, 0),
(1763, 434, 'Cracked glass', 123, 0),
(1764, 434, 'Knob', 123, 0),
(1765, 434, 'Hinge', 123, 0),
(1766, 434, 'Threshold', 123, 0),
(1767, 434, 'Stuck open or closed', 123, 0),
(1768, 434, 'Door sticks', 123, 0),
(1769, 434, 'Door is cracking', 123, 0),
(1770, 434, 'Door won''t clear new flooring', 123, 0),
(1771, 434, 'Track for sliding door', 123, 0),
(1772, 434, 'Moisture between panes of insulated glass', 123, 0),
(1773, 434, 'Other', 123, 0),
(1774, 436, 'Build or replace a deck', 124, 0),
(1775, 436, 'Repair an existing deck', 124, 0),
(1776, 437, 'Built-in benches', 124, 0),
(1777, 437, 'Custom railings', 124, 0),
(1778, 437, 'Multi-level deck', 124, 0),
(1779, 437, 'Electrical features', 124, 0),
(1780, 437, 'Waterproof to protect area below', 124, 0),
(1781, 437, 'Pergola/Arbor/Patio Cover', 124, 0),
(1782, 437, 'Planters/Storage boxes', 124, 0),
(1783, 437, 'Screened-in deck', 124, 0),
(1784, 437, 'Hot tub/Spa', 124, 0),
(1785, 437, 'Built-in barbeque', 124, 0),
(1786, 439, 'Yes', 125, 0),
(1787, 439, 'No', 125, 0),
(1788, 440, 'Deck is unstable', 125, 0),
(1789, 440, 'Stairs/Railings not to code', 125, 0),
(1790, 440, 'Discolored/Mildewed decking', 125, 0),
(1791, 440, 'Waterproofing', 125, 0),
(1792, 440, 'Re-staining', 125, 0),
(1793, 440, 'Faulty or broken stairs/railing', 125, 0),
(1794, 440, 'Loose decking or stairs', 125, 0),
(1795, 440, 'Rotted decking or stairs', 125, 0),
(1796, 440, 'Arbor/Pergola is damaged', 125, 0),
(1797, 440, 'Other', 125, 0),
(1798, 441, 'Redwood', 125, 0),
(1799, 441, 'Pressure-treated pine', 125, 0),
(1800, 441, 'Vinyl', 125, 0),
(1801, 441, 'Don''t know', 125, 0),
(1802, 441, 'Cedar', 125, 0),
(1803, 441, 'Composite/Synthetic materials', 125, 0),
(1804, 441, 'Other', 125, 0),
(1805, 442, '', 126, 0),
(1806, 443, 'Front door', 126, 0),
(1807, 443, 'Side door', 126, 0),
(1808, 443, 'Back door', 126, 0),
(1809, 443, 'Indoors', 126, 0),
(1810, 443, 'Other', 126, 0),
(1811, 444, 'Use a walker', 126, 0),
(1812, 444, 'Use motorized scooter', 126, 0),
(1813, 444, 'Use motorized wheelchair', 126, 0),
(1814, 444, 'Use manual wheelchair', 126, 0),
(1815, 445, 'Wood', 126, 0),
(1816, 445, 'Metal / Aluminum', 126, 0),
(1817, 445, 'Don''t Know', 126, 0),
(1818, 445, 'Concrete', 126, 0),
(1819, 445, 'Other', 126, 0),
(1820, 447, 'Arbor', 127, 0),
(1821, 447, 'Trellis', 127, 0),
(1822, 447, 'Pergola', 127, 0),
(1823, 447, 'Would like recommendation', 127, 0),
(1824, 448, 'Sketches and/or basic idea of project', 127, 0),
(1825, 448, 'Professional drawings (complete set of plans)', 127, 0),
(1826, 448, 'None', 127, 0),
(1827, 449, 'New addition for new landscape design', 127, 0),
(1828, 449, 'New addition for existing landscape', 127, 0),
(1829, 449, 'New structure for covering deck, patio, walkway, etc.', 127, 0),
(1830, 449, 'Replacing existing structure', 127, 0),
(1831, 450, 'Redwood', 127, 0),
(1832, 450, 'Pressure treated pine', 127, 0),
(1833, 450, 'Would like recommendation', 127, 0),
(1834, 450, 'Cedar', 127, 0),
(1835, 450, 'Synthetic/Composite material', 127, 0),
(1836, 452, 'Yes', 128, 0),
(1837, 452, 'No', 128, 0),
(1838, 453, 'Wood', 128, 0),
(1839, 453, 'Concrete', 128, 0),
(1840, 453, 'Wrought iron rails', 128, 0),
(1841, 453, 'Metal', 128, 0),
(1842, 453, 'Wood rails', 128, 0),
(1843, 453, 'Metal tube rails', 128, 0),
(1844, 453, 'Don''t know', 128, 0),
(1845, 453, 'Other', 128, 0),
(1846, 454, 'Ramp', 128, 0),
(1847, 454, 'Metal handrails/pickets', 128, 0),
(1848, 454, 'Add non-skid surface', 128, 0),
(1849, 454, 'Change length or direction', 128, 0),
(1850, 454, 'Wooden handrails/pickets', 128, 0),
(1851, 454, 'Add/replace handrails', 128, 0),
(1852, 454, 'Change slope', 128, 0),
(1853, 454, 'Paint', 128, 0),
(1854, 454, 'Other', 128, 0),
(1855, 455, 'Front door', 128, 0),
(1856, 455, 'Side door', 128, 0),
(1857, 455, 'Back door', 128, 0),
(1858, 455, 'Indoors', 128, 0),
(1859, 455, 'Other', 128, 0),
(1860, 456, 'With walker', 128, 0),
(1861, 456, 'With motorized wheelchair', 128, 0),
(1862, 456, 'With motorized scooter', 128, 0),
(1863, 456, 'With manual wheelchair', 128, 0),
(1864, 456, 'Other', 128, 0),
(1865, 457, 'Home/Residence', 128, 0),
(1866, 457, 'Business', 128, 0),
(1867, 459, 'Permanent', 129, 0),
(1868, 459, 'Standing', 129, 0),
(1869, 459, 'Suspended', 129, 0),
(1870, 459, 'Want recommendation', 129, 0),
(1871, 459, 'Portable', 129, 0),
(1872, 459, 'Floating', 129, 0),
(1873, 459, 'Other', 129, 0),
(1874, 460, 'Lake', 129, 0),
(1875, 460, 'Ocean', 129, 0),
(1876, 460, 'Deep water (10+ feet)', 129, 0),
(1877, 460, 'Soft bottom', 129, 0),
(1878, 460, 'Sandy bottom', 129, 0),
(1879, 460, 'River', 129, 0),
(1880, 460, 'Strong current', 129, 0),
(1881, 460, 'Major tide changes (5-10+ feet)', 129, 0),
(1882, 460, 'Rocky bottom', 129, 0),
(1883, 460, 'Other', 129, 0),
(1884, 462, 'Replace existing fence', 130, 0),
(1885, 462, 'Repair existing fence', 130, 0),
(1886, 462, 'Install a new fence', 130, 0),
(1887, 463, 'Keep pets contained', 130, 0),
(1888, 463, 'Keep children safely in yard', 130, 0),
(1889, 463, 'Property boundary', 130, 0),
(1890, 463, 'Provide security', 130, 0),
(1891, 463, 'Wind or noise buffer', 130, 0),
(1892, 463, 'Contain livestock', 130, 0),
(1893, 463, 'Attractive yard addition', 130, 0),
(1894, 463, 'Provide privacy', 130, 0),
(1895, 463, 'Other', 130, 0),
(1896, 465, 'Fence is damaged', 131, 0),
(1897, 465, 'Post is damaged', 131, 0),
(1898, 465, 'Fence is loose', 131, 0),
(1899, 465, 'Fence is rusty', 131, 0),
(1900, 465, 'Other', 131, 0),
(1901, 465, 'Gate is sagging', 131, 0),
(1902, 465, 'Hardware is damaged/broken/missing', 131, 0),
(1903, 465, 'Post is wobbly', 131, 0),
(1904, 466, '4-6 feet', 131, 0),
(1905, 466, 'Less than 4 feet', 131, 0),
(1906, 466, '6-8 feet', 131, 0),
(1907, 466, 'Greater than 8 feet', 131, 0);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  `image` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `desc`, `status`, `order`, `image`) VALUES
(1, 'service Provider Home page Banner', '<p>\r\n	service Provider Home page Banner description</p>\r\n', 0, 1, '915202117_pro_banner.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `blockips`
--

CREATE TABLE IF NOT EXISTS `blockips` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE IF NOT EXISTS `bookings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_provider_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` enum('P','A','R','C') NOT NULL COMMENT 'P="pending" ,A="Accept","R"=Reject,"C"=Cancel'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `user_id`, `service_provider_id`, `service_id`, `category_id`, `address`, `state`, `city`, `zip`, `start_date`, `end_date`, `amount`, `status`) VALUES
(1, 11, 16, 5, 1, 'Sukantanagar,Saltlake,Sector-iv,kolkata,700098', 'West Bengal ', 'Kolkata', '700098', '2017-04-12', '2017-04-12', 100, 'A'),
(2, 12, 17, 6, 5, 'Gate2,Rajarhat,Newtown,kolkata,700100', 'West Bengal ', 'Kolkata', '700098', '2017-04-13', '2017-04-13', 80, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`, `status`) VALUES
(2, 'Manage a Event', '58ff294088faf.jpg', 1),
(3, 'MEAL PREPARATION', '58ee0d0681462.jpg', 1),
(4, 'TRANSPORTATION', '58ee0d11b8fd6.jpg', 1),
(5, 'Miscellaneous', '58ee0d1a130e1.jpg', 1),
(7, 'Moving to Home', '58fdab592e0a1.jpeg', 1),
(8, 'Planning a Wedding', '58ff282f6347f.jpg', 1),
(9, 'test', '591973f3bc152.jpg', 1),
(10, 'Bathroom Remodeling and Design', '59197c36b1a6f.jpg', 1),
(11, 'Cabinets', '591991605c2f1.jpg', 1),
(12, 'Countertops', '5919a24fe6246.jpg', 1),
(13, 'Electrical', '5919adb0c5dd8.jpg', 1),
(14, 'Flooring', '591a94d0cba53.jpg', 1),
(15, 'Glass & Mirrors', '591aa67f8690d.jpg', 1),
(16, 'Hot Tubs & Spas', '591ab1d30454c.jpg', 1),
(17, 'Painting & Staining', '591bf0b6d1051.jpg', 1),
(18, 'Plumbing', '591bfbc445186.jpg', 1),
(19, 'Tile', '591bfc4532b7a.jpg', 1),
(20, 'Walls & Ceilings', '591c02814a1f9.jpg', 1),
(21, 'Windows & Doors', '591c11a7539fa.jpg', 1),
(22, 'Closets', '591c1a507bd1a.jpg', 1),
(23, 'Stairs & Railings', '591c240edd573.jpg', 1),
(24, 'Siding, Gutters & Exterior Trim', '591c28d5351a8.jpg', 1),
(25, 'Outbuildings, Sheds & Gazebos', '591c3047377a2.jpg', 1),
(26, 'Framing', '591c37187a155.jpg', 1),
(27, 'Finish Carpentry, Trim & Molding', '591c3d4ac4324.jpg', 1),
(28, 'Doors', '591c3ff9184f5.jpg', 1),
(29, 'Decks, Fences & Ramps', '591c43103b65f.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE IF NOT EXISTS `cms_pages` (
  `id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `page_description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `page_title`, `page_description`, `slug`, `status`) VALUES
(3, 'Terms of use', '<p>\r\n	Test Content</p>\r\n', '', 1),
(4, 'About', 'About us content will be here', '', 1),
(5, 'Privacy', '<p>\r\n	<span style="font-size:16px;"><span style="font-family:comic sans ms,cursive;"><span style="background-color:#ff8c00;">Content coming soon</span></span></span></p>\r\n', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` longtext NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `message`, `status`) VALUES
(1, 'Subhradip Jana', 'nits.subhradip@gmail.com', 'Test Test TestTest', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'AS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua And Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas The'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CD', 'Congo The Democratic Republic Of The'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'CI', 'Cote D''Ivoire (Ivory Coast)'),
(54, 'HR', 'Croatia (Hrvatska)'),
(55, 'CU', 'Cuba'),
(56, 'CY', 'Cyprus'),
(57, 'CZ', 'Czech Republic'),
(58, 'DK', 'Denmark'),
(59, 'DJ', 'Djibouti'),
(60, 'DM', 'Dominica'),
(61, 'DO', 'Dominican Republic'),
(62, 'TP', 'East Timor'),
(63, 'EC', 'Ecuador'),
(64, 'EG', 'Egypt'),
(65, 'SV', 'El Salvador'),
(66, 'GQ', 'Equatorial Guinea'),
(67, 'ER', 'Eritrea'),
(68, 'EE', 'Estonia'),
(69, 'ET', 'Ethiopia'),
(70, 'XA', 'External Territories of Australia'),
(71, 'FK', 'Falkland Islands'),
(72, 'FO', 'Faroe Islands'),
(73, 'FJ', 'Fiji Islands'),
(74, 'FI', 'Finland'),
(75, 'FR', 'France'),
(76, 'GF', 'French Guiana'),
(77, 'PF', 'French Polynesia'),
(78, 'TF', 'French Southern Territories'),
(79, 'GA', 'Gabon'),
(80, 'GM', 'Gambia The'),
(81, 'GE', 'Georgia'),
(82, 'DE', 'Germany'),
(83, 'GH', 'Ghana'),
(84, 'GI', 'Gibraltar'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'XU', 'Guernsey and Alderney'),
(92, 'GN', 'Guinea'),
(93, 'GW', 'Guinea-Bissau'),
(94, 'GY', 'Guyana'),
(95, 'HT', 'Haiti'),
(96, 'HM', 'Heard and McDonald Islands'),
(97, 'HN', 'Honduras'),
(98, 'HK', 'Hong Kong S.A.R.'),
(99, 'HU', 'Hungary'),
(100, 'IS', 'Iceland'),
(101, 'IN', 'India'),
(102, 'ID', 'Indonesia'),
(103, 'IR', 'Iran'),
(104, 'IQ', 'Iraq'),
(105, 'IE', 'Ireland'),
(106, 'IL', 'Israel'),
(107, 'IT', 'Italy'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'XJ', 'Jersey'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea North'),
(116, 'KR', 'Korea South'),
(117, 'KW', 'Kuwait'),
(118, 'KG', 'Kyrgyzstan'),
(119, 'LA', 'Laos'),
(120, 'LV', 'Latvia'),
(121, 'LB', 'Lebanon'),
(122, 'LS', 'Lesotho'),
(123, 'LR', 'Liberia'),
(124, 'LY', 'Libya'),
(125, 'LI', 'Liechtenstein'),
(126, 'LT', 'Lithuania'),
(127, 'LU', 'Luxembourg'),
(128, 'MO', 'Macau S.A.R.'),
(129, 'MK', 'Macedonia'),
(130, 'MG', 'Madagascar'),
(131, 'MW', 'Malawi'),
(132, 'MY', 'Malaysia'),
(133, 'MV', 'Maldives'),
(134, 'ML', 'Mali'),
(135, 'MT', 'Malta'),
(136, 'XM', 'Man (Isle of)'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'YT', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia'),
(144, 'MD', 'Moldova'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'MS', 'Montserrat'),
(148, 'MA', 'Morocco'),
(149, 'MZ', 'Mozambique'),
(150, 'MM', 'Myanmar'),
(151, 'NA', 'Namibia'),
(152, 'NR', 'Nauru'),
(153, 'NP', 'Nepal'),
(154, 'AN', 'Netherlands Antilles'),
(155, 'NL', 'Netherlands The'),
(156, 'NC', 'New Caledonia'),
(157, 'NZ', 'New Zealand'),
(158, 'NI', 'Nicaragua'),
(159, 'NE', 'Niger'),
(160, 'NG', 'Nigeria'),
(161, 'NU', 'Niue'),
(162, 'NF', 'Norfolk Island'),
(163, 'MP', 'Northern Mariana Islands'),
(164, 'NO', 'Norway'),
(165, 'OM', 'Oman'),
(166, 'PK', 'Pakistan'),
(167, 'PW', 'Palau'),
(168, 'PS', 'Palestinian Territory Occupied'),
(169, 'PA', 'Panama'),
(170, 'PG', 'Papua new Guinea'),
(171, 'PY', 'Paraguay'),
(172, 'PE', 'Peru'),
(173, 'PH', 'Philippines'),
(174, 'PN', 'Pitcairn Island'),
(175, 'PL', 'Poland'),
(176, 'PT', 'Portugal'),
(177, 'PR', 'Puerto Rico'),
(178, 'QA', 'Qatar'),
(179, 'RE', 'Reunion'),
(180, 'RO', 'Romania'),
(181, 'RU', 'Russia'),
(182, 'RW', 'Rwanda'),
(183, 'SH', 'Saint Helena'),
(184, 'KN', 'Saint Kitts And Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'PM', 'Saint Pierre and Miquelon'),
(187, 'VC', 'Saint Vincent And The Grenadines'),
(188, 'WS', 'Samoa'),
(189, 'SM', 'San Marino'),
(190, 'ST', 'Sao Tome and Principe'),
(191, 'SA', 'Saudi Arabia'),
(192, 'SN', 'Senegal'),
(193, 'RS', 'Serbia'),
(194, 'SC', 'Seychelles'),
(195, 'SL', 'Sierra Leone'),
(196, 'SG', 'Singapore'),
(197, 'SK', 'Slovakia'),
(198, 'SI', 'Slovenia'),
(199, 'XG', 'Smaller Territories of the UK'),
(200, 'SB', 'Solomon Islands'),
(201, 'SO', 'Somalia'),
(202, 'ZA', 'South Africa'),
(203, 'GS', 'South Georgia'),
(204, 'SS', 'South Sudan'),
(205, 'ES', 'Spain'),
(206, 'LK', 'Sri Lanka'),
(207, 'SD', 'Sudan'),
(208, 'SR', 'Suriname'),
(209, 'SJ', 'Svalbard And Jan Mayen Islands'),
(210, 'SZ', 'Swaziland'),
(211, 'SE', 'Sweden'),
(212, 'CH', 'Switzerland'),
(213, 'SY', 'Syria'),
(214, 'TW', 'Taiwan'),
(215, 'TJ', 'Tajikistan'),
(216, 'TZ', 'Tanzania'),
(217, 'TH', 'Thailand'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad And Tobago'),
(222, 'TN', 'Tunisia'),
(223, 'TR', 'Turkey'),
(224, 'TM', 'Turkmenistan'),
(225, 'TC', 'Turks And Caicos Islands'),
(226, 'TV', 'Tuvalu'),
(227, 'UG', 'Uganda'),
(228, 'UA', 'Ukraine'),
(229, 'AE', 'United Arab Emirates'),
(230, 'GB', 'United Kingdom'),
(231, 'US', 'United States'),
(232, 'UM', 'United States Minor Outlying Islands'),
(233, 'UY', 'Uruguay'),
(234, 'UZ', 'Uzbekistan'),
(235, 'VU', 'Vanuatu'),
(236, 'VA', 'Vatican City State (Holy See)'),
(237, 'VE', 'Venezuela'),
(238, 'VN', 'Vietnam'),
(239, 'VG', 'Virgin Islands (British)'),
(240, 'VI', 'Virgin Islands (US)'),
(241, 'WF', 'Wallis And Futuna Islands'),
(242, 'EH', 'Western Sahara'),
(243, 'YE', 'Yemen'),
(244, 'YU', 'Yugoslavia'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `subject`, `content`, `status`) VALUES
(1, 'Forgot Password', '<meta charset="utf-8" />\r\n<title></title>\r\n<table border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, sans-serif; font-size:14px; color: #525252;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="500">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#F66296" height="80" style="padding: 5px 0">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td style="text-align:center;" width="100%">\r\n												<img alt="" height="56" src="http://104.131.83.218/nail/app/webroot/images/logo.png" width="170" /></td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-size:18px; color:#454545">Hi,</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Please reset your password from the following link.</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td style="font-weight: bold">\r\n																LINK: [LINK]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Thank You</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																The ServicePro Team</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#322140 " height="40">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																<font style="color: #fff;">Copyright &copy; 2017 company name</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1),
(2, 'Registration', '<meta charset="utf-8" />\r\n<title></title>\r\n<table border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, sans-serif; font-size:14px; color: #525252;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="500">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#F66296" height="80" style="padding: 5px 0">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td style="text-align:center;" width="100%">\r\n												<img alt="" height="56" src="http://104.131.83.218/nail/app/webroot/images/logo.png" width="170" /></td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-size:18px; color:#454545">Hi,</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Youe account has been successfully registered.</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Email: [EMAIL]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Password: [PASSWORD]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Phone Number: [PHONE]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td style="font-weight: bold">\r\n																LINK: [LINK]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Thank You</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																The ServicePro Team</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#322140 " height="40">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																<font style="color: #fff;">Copyright &copy; 2017 company name</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1),
(6, 'Active Account', '<meta charset="utf-8" />\r\n<title></title>\r\n<table border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, sans-serif; font-size:14px; color: #525252;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="500">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#F66296" height="80" style="padding: 5px 0">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td style="text-align:center;" width="100%">\r\n												<img alt="" height="56" src="http://104.131.83.218/nail/app/webroot/images/logo.png" width="170" /></td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-size:18px; color:#454545">Hi [NAME],</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font>You account has been successfull activated you can login your account:-</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font>Thanks for loving ServicePro</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																The ServicePro Team</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#322140 " height="40">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																<font style="color: #fff;">Copyright &copy; 2017 company name</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1),
(9, 'Reply', '<p>\r\n	&nbsp;</p>\r\n<meta charset="utf-8" />\r\n<title></title>\r\n<br />\r\n<table border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, sans-serif; font-size:14px; color: #525252;" width="100%">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n				<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="500">\r\n					<tbody>\r\n						<tr>\r\n							<td bgcolor="#F66296" height="80" style="padding: 5px 0">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td style="text-align:center;" width="100%">\r\n												<img alt="" height="56" src="http://104.131.83.218/nail/app/webroot/images/logo.png" width="170" /></td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																<font style="font-size:18px; color:#454545">Hi,</font></td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Reply:</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																[CONTENT]</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																&nbsp;</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																Thank You</td>\r\n														</tr>\r\n														<tr>\r\n															<td>\r\n																The ServicePro Team</td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%;">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n						<tr>\r\n							<td>\r\n								&nbsp;</td>\r\n						</tr>\r\n						<tr>\r\n							<td bgcolor="#322140 " height="40">\r\n								<table border="0" cellpadding="0" cellspacing="0" width="500">\r\n									<tbody>\r\n										<tr>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n											<td width="90%">\r\n												<table border="0" cellpadding="0" cellspacing="0" width="100%">\r\n													<tbody>\r\n														<tr>\r\n															<td>\r\n																<font style="color: #fff;">Copyright &copy; 2017 company name</font></td>\r\n														</tr>\r\n													</tbody>\r\n												</table>\r\n											</td>\r\n											<td width="5%">\r\n												&nbsp;</td>\r\n										</tr>\r\n									</tbody>\r\n								</table>\r\n							</td>\r\n						</tr>\r\n					</tbody>\r\n				</table>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eventnotifications`
--

CREATE TABLE IF NOT EXISTS `eventnotifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eventnotifications`
--

INSERT INTO `eventnotifications` (`id`, `user_id`, `device_id`, `device_type`) VALUES
(1, 24, 'abcdefghij', 'A'),
(3, 26, 'aaaaaaaaaaaaaa', 'A'),
(5, 27, 'wwwwwwwww', 'A'),
(6, 14, 'hhhhhhhhhhhh', 'A'),
(7, 30, '23568979446464444sawdasdasaaddadad', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answar` longtext NOT NULL,
  `sattus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answar`, `sattus`) VALUES
(1, 'Test Question', 'Test Answar', 1),
(4, 'Question 1', 'Answer 1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE IF NOT EXISTS `leads` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `amount` int(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0="pending" ,1="Accept","2"=Reject,"3"=Cancel'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `user_id`, `service_id`, `category_id`, `address`, `state`, `city`, `zip`, `start_date`, `end_date`, `amount`, `status`) VALUES
(1, 11, 5, 1, 'Sukantanagar,Saltlake,Sector-iv,kolkata,700098', 'West Bengal ', 'Kolkata', '700098', '2017-04-12', '2017-04-12', 0, 0),
(2, 12, 6, 5, 'Gate2,Rajarhat,Newtown,kolkata,700100', 'West Bengal ', 'Kolkata', '700098', '2017-04-13', '2017-04-13', 0, 1),
(3, 28, 8, 8, 'Gate2,Rajarhat,Newtown,kolkata,700100', 'West Bengal ', 'Kolkata', '700098', '2017-07-1', '2017-07-15', 0, 1),
(4, 28, 7, 2, 'Gate2,Rajarhat,Newtown,kolkata,700100', 'West Bengal ', 'Kolkata', '700098', '2017-07-1', '2017-07-15', 0, 1),
(5, 28, 9, 8, 'Gate2,Rajarhat,Newtown,kolkata,700100', 'West Bengal ', 'Kolkata', '700098', '2017-07-1', '2017-07-15', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `leftsideactions`
--

CREATE TABLE IF NOT EXISTS `leftsideactions` (
  `id` int(11) NOT NULL,
  `leftaction` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leftsideactions`
--

INSERT INTO `leftsideactions` (`id`, `leftaction`, `status`) VALUES
(1, 'User', 1),
(2, 'Category', 1),
(3, 'Advertisement', 1),
(4, 'Charity', 1),
(5, 'Gift Card Category', 1),
(6, 'Gift Card', 1),
(7, 'Faq', 1),
(8, 'Email Template', 1),
(9, 'Contents', 1);

-- --------------------------------------------------------

--
-- Table structure for table `membership_plans`
--

CREATE TABLE IF NOT EXISTS `membership_plans` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `no_of_post` int(11) NOT NULL,
  `no_of_marketplace` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `content` text NOT NULL,
  `plan_code` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `membership_plans`
--

INSERT INTO `membership_plans` (`id`, `title`, `no_of_post`, `no_of_marketplace`, `price`, `duration`, `content`, `plan_code`) VALUES
(3, 'Basic', 30, 10, 100, 1, '<p class="plan-desc">\n	Lorem ipsum dolor sit onse</p>\n<p class="plan-desc">\n	Dolor sit onsectetur adipis</p>\n<p class="plan-desc">\n	Ut enim ad minim veniam, quis nostrud exercitation</p>\n<p class="plan-desc">\n	Lorem ipsum dolor sit onse</p>\n<p class="plan-desc">\n	Dolor sit onsectetur adipis</p>\n<p class="plan-desc">\n	Ut enim ad minim veniam, quis nostrud exercitation</p>\n', 'PLN_44b2m5hpe4www5v'),
(4, 'Gold', 50, 30, 200, 3, '<p class="plan-desc">\r\n	Lorem ipsum dolor sit onse</p>\r\n<p class="plan-desc">\r\n	Dolor sit onsectetur adipis</p>\r\n<p class="plan-desc">\r\n	Ut enim ad minim veniam, quis nostrud exercitation</p>\r\n<p class="plan-desc">\r\n	Lorem ipsum dolor sit onse</p>\r\n<p class="plan-desc">\r\n	Dolor sit onsectetur adipis</p>\r\n<p class="plan-desc">\r\n	Ut enim ad minim veniam, quis nostrud exercitation</p>\r\n', 'PLN_z4rw6wbjf5k1p1y'),
(5, 'Silver', 40, 20, 300, 2, '<p class="plan-desc">\r\n	Lorem ipsum dolor sit onse</p>\r\n<p class="plan-desc">\r\n	Dolor sit onsectetur adipis</p>\r\n<p class="plan-desc">\r\n	Ut enim ad minim veniam, quis nostrud exercitation</p>\r\n<p class="plan-desc">\r\n	Lorem ipsum dolor sit onse</p>\r\n<p class="plan-desc">\r\n	Dolor sit onsectetur adipis</p>\r\n<p class="plan-desc">\r\n	Ut enim ad minim veniam, quis nostrud exercitation</p>\r\n', 'PLN_wlkjokkxdvywn6v');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(255) NOT NULL,
  `msg` varchar(1000) NOT NULL,
  `sent_by` varchar(100) NOT NULL,
  `sent_to` varchar(100) NOT NULL,
  `service_id` int(255) NOT NULL,
  `lead_id` int(255) NOT NULL,
  `dt_pub_date` datetime NOT NULL,
  `is_read` int(11) NOT NULL,
  `thread` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `msg`, `sent_by`, `sent_to`, `service_id`, `lead_id`, `dt_pub_date`, `is_read`, `thread`) VALUES
(21, 'Hi ', '11', '26', 6, 1, '2017-04-20 12:04:02', 0, '1385925212'),
(22, 'I can do this job. My fees 200$', '26', '11', 6, 1, '2017-04-20 12:04:26', 0, '1385925212');

-- --------------------------------------------------------

--
-- Table structure for table `plants`
--

CREATE TABLE IF NOT EXISTS `plants` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `image` text NOT NULL,
  `request` int(255) NOT NULL,
  `job` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plants`
--

INSERT INTO `plants` (`id`, `name`, `description`, `price`, `duration`, `image`, `request`, `job`) VALUES
(24, 'Gold', 'Gold Plan', 100, 10, '', 20, 20),
(25, 'Silver', '<p>\r\n	Silver plan</p>\r\n', 50, 6, '', 20, 20),
(28, 'Platinum', '<p>\r\n	Platinum Plan</p>\r\n', 2000, 30, '', 50, 50);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `description` longtext,
  `status` int(11) NOT NULL DEFAULT '1',
  `icon` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_feature` int(11) DEFAULT NULL,
  `is_popular` int(11) DEFAULT NULL,
  `post_order` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `cat_id`, `description`, `status`, `icon`, `image`, `is_feature`, `is_popular`, `post_order`) VALUES
(5, 17, 'Home cleaning', 7, NULL, 1, '58fdf197f2ea0.jpeg', '58fdf197f2d18.jpeg', 1, 1, 2),
(6, 16, 'Interior Decoration', 7, NULL, 1, '58fee03b64bd2.png', '58fee03b63fff.jpg', 1, 1, 1),
(7, NULL, 'DJ', 2, NULL, 1, '', '58ff235eb9e47.jpg', 0, 0, 0),
(8, NULL, 'Wedding photography', 8, NULL, 1, '', '58ff2a172ba55.jpeg', 0, 0, 0),
(9, NULL, 'Wedding event and makeup', 8, NULL, 1, '', '58ff2b710945c.jpg', 0, 0, 0),
(11, NULL, 'Bathroom - Remodel', 10, NULL, 1, '', '', 0, 0, 0),
(12, NULL, 'Bathtub Install or Replace', 10, NULL, 1, '', '', 0, 0, 0),
(13, NULL, 'Bathtub Liner or Shower Surround - Install or Replace', 10, NULL, 1, '', '', 0, 0, 0),
(14, NULL, 'Shower Install or Replace', 10, NULL, 1, '', '', 0, 0, 0),
(15, NULL, 'Architect', 10, NULL, 1, '', '', 0, 0, 0),
(16, NULL, 'Bathtub Resurfacing', 10, NULL, 1, '', '', 0, 0, 0),
(17, NULL, 'Cabinets', 10, NULL, 1, '', '', 0, 0, 0),
(18, NULL, 'Cabinets - Install', 11, NULL, 1, '', '', 0, 0, 0),
(19, NULL, 'What is the location of your project? (Enter Zip Code) *', 11, NULL, 1, '', '', 0, 0, 0),
(20, NULL, 'Install Cabinets', 11, NULL, 1, '', '', 0, 0, 0),
(21, NULL, 'Cabinets - Reface', 11, NULL, 1, '', '', 0, 0, 0),
(22, NULL, 'Pre-Made Cabinets - Install', 10, NULL, 1, '', '', 0, 0, 0),
(23, NULL, 'Built-In Furniture - Construct', 11, NULL, 1, '', '', 0, 0, 0),
(24, NULL, 'Cabinets - Refinish', 11, NULL, 1, '', '', 0, 0, 0),
(25, NULL, 'Cabinets - Repair', 11, NULL, 1, '', '', 0, 0, 0),
(26, NULL, 'Custom Cabinets - Build', 11, NULL, 1, '', '', 0, 0, 0),
(27, NULL, 'Laminate Countertops - Install', 12, NULL, 1, '', '', 0, 0, 0),
(28, NULL, 'Stone Slab Countertops - Install (Granite, Marble, Quartz, etc)', 12, NULL, 1, '', '', 0, 0, 0),
(29, NULL, 'Bathtub or Other Bath or Kitchen Surface - Resurface or Reface', 12, NULL, 1, '', '', 0, 0, 0),
(30, NULL, 'Solid Surface Countertops - Install (Concrete, Stainless Steel, etc)', 12, NULL, 1, '', '', 0, 0, 0),
(31, NULL, 'Tile: Ceramic and Porcelain - Install', 12, NULL, 1, '', '', 0, 0, 0),
(32, NULL, 'Tile: Natural Stone (Granite, Marble, Slate, Quartz, etc) - Install', 12, NULL, 1, '', '', 0, 0, 0),
(33, NULL, 'Electrical Switches, Outlets & Fixtures - Install or Repair', 13, NULL, 1, '', '', 0, 0, 0),
(34, NULL, 'Electrical for Home Addition or Remodel - Install', 13, NULL, 1, '', '', 0, 0, 0),
(35, NULL, 'Designer - Interior Lighting Plan', 13, NULL, 1, '', '', 0, 0, 0),
(36, NULL, 'Electric Vehicle Charging Station Installation', 13, NULL, 1, '', '', 0, 0, 0),
(37, NULL, 'Electrical Wiring or Panel Upgrade', 13, NULL, 1, '', '', 0, 0, 0),
(38, NULL, 'Home Automation System - Install or Service', 13, NULL, 1, '', '', 0, 0, 0),
(39, NULL, 'Tile: Ceramic and Porcelain - Install', 14, NULL, 1, '', '', 0, 0, 0),
(40, NULL, 'Wood Flooring - Install or Completely Replace', 14, NULL, 1, '', '', 0, 0, 0),
(41, NULL, 'Wood Flooring - Refinish', 14, NULL, 1, '', '', 0, 0, 0),
(42, NULL, 'Carpet - Install', 14, NULL, 1, '', '', 0, 0, 0),
(43, NULL, 'Laminate Wood or Stone Flooring - Install', 14, NULL, 1, '', '', 0, 0, 0),
(44, NULL, 'Tile: Natural Stone (Granite, Marble, Slate, Quartz, etc) - Install', 14, NULL, 1, '', '', 0, 0, 0),
(45, NULL, 'Vinyl or Linoleum Sheet Flooring or Tiles - Install', 14, NULL, 1, '', '', 0, 0, 0),
(46, NULL, 'Glass Shower Door or Enclosure - Install', 15, NULL, 1, '', '', 0, 0, 0),
(47, NULL, 'Mirrors - Install or Replace', 15, NULL, 1, '', '', 0, 0, 0),
(48, NULL, 'Window - Repair', 15, NULL, 1, '', '', 0, 0, 0),
(49, NULL, 'Glass Blocks - Install', 15, NULL, 1, '', '', 0, 0, 0),
(50, NULL, 'Glass Shower Door or Enclosure - Repair', 15, NULL, 1, '', '', 0, 0, 0),
(51, NULL, 'Mirrors - Repair or Resilver', 15, NULL, 1, '', '', 0, 0, 0),
(52, NULL, 'Stained or Specialty Glass - Install or Replace', 15, NULL, 1, '', '', 0, 0, 0),
(53, NULL, 'Stained or Specialty Glass - Repair', 15, NULL, 1, '', '', 0, 0, 0),
(54, NULL, 'Hot Tub or Spa - Install or Replace', 16, NULL, 1, '', '', 0, 0, 0),
(55, NULL, 'Hot Tub or Spa - Repair or Service', 16, NULL, 1, '', '', 0, 0, 0),
(56, NULL, 'Interior Home or Surfaces - Paint or Stain', 17, NULL, 1, '', '', 0, 0, 0),
(57, NULL, 'Specialty Painting - Faux Finishes', 17, NULL, 1, '', '', 0, 0, 0),
(58, NULL, 'Bathtub or Other Bath or Kitchen Surface - Resurface or Reface', 17, NULL, 1, '', '', 0, 0, 0),
(59, NULL, 'Exterior Home or Structure - Paint or Stain', 17, NULL, 1, '', '', 0, 0, 0),
(60, NULL, 'Specialty Painting - Mural or Trompe Loeil', 17, NULL, 1, '', '', 0, 0, 0),
(61, NULL, 'Specialty Painting - Textures', 17, NULL, 1, '', '', 0, 0, 0),
(62, NULL, 'Drain Clearing', 18, NULL, 1, '', '', 0, 0, 0),
(63, NULL, 'Grout: Replace or Repair', 19, NULL, 1, '', '', 0, 0, 0),
(64, NULL, 'Tile: Ceramic and Porcelain - Install', 19, NULL, 1, '', '', 0, 0, 0),
(65, NULL, 'Tile: Natural Stone (Granite, Marble, Slate, Quartz, etc) - Install', 19, NULL, 1, '', '', 0, 0, 0),
(66, NULL, 'Stone Restoration & Polishing', 19, NULL, 1, '', '', 0, 0, 0),
(67, NULL, 'Tile & Grout Cleaning', 19, NULL, 1, '', '', 0, 0, 0),
(68, NULL, 'Tile: Ceramic and Porcelain - Repair', 19, NULL, 1, '', '', 0, 0, 0),
(69, NULL, 'Tile: Natural Stone (Granite, Marble, Slate, Quartz, etc) - Repair', 19, NULL, 1, '', '', 0, 0, 0),
(70, NULL, 'Acoustic Ceiling Tiles - Install', 20, NULL, 1, '', '', 0, 0, 0),
(71, NULL, 'Carpentry Framing - Install', 20, NULL, 1, '', '', 0, 0, 0),
(72, NULL, 'Ceiling Fan - Install', 20, NULL, 1, '', '', 0, 0, 0),
(73, NULL, 'Drywall - Install', 20, NULL, 1, '', '', 0, 0, 0),
(74, NULL, 'Brick, Stone or Block Wall - Install', 20, NULL, 1, '', '', 0, 0, 0),
(75, NULL, 'Closet - Build', 20, NULL, 1, '', '', 0, 0, 0),
(76, NULL, 'Glass Blocks - Install', 20, NULL, 1, '', '', 0, 0, 0),
(77, NULL, 'Interior Trim and Decorative Moldings - Install', 20, NULL, 1, '', '', 0, 0, 0),
(78, NULL, 'Plaster - Install', 20, NULL, 1, '', '', 0, 0, 0),
(79, NULL, 'Poured Concrete Wall - Install', 20, NULL, 1, '', '', 0, 0, 0),
(80, NULL, 'Wallcovering or Wallpaper - Install', 20, NULL, 1, '', '', 0, 0, 0),
(81, NULL, 'Doors - Install or Repair', 21, NULL, 1, '', '', 0, 0, 0),
(82, NULL, 'Sliding Door - Install', 21, NULL, 1, '', '', 0, 0, 0),
(83, NULL, 'Doors (Interior) - Install or Replace', 21, NULL, 1, '', '', 0, 0, 0),
(84, NULL, 'Egress Window - Install', 21, NULL, 1, '', '', 0, 0, 0),
(85, NULL, 'Skylight - Install', 21, NULL, 1, '', '', 0, 0, 0),
(86, NULL, 'Stained or Specialty Glass - Install or Replace', 21, NULL, 1, '', '', 0, 0, 0),
(87, NULL, 'Windows - New', 21, NULL, 1, '', '', 0, 0, 0),
(90, NULL, 'Room or Closet Organizers - Install', 22, NULL, 1, '', '', 0, 0, 0),
(91, NULL, 'Built-In Furniture - Construct', 22, NULL, 1, '', '', 0, 0, 0),
(92, NULL, 'Closet - Build', 22, NULL, 1, '', '', 0, 0, 0),
(93, NULL, 'Custom Cabinets - Build', 22, NULL, 1, '', '', 0, 0, 0),
(94, NULL, 'Wood Stairs and Railings - Install or Replace', 23, NULL, 1, '', '', 0, 0, 0),
(95, NULL, 'Metal Stairs and Railings - Install or Replace', 23, NULL, 1, '', '', 0, 0, 0),
(96, NULL, 'Metal Stairs and Railings - Repair', 23, NULL, 1, '', '', 0, 0, 0),
(97, NULL, 'Wood Stairs and Railings - Repair', 23, NULL, 1, '', '', 0, 0, 0),
(98, NULL, 'Exterior Trim - Install or Replace', 24, NULL, 1, '', '', 0, 0, 0),
(99, NULL, 'Exterior Trim - Repair ', 24, NULL, 1, '', '', 0, 0, 0),
(100, NULL, 'Wood Gutters - Install', 24, NULL, 1, '', '', 0, 0, 0),
(101, NULL, 'Wood Gutters - Repair', 24, NULL, 1, '', '', 0, 0, 0),
(102, NULL, 'Wood or Fiber-Cement Siding - Install or Completely Replace', 24, NULL, 1, '', '', 0, 0, 0),
(103, NULL, 'Wood or Fiber-Cement Siding - Repair or Partially Replace', 24, NULL, 1, '', '', 0, 0, 0),
(104, NULL, 'Gazebo or Freestanding Porch - Build or Install', 25, NULL, 1, '', '', 0, 0, 0),
(105, NULL, 'Shed, Barn or Playhouse - Build', 25, NULL, 1, '', '', 0, 0, 0),
(106, NULL, 'Shed, Barn or Playhouse - Repair', 25, NULL, 1, '', '', 0, 0, 0),
(107, NULL, 'Gazebo or Freestanding Porch - Repair', 25, NULL, 1, '', '', 0, 0, 0),
(108, NULL, 'Greenhouse - Repair', 25, NULL, 1, '', '', 0, 0, 0),
(109, NULL, 'Outdoor Kitchen-Build', 25, NULL, 1, '', '', 0, 0, 0),
(110, NULL, 'Water Dock - Repair', 25, NULL, 1, '', '', 0, 0, 0),
(111, NULL, 'Carpentry Framing - Install', 26, NULL, 1, '', '', 0, 0, 0),
(112, NULL, 'Carpentry Framing - Repair', 26, NULL, 1, '', '', 0, 0, 0),
(113, NULL, 'Closet - Build', 26, NULL, 1, '', '', 0, 0, 0),
(114, NULL, 'Steel Beams (Custom) - Fabricate', 26, NULL, 1, '', '', 0, 0, 0),
(115, NULL, 'Steel Beams (Structural) - Install', 26, NULL, 1, '', '', 0, 0, 0),
(116, NULL, 'Interior Trim and Decorative Moldings - Install', 27, NULL, 1, '', '', 0, 0, 0),
(117, NULL, 'Exterior Trim - Install or Replace', 27, NULL, 1, '', '', 0, 0, 0),
(118, NULL, 'Exterior Trim - Repair', 27, NULL, 1, '', '', 0, 0, 0),
(119, NULL, 'Interior Trim and Decorative Moldings - Repair', 27, NULL, 1, '', '', 0, 0, 0),
(120, NULL, 'Doors (Exterior) - Install or Replace', 28, NULL, 1, '', '', 0, 0, 0),
(121, NULL, 'Doors (Interior) - Install or Replace', 28, NULL, 1, '', '', 0, 0, 0),
(122, NULL, 'Doors - Install or Repair', 28, NULL, 1, '', '', 0, 0, 0),
(123, NULL, 'Doors - Repair', 28, NULL, 1, '', '', 0, 0, 0),
(124, NULL, 'Deck or Porch - Build or Replace', 29, NULL, 1, '', '', 0, 0, 0),
(125, NULL, 'Deck or Porch - Repair', 29, NULL, 1, '', '', 0, 0, 0),
(126, NULL, 'Disability Ramps - Build', 29, NULL, 1, '', '', 0, 0, 0),
(127, NULL, 'Arbor, Pergola or Trellis - Build Custom', 29, NULL, 1, '', '', 0, 0, 0),
(128, NULL, 'Disability Ramps - Repair', 29, NULL, 1, '', '', 0, 0, 0),
(129, NULL, 'Water Dock - Build ', 29, NULL, 1, '', '', 0, 0, 0),
(130, NULL, 'Wood Fence - Install', 29, NULL, 1, '', '', 0, 0, 0),
(131, NULL, 'Wood Fence - Repair', 29, NULL, 1, '', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `post_images`
--

CREATE TABLE IF NOT EXISTS `post_images` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `originalpath` varchar(255) NOT NULL,
  `resizepath` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=322 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_images`
--

INSERT INTO `post_images` (`id`, `post_id`, `originalpath`, `resizepath`) VALUES
(231, 14358, '58416804229be.jpg', '58416804229be.jpg'),
(233, 14357, '584168fc4065e.jpg', '584168fc4065e.jpg'),
(234, 14356, '5841692bbeb52.jpg', '5841692bbeb52.jpg'),
(235, 14355, '5841696cd9d9b.jpg', '5841696cd9d9b.jpg'),
(236, 14354, '5841699050681.jpg', '5841699050681.jpg'),
(237, 14353, '584169d33689c.jpg', '584169d33689c.jpg'),
(238, 14352, '584169ef5e5ce.jpg', '584169ef5e5ce.jpg'),
(269, 14374, '957974697_image1.jpg', '957974697_image1.jpg'),
(270, 14374, '1032603759_image2.jpg', '1032603759_image2.jpg'),
(271, 14375, '332461401_image3.jpg', '332461401_image3.jpg'),
(272, 14375, '1686734078_image4.jpg', '1686734078_image4.jpg'),
(273, 14376, '1172891136_image1.jpg', '1172891136_image1.jpg'),
(274, 14376, '1511286918_image2.jpg', '1511286918_image2.jpg'),
(275, 14376, '559586150_image3.jpg', '559586150_image3.jpg'),
(276, 14376, '2122839019_image4.jpg', '2122839019_image4.jpg'),
(277, 14376, '1018193631_imgpsh_fullsize.png', '1018193631_imgpsh_fullsize.png'),
(278, 14376, '797898302_videoimage.png', '797898302_videoimage.png'),
(279, 14379, '1626485120_image4.jpg', '1626485120_image4.jpg'),
(280, 14379, '1651353100_image3.jpg', '1651353100_image3.jpg'),
(281, 14379, '1132469648_image2.jpg', '1132469648_image2.jpg'),
(282, 14380, '788725106_image4.jpg', '788725106_image4.jpg'),
(283, 14380, '1284112616_image3.jpg', '1284112616_image3.jpg'),
(284, 14380, '54213533_image2.jpg', '54213533_image2.jpg'),
(285, 14380, '881800208_image1.jpg', '881800208_image1.jpg'),
(286, 14381, '155816354_image4.jpg', '155816354_image4.jpg'),
(287, 14381, '844422877_image3.jpg', '844422877_image3.jpg'),
(288, 14381, '427297839_image2.jpg', '427297839_image2.jpg'),
(289, 14381, '1096945266_image1.jpg', '1096945266_image1.jpg'),
(290, 14382, '226907113_image4.jpg', '226907113_image4.jpg'),
(291, 14382, '726783937_image3.jpg', '726783937_image3.jpg'),
(292, 14382, '1288906732_image2.jpg', '1288906732_image2.jpg'),
(293, 14383, '447003949_image1.jpg', '447003949_image1.jpg'),
(294, 14384, '1936642466_videoimage.png', '1936642466_videoimage.png'),
(295, 14385, '1344704137_image4.jpg', '1344704137_image4.jpg'),
(296, 14385, '1427606490_image3.jpg', '1427606490_image3.jpg'),
(297, 14385, '1417138035_image2.jpg', '1417138035_image2.jpg'),
(298, 14389, '49175199_images.jpg', '49175199_images.jpg'),
(299, 14389, '1789187375_images1.jpg', '1789187375_images1.jpg'),
(300, 14389, '486334005_images345.jpg', '486334005_images345.jpg'),
(301, 14389, '2072028099_img.caspx.jpg', '2072028099_img.caspx.jpg'),
(302, 14385, '1328408488_image1.jpg', '1328408488_image1.jpg'),
(303, 14402, '163312612_wserwerwer.png', '163312612_wserwerwer.png'),
(304, 14403, '148747842_image1.jpg', '148747842_image1.jpg'),
(305, 14404, '526820374_image4.jpg', '526820374_image4.jpg'),
(306, 14406, '815014252_wserwerwer.png', '815014252_wserwerwer.png'),
(307, 14428, '586369bc2d567.jpg', '586369bc2d567.jpg'),
(308, 14429, '5863d9b9aef9f.jpg', '5863d9b9aef9f.jpg'),
(309, 14435, '1776905732_image1.jpg', '1776905732_image1.jpg'),
(310, 14437, '187630941_chrysanthemum.jpg', '187630941_chrysanthemum.jpg'),
(311, 14438, '1811568192_cms1.jpg', '1811568192_cms1.jpg'),
(312, 14437, '1240369334_pexels-photo-226243.jpeg', '1240369334_pexels-photo-226243.jpeg'),
(313, 14389, '1087171619_phone-camera-624x382.jpg', '1087171619_phone-camera-624x382.jpg'),
(314, 14441, '58d124966b140.jpeg', '58d124966b140.jpeg'),
(315, 2, '58d226d7eca19.jpeg', '58d226d7eca19.jpeg'),
(316, 3, '58d22bbe9386c.jpeg', '58d22bbe9386c.jpeg'),
(317, 4, '58d22c814c2dd.jpeg', '58d22c814c2dd.jpeg'),
(318, 9, '58dceec921555.jpg', '58dceec921555.jpg'),
(319, 10, '58de4fef62107.jpg', '58de4fef62107.jpg'),
(320, 11, '58e496337b177.jpeg', '58e496337b177.jpeg'),
(321, 12, '58eb49c386768.jpeg', '58eb49c386768.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `service_id` int(255) NOT NULL,
  `q_order` int(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=467 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `name`, `service_id`, `q_order`) VALUES
(4, 'What is the event', 5, 1),
(5, 'When it will happen', 5, 2),
(6, 'What is your event?', 7, 1),
(7, 'When the event will take place?', 7, 2),
(8, 'Average age of your guests ', 7, 3),
(9, 'What do you want to decorate?', 6, 1),
(10, 'How many square feet area?', 6, 2),
(11, 'What are you want to decorate?', 6, 3),
(13, 'How many days of your wedding will be captured?', 8, 1),
(14, 'Which services do you want to include?', 9, 1),
(15, 'What changes will be included in this remodel?', 11, 1),
(16, 'What type of bathtub do you want installed?', 12, 1),
(17, 'What style is your current bathtub?', 13, 1),
(18, 'What type of color and finish are you considering?', 13, 2),
(19, 'Are you also interested in a wall surround or wall panel system?', 13, 3),
(20, 'Which accessories are you interested in learning more about? (Optional)', 13, 4),
(21, 'What is the location of your project? (Enter Zip Code) *', 14, 1),
(22, 'What type of shower do you want installed?', 14, 2),
(23, 'What changes will be included with this shower install?', 14, 3),
(24, 'What kind of location is this?', 14, 4),
(25, 'What is the location of your project? (Enter Zip Code) *', 15, 1),
(28, 'What best describes this architectural project? *', 15, 2),
(29, 'What type of building is involved? *', 15, 3),
(30, 'What architectural services do you need? (Select all that apply)', 15, 4),
(31, 'What is the location of your project? (Enter Zip Code) *', 16, 1),
(32, 'Which fixtures or surfaces require resurfacing? (Check all that apply)', 16, 2),
(33, 'What needs to be done? (Check all that apply)', 16, 3),
(34, 'What is the location of your project? (Enter Zip Code) *', 18, 1),
(35, 'What is the location of your project? (Enter Zip Code) *', 20, 1),
(36, 'What best describes your cabinet project? *', 20, 2),
(37, 'Is this project part of a larger remodel? *', 20, 3),
(38, 'Select the location of the cabinets: *', 20, 4),
(39, 'What is the location of your project? (Enter Zip Code) *', 21, 1),
(40, 'Please select the location(s) where the cabinets will be refaced.', 21, 2),
(41, 'Of what material are the existing cabinets made?', 21, 3),
(42, 'What special features would you like incorporated with your cabinetry? (Check all that apply)', 21, 4),
(43, 'What is the location of your project? (Enter Zip Code) *', 22, 1),
(44, 'What best describes your cabinet project? *', 22, 2),
(45, 'Is this project part of a larger remodel? *', 22, 3),
(46, 'Select the location of the cabinets: *', 22, 4),
(47, 'What is the location of your project? (Enter Zip Code) *', 23, 1),
(48, 'What kind of Built-In furniture would you like?', 23, 2),
(49, 'Where do you want to install the Built-In?', 23, 3),
(50, 'What features would you like?', 23, 4),
(51, 'What is the location of your project? (Enter Zip Code) *', 24, 1),
(52, 'Please select the location where the cabinets will be refinished.', 24, 2),
(53, 'What type of refinishing do you want done to your cabinets? (Check all that apply)', 24, 3),
(54, 'Of what material are the existing cabinets made?', 24, 4),
(55, 'What is the location of your project? (Enter Zip Code) *', 25, 1),
(56, 'Check all problems you are having with your cabinets.', 25, 2),
(57, 'Of what material are the cabinets made?', 25, 3),
(58, 'What is the location of your project? (Enter Zip Code) *', 26, 1),
(59, 'What best describes your cabinet project? *', 26, 2),
(60, 'Is this project part of a larger remodel? *', 26, 3),
(61, 'Select the location of the cabinets: *', 26, 4),
(62, 'What is the location of your project? (Enter Zip Code) *', 27, 1),
(63, 'What best describes this countertop project? *', 27, 2),
(64, 'Is this project part of a larger remodel? *', 27, 3),
(65, 'Select all the features for your laminate countertop.', 27, 4),
(66, 'What is the location of your project? (Enter Zip Code) *', 28, 1),
(67, 'What best describes this countertop project? *', 28, 2),
(68, 'Is this project part of a larger remodel? *', 28, 3),
(69, 'Select the types of solid stone you are interested in:', 28, 4),
(70, 'Select the features for your countertop:', 28, 5),
(71, 'What is the location of your project? (Enter Zip Code) *', 29, 1),
(72, 'Which fixtures or surfaces require resurfacing? (Check all that apply)', 29, 2),
(73, 'What needs to be done? (Check all that apply)', 29, 3),
(74, 'What is the location of your project? (Enter Zip Code) *', 30, 1),
(75, 'What best describes this countertop project? *', 30, 2),
(76, 'Is this project part of a larger remodel? *', 30, 3),
(77, 'What kind of solid countertop material do you want?', 30, 4),
(78, 'Select the features for your solid countertop. (Select all that apply)', 30, 5),
(79, 'What is the location of your project? (Enter Zip Code) *', 31, 1),
(80, 'What kind of project is this? *', 31, 2),
(81, 'Have you already purchased the tile for this project? *', 31, 3),
(82, 'Where will the tile be installed?', 31, 4),
(83, 'What type of tile do you want to have installed?', 31, 5),
(84, 'What is the location of your project? (Enter Zip Code) *', 32, 1),
(85, 'What kind of project is this? *', 32, 2),
(86, 'Have you already purchased the tile for this project?', 32, 3),
(87, 'What kind of stone would you like installed?', 32, 4),
(88, 'Where would you like to install stone tiles? (Check all that apply)', 32, 5),
(89, 'Are you interested in financing?', 32, 6),
(90, 'What is the location of your project? (Enter Zip Code) *', 33, 1),
(91, 'What kind of location is this? *', 33, 2),
(92, 'Is this an emergency? *', 33, 3),
(93, 'What services do you need? (Check all that apply)', 33, 4),
(94, 'What electrical fixtures are included in this project? (Check all that apply)', 33, 5),
(95, 'What is the location of your project? (Enter Zip Code) *', 34, 1),
(96, 'What kind of location is this? *', 34, 2),
(97, 'What best describes your electrical project? *', 34, 3),
(98, 'What area(s) of the home will be included in this project?', 34, 4),
(99, 'Which types of electrical devices will you need wired?', 34, 5),
(100, 'What is the location of your project? (Enter Zip Code) *', 35, 1),
(101, 'What is the nature of this project? (Check all that apply)', 35, 2),
(102, 'Why do you need a lighting designer?', 35, 3),
(103, 'What is the location of your project? (Enter Zip Code) *', 36, 1),
(104, 'What kind of location is this? *', 36, 2),
(105, 'Have you already purchased an electric vehicle charging station? *', 36, 3),
(106, 'Is the installation site prewired to support the required electrical load (typically referred to as 220v or 240v)? *', 36, 4),
(107, 'What is the location of your project? (Enter Zip Code) *', 37, 1),
(108, 'What best describes your electrical project? *', 37, 2),
(109, 'What kind of location is this? *', 37, 3),
(110, 'What is the location of your project? (Enter Zip Code) *', 38, 1),
(111, 'What type of project is this?', 38, 2),
(112, 'What components would you like to have integrated?', 38, 3),
(113, 'What is the location of your project? (Enter Zip Code) *', 39, 1),
(114, 'What kind of project is this? *', 39, 2),
(115, 'Have you already purchased the tile for this project? *', 39, 3),
(116, 'Where will the tile be installed?', 39, 4),
(117, 'What type of tile do you want to have installed?', 39, 5),
(118, 'What is the location of your project? (Enter Zip Code) *', 40, 1),
(119, 'What are you looking for? *', 40, 2),
(120, 'What best describes this flooring project? *', 40, 3),
(121, 'Select the type of wood flooring you would like to install: *', 40, 4),
(122, 'Have you already purchased the materials for this project? *', 40, 5),
(123, 'Select the rooms involved in this flooring project:', 40, 6),
(124, 'What is the location of your project? (Enter Zip Code) *', 41, 1),
(125, 'What are you looking for? *', 41, 2),
(126, 'Does the floor have any of the following problems? (Check all that apply)', 41, 3),
(127, 'Where are the hardwood floors located?', 41, 4),
(128, 'Select the stain color you would like for your refinished wood floors:', 41, 5),
(129, 'What is the location of your project? (Enter Zip Code) *', 42, 1),
(130, 'What are you looking for? *', 42, 2),
(131, 'What kind of location is this? *', 42, 3),
(132, 'Have you already purchased the carpet for this project? *', 42, 4),
(133, 'What kind of carpet installation is this?', 42, 5),
(134, 'Where will the carpet be installed?', 42, 6),
(135, 'What is the location of your project? (Enter Zip Code) *', 43, 1),
(136, 'Have you already purchased the flooring materials for this project? *', 43, 2),
(137, 'What style of laminate flooring would you like?', 43, 3),
(138, 'In which rooms would you like to install new flooring? (Check all that apply)', 43, 4),
(139, 'What is the location of your project? (Enter Zip Code) *', 44, 1),
(140, 'What kind of project is this? *', 44, 2),
(141, 'Have you already purchased the tile for this project?', 44, 3),
(142, 'What kind of stone would you like installed?', 44, 4),
(143, 'Where would you like to install stone tiles? (Check all that apply)', 44, 5),
(144, 'Are you interested in financing?', 44, 6),
(145, 'What is the location of your project? (Enter Zip Code) *', 45, 1),
(146, 'What type of vinyl flooring do you want installed?', 45, 2),
(147, 'Have you already purchased the flooring materials for this project?', 45, 3),
(148, 'What is the location of your project? (Enter Zip Code) *', 46, 1),
(149, 'Choose the kind of glass shower doors you need installed.', 46, 2),
(150, 'What type of glass?', 46, 3),
(151, 'What best describes the configuration of the unit?', 46, 4),
(152, 'What type of door and panel(s)?', 46, 5),
(153, 'What is the location of your project? (Enter Zip Code) *', 47, 1),
(154, 'What best describes this mirror project? *', 47, 2),
(155, 'Tell us the type(s) of mirror you would like installed. (Select all that apply)', 47, 3),
(156, 'Would you like any old mirrors removed?', 47, 4),
(157, 'What is the location of your project? (Enter Zip Code) *', 48, 1),
(159, 'Is this an emergency? *', 48, 2),
(160, 'List all the problems you need repaired: (Select all that apply)', 48, 3),
(161, 'Select the style of window(s) that need to be repaired: (Select all that apply)', 48, 4),
(162, 'Of what material are the window frames made?', 48, 5),
(163, 'What is the location of your project? (Enter Zip Code) *', 49, 1),
(164, 'Where do you want the glass/acrylic block installed?', 49, 2),
(165, 'What features do you want? (Select all that apply)', 49, 3),
(166, 'What is the location of your project? (Enter Zip Code) *', 50, 1),
(167, 'Select any of the following shower door repairs needed. (Check all that apply)', 50, 2),
(168, 'What is the door or enclosure used for?', 50, 3),
(169, 'What is the location of your project? (Enter Zip Code) *', 51, 1),
(170, 'What best describes this mirror project? *', 51, 2),
(171, 'Which of these describes the mirror needing repair?', 51, 3),
(172, 'What is the location of your project? (Enter Zip Code) *', 52, 1),
(173, 'Where do you want to install decorative glass? (Check all that apply)', 52, 2),
(174, 'What kind of glass do you want? (Check all that apply)', 52, 3),
(175, 'Would like recommendation', 52, 4),
(176, 'What is the location of your project? (Enter Zip Code) *', 53, 1),
(177, 'Is this an emergency? *', 53, 2),
(178, 'Why does it need repairing? (Check all that apply)', 53, 3),
(179, 'What kind of glass piece needs repair?', 53, 4),
(180, 'What is the location of your project? (Enter Zip Code) *', 54, 1),
(181, 'Have you already purchased the hot tub or spa for this project? *', 54, 2),
(182, 'What type of hot tub or spa would you like?', 54, 3),
(183, 'Will this hot tub or spa be indoors or outdoors?', 54, 4),
(184, 'Select the features you want in your hot tub.', 54, 5),
(185, 'What is the location of your project? (Enter Zip Code) *', 55, 1),
(186, 'What type of hot tub or spa needs to be repaired?', 55, 2),
(187, 'What describes the problem with your hot tub or spa? (Choose all that apply)', 55, 3),
(188, 'What is the location of your project? (Enter Zip Code) *', 56, 1),
(189, 'What is the occupancy of the house?', 56, 2),
(190, 'Which rooms need to be painted? (Check all that apply)', 56, 3),
(191, 'What needs to be painted?', 56, 4),
(192, 'What is the location of your project? (Enter Zip Code) *', 57, 1),
(193, 'What do you want painted? (Check all that apply)', 57, 2),
(194, 'Do you know what paint effect(s) you want?', 57, 3),
(195, 'What is the location of your project? (Enter Zip Code) *', 58, 1),
(196, 'Which fixtures or surfaces require resurfacing? (Check all that apply)', 58, 2),
(197, 'What needs to be done? (Check all that apply)', 58, 3),
(198, 'What is the location of your project? (Enter Zip Code) *', 59, 1),
(199, 'What type of project is this?', 59, 2),
(200, 'What kinds of surfaces need to be painted and/or stained?', 59, 3),
(201, 'Does the exterior of your house show any of the following signs?', 59, 4),
(202, 'What is the location of your project? (Enter Zip Code) *', 60, 1),
(203, 'What do you want painted? (Check all that apply)', 60, 2),
(204, 'What type of design preparation has been done?', 60, 3),
(205, 'What is the location of your project? (Enter Zip Code) *', 61, 1),
(206, 'What kind of texturing do you need? (Check all that apply)', 61, 2),
(207, 'What surfaces need to be textured?', 61, 3),
(208, 'What is the location of your project? (Enter Zip Code) *', 63, 1),
(209, 'Where is the tile located? (Check all that apply)', 63, 2),
(210, 'What type of tile needs to have the grout replaced or repaired?', 63, 3),
(211, 'What is the location of your project? (Enter Zip Code) *', 64, 1),
(212, 'What kind of project is this? *', 64, 2),
(213, 'Have you already purchased the tile for this project? *', 64, 3),
(214, 'Where will the tile be installed?', 64, 4),
(215, 'What type of tile do you want to have installed?', 64, 5),
(216, 'What is the location of your project? (Enter Zip Code) *', 65, 1),
(217, 'What kind of project is this? *', 65, 2),
(218, 'Have you already purchased the tile for this project?', 65, 3),
(219, 'What kind of stone would you like installed?', 65, 4),
(220, 'Where would you like to install stone tiles? (Check all that apply)', 65, 5),
(221, 'Are you interested in financing?', 65, 6),
(222, 'What is the location of your project? (Enter Zip Code) *', 66, 1),
(223, 'Where is the stone located? (Select all that apply)', 66, 2),
(224, 'What type of stone do you want restored or polished?', 66, 3),
(225, 'What is the location of your project? (Enter Zip Code) *', 67, 1),
(226, 'What type of service do you need? *', 67, 2),
(227, 'What type of surface is involved? (Select all that apply)', 67, 3),
(228, 'What is the location of your project? (Enter Zip Code) *', 68, 1),
(229, 'What best describes the damage? (Check all that apply)', 68, 2),
(230, 'Where is the tile located? (Check all that apply)', 68, 3),
(231, 'Do you have spare tiles left over from installation?', 68, 4),
(232, 'What is the location of your project? (Enter Zip Code) *', 69, 1),
(233, 'Which best describes the nature of the damage? (Check all that apply)', 69, 2),
(234, 'Where is the tile located? (Check all that apply)', 69, 3),
(235, 'Do you have spare tiles left over from installation?', 69, 4),
(236, 'What is the location of your project? (Enter Zip Code) *', 70, 1),
(237, 'Why do you need to install a new ceiling?', 70, 2),
(238, 'Select all of the specific characteristics you are looking for.', 70, 3),
(239, 'What is the location of your project? (Enter Zip Code) *', 71, 1),
(240, 'Are there architectural or structural drawings for your project?', 71, 2),
(241, 'What type of framing material is specified?', 71, 3),
(242, 'Which projects best describe your framing needs? (Check all that apply)', 71, 4),
(243, 'What is the location of your project? (Enter Zip Code) *', 72, 1),
(244, 'What type of ceiling fan installation is this?', 72, 2),
(245, 'Have you already purchased the fan(s) for this project?', 72, 3),
(246, 'How will the ceiling fan be controlled? (Check all that apply)', 72, 4),
(247, 'What quality level of ceiling fan do you want installed?', 72, 5),
(248, 'What is the location of your project? (Enter Zip Code) *', 73, 1),
(249, 'What kind of drywall project is this? (Check all that apply)', 73, 2),
(250, 'Which rooms of your house need drywall? (Check all that apply)', 73, 3),
(251, 'What type of finish(es) do you want? (Check all that apply)', 73, 4),
(252, 'What is the location of your project? (Enter Zip Code) *', 74, 1),
(253, 'What type of material do you want?', 74, 2),
(254, 'What is the purpose of the wall?', 74, 3),
(255, 'Select any other services needed as part of this project.', 74, 4),
(256, 'What is the location of your project? (Enter Zip Code) *', 75, 1),
(257, 'Where will the closet be located?', 75, 2),
(258, 'What features would you like to be included in the new closet? (Select all that apply)', 75, 3),
(259, 'Is this part of a larger home remodel?', 75, 4),
(260, 'What is the location of your project? (Enter Zip Code) *', 76, 1),
(261, 'Where do you want the glass/acrylic block installed?', 76, 2),
(262, 'What features do you want? (Select all that apply)', 76, 3),
(263, 'What features do you want? (Select all that apply)', 76, 3),
(264, 'What is the location of your project? (Enter Zip Code) *', 77, 1),
(265, 'For the interior, which of the following features need finish work? (Check all that apply)', 77, 2),
(266, 'Which of the following (doors and windows) do you need installed?', 77, 3),
(267, 'Are you looking for a finish carpenter/woodworker to complete multiple small projects?', 77, 4),
(268, 'What is the location of your project? (Enter Zip Code) *', 78, 1),
(269, 'What type of finish do you want?', 78, 2),
(270, 'Which rooms of the house need plaster? (Select all that apply)', 78, 3),
(271, 'What is the location of your project? (Enter Zip Code) *', 79, 1),
(272, 'What is the purpose of the concrete wall?', 79, 2),
(273, 'Select any special applications or finishes you would like for this project.', 79, 3),
(274, 'What is the location of your project? (Enter Zip Code) *', 80, 1),
(275, 'Have you already purchased the materials for this project?', 80, 2),
(276, 'Select all of the preparation you will need done. (Check all that apply)', 80, 3),
(277, 'What is the location of your project? (Enter Zip Code) *', 81, 1),
(279, 'What is the location of your project? (Enter Zip Code) *', 82, 1),
(280, 'What best describes this door project? *', 82, 2),
(281, 'What type of door project is this? *', 82, 3),
(282, 'Is this an emergency? *', 82, 4),
(283, 'What kind of location is this? *', 82, 5),
(284, 'What is the location of your project? (Enter Zip Code) *', 83, 1),
(285, 'What best describes this door project? *', 83, 2),
(286, 'What type of door would you like installed? *', 83, 3),
(287, 'Is this an emergency? *', 83, 4),
(288, 'Have you already purchased the door(s) for this project?', 83, 5),
(289, 'What is the location of your project? (Enter Zip Code) *', 84, 1),
(291, 'Is this an emergency? *', 84, 2),
(292, 'What is the nature of this project?', 84, 3),
(293, 'Is this part of a basement remodel?', 84, 4),
(294, 'Select any features you are interested in:', 84, 5),
(295, 'What is the location of your project? (Enter Zip Code) *', 85, 1),
(296, 'Select the best description of what you need.', 85, 2),
(297, 'What kind of roof do you have?', 85, 3),
(298, 'What is the location of your project? (Enter Zip Code) *', 86, 1),
(299, 'Where do you want to install decorative glass? (Check all that apply)', 86, 2),
(300, 'What kind of glass do you want? (Check all that apply)', 86, 3),
(301, 'Do you know what kind of design you want?', 86, 4),
(302, 'What is the location of your project? (Enter Zip Code) *', 87, 1),
(303, 'What is the nature of this project? *', 87, 2),
(304, 'Is this an emergency? *', 87, 3),
(305, 'What is the location of your project? (Enter Zip Code) *', 89, 1),
(306, 'Describe the closet or room where this organizer system will be installed: (Select all that apply)', 89, 2),
(307, 'What type of organization are you hoping to install? (Select all that apply)', 89, 3),
(308, 'What is the location of your project? (Enter Zip Code) *', 90, 1),
(309, 'Describe the closet or room where this organizer system will be installed: (Select all that apply)', 90, 2),
(310, 'What type of organization are you hoping to install? (Select all that apply)', 90, 43),
(311, 'What is the location of your project? (Enter Zip Code) *', 91, 1),
(312, 'What kind of Built-In furniture would you like?', 91, 2),
(313, 'Where do you want to install the Built-In?', 91, 2),
(314, 'What features would you like?', 91, 4),
(315, 'What is the location of your project? (Enter Zip Code) *', 92, 1),
(316, 'Where will the closet be located?', 92, 2),
(317, 'What features would you like to be included in the new closet? (Select all that apply)', 92, 3),
(318, 'Is this part of a larger home remodel?', 92, 4),
(319, 'What is the location of your project? (Enter Zip Code) *', 93, 1),
(320, 'What best describes your cabinet project? *', 93, 2),
(321, 'Is this project part of a larger remodel? *', 93, 3),
(322, 'Select the location of the cabinets: *', 93, 4),
(323, 'What is the location of your project? (Enter Zip Code) *', 94, 1),
(324, 'What kind of stairs do you want installed? (Check all that apply)', 94, 2),
(325, 'How will the stairs be constructed?', 94, 3),
(326, 'What is the location of your project? (Enter Zip Code) *', 95, 1),
(327, 'Please enter the type of stairs (Check all that apply)', 95, 2),
(328, 'How will the stairs be constructed?', 95, 3),
(329, 'Please enter the type of material (Check all that apply)', 95, 4),
(330, 'How wide are the stairs you want built?', 95, 5),
(331, 'What is the location of your project? (Enter Zip Code) *', 96, 1),
(332, 'Please enter the type of stairs (Check all that apply)', 96, 2),
(333, 'What is the problem? (Check all that apply)', 96, 3),
(334, 'Please enter the type of material (Check all that apply)', 96, 4),
(335, 'What is the location of your project? (Enter Zip Code) *', 97, 1),
(336, 'Is this an emergency? *', 97, 2),
(337, 'What kind of stairs need repair? (Check all that apply)', 97, 3),
(338, 'What is the problem? (Check all that apply)', 97, 4),
(339, 'What is the location of your project? (Enter Zip Code) *', 98, 1),
(340, 'What is the nature of this project? *', 98, 2),
(341, 'What kind of material will be used for trim? (Select all that apply)', 98, 3),
(342, 'What type of sidiing do you currently have?', 98, 4),
(343, 'What is the location of your project? (Enter Zip Code) *', 99, 1),
(344, 'Check all visible damage.', 99, 2),
(345, 'What kind of trim do you have?', 99, 3),
(346, 'What type of siding do you have?', 99, 4),
(347, 'What is the location of your project? (Enter Zip Code) *', 100, 1),
(348, 'What style of gutter do you want?', 100, 2),
(350, 'What size gutter would you like?', 100, 3),
(351, 'What is the location of your project? (Enter Zip Code) *', 101, 1),
(352, 'Select which problems need repair.', 101, 2),
(353, 'What is the location of your project? (Enter Zip Code) *', 102, 1),
(354, 'What type of project is this? *', 102, 2),
(355, 'What is the location of your project? (Enter Zip Code) *', 103, 1),
(356, 'List any visible symptoms.', 103, 2),
(357, 'What is the location of your project? (Enter Zip Code) *', 104, 1),
(358, 'What style of gazebo do you want?', 104, 2),
(359, 'What kind of deck/floor?', 104, 3),
(360, 'What other features would you like?', 104, 4),
(361, 'What is the location of your project? (Enter Zip Code) *', 105, 1),
(362, 'For what will the building be used?', 105, 2),
(363, 'Of what material do you want your building made?', 105, 3),
(364, 'What features do you want for your building?', 105, 4),
(365, 'What level of customization do you need?', 105, 5),
(366, 'What is the location of your project? (Enter Zip Code) *', 106, 1),
(367, 'What needs to be repaired?', 106, 2),
(368, 'What is the building used for?', 106, 3),
(369, 'What kind of barn, shed, or playhouse do you have?', 106, 4),
(370, 'What is the location of your project? (Enter Zip Code) *', 107, 1),
(371, 'What work needs to be done? (Check all that apply)', 107, 2),
(372, 'What is the location of your project? (Enter Zip Code) *', 108, 1),
(373, 'What kind of service do you need? (Check all that apply)', 108, 2),
(374, 'What is the location of your project? (Enter Zip Code) *', 109, 1),
(375, 'What features would you like for your outdoor kitchen?', 109, 2),
(376, 'What flooring option would you prefer?', 109, 3),
(377, 'What is the location of your project? (Enter Zip Code) *', 110, 1),
(378, 'Is this an emergency? *', 110, 2),
(379, 'What needs servicing? (Check all that apply)', 110, 3),
(380, 'What type of dock is it? (Check all that apply)', 110, 4),
(381, 'What is the location of your project? (Enter Zip Code) *', 111, 1),
(382, 'Are there architectural or structural drawings for your project?', 111, 2),
(383, 'What type of framing material is specified?', 111, 3),
(384, 'Which projects best describe your framing needs? (Check all that apply)', 111, 4),
(385, 'What is the location of your project? (Enter Zip Code) *', 112, 1),
(386, 'What type(s) of framing needs repair? (Check all that apply)', 112, 2),
(387, 'Is the repair needed due to dry rot or water damage?', 112, 3),
(388, 'What is the location of your project? (Enter Zip Code) *', 113, 1),
(389, 'Where will the closet be located?', 113, 2),
(390, 'What features would you like to be included in the new closet? (Select all that apply)', 113, 3),
(391, 'Is this part of a larger home remodel?', 113, 4),
(392, 'What is the location of your project? (Enter Zip Code) *', 114, 1),
(393, 'What will the beam be supporting?', 114, 2),
(394, 'Where is the beam being used?', 114, 3),
(395, 'Would you like the beam delivered to the job site?', 114, 4),
(396, 'What is the location of your project? (Enter Zip Code) *', 115, 1),
(397, 'What project is the beam for?', 115, 2),
(398, 'What will the beam be supporting?', 115, 3),
(399, 'Where is the beam being used?', 115, 4),
(400, 'What is the location of your project? (Enter Zip Code) *', 116, 1),
(401, 'For the interior, which of the following features need finish work? (Check all that apply)', 116, 2),
(402, 'Which of the following (doors and windows) do you need installed?', 116, 3),
(403, 'Are you looking for a finish carpenter/woodworker to complete multiple small projects?', 116, 4),
(404, 'What is the location of your project? (Enter Zip Code) *', 117, 1),
(405, 'What is the nature of this project? *', 117, 2),
(406, 'What kind of material will be used for trim? (Select all that apply)', 117, 3),
(407, 'What type of sidiing do you currently have?', 117, 4),
(408, 'What is the location of your project? (Enter Zip Code) *', 118, 1),
(409, 'Check all visible damage.', 118, 2),
(410, 'What kind of trim do you have?', 118, 3),
(411, 'What type of siding do you have?', 118, 4),
(412, 'What is the location of your project? (Enter Zip Code) *', 119, 1),
(413, 'What needs to be done? (Check all that apply)', 119, 2),
(414, 'How was your finish trim damaged? (Check all that apply)', 119, 3),
(415, 'What is the location of your project? (Enter Zip Code) *', 120, 1),
(416, 'What best describes this door project? *', 120, 2),
(417, 'What type of door would you like installed? *', 120, 3),
(418, 'Is this an emergency? *', 120, 4),
(419, 'Have you already purchased the door(s) for this project?', 120, 5),
(420, 'What is the location of your project? (Enter Zip Code) *', 121, 1),
(421, 'What best describes this door project? *', 121, 2),
(422, 'What type of door would you like installed? *', 121, 3),
(423, 'Is this an emergency? *', 121, 4),
(424, 'Have you already purchased the door(s) for this project?', 121, 5),
(425, 'What is the location of your project? (Enter Zip Code) *', 122, 1),
(426, 'What best describes this door project? *', 122, 2),
(427, 'What type of door project is this? *', 122, 3),
(428, 'Is this an emergency? *', 122, 4),
(429, 'What kind of location is this? *', 122, 5),
(430, 'What is the location of your project? (Enter Zip Code) *', 123, 1),
(431, 'What kind of location is this? *', 123, 2),
(432, 'Is this an emergency? *', 123, 3),
(433, 'What kind of door needs to be repaired? (Select all that apply)', 123, 4),
(434, 'Select all parts of the door or frame that are damaged.', 123, 5),
(435, 'What is the location of your project? (Enter Zip Code) *', 124, 1),
(436, 'What best describes the project? *', 124, 2),
(437, 'Select any special features you would like:', 124, 2),
(438, 'What is the location of your project? (Enter Zip Code) *', 125, 1),
(439, 'Is this an emergency? *', 125, 2),
(440, 'What kind of repair or maintenance do you need for your deck? (Select all that apply)', 125, 3),
(441, 'What is your deck made of?', 125, 4),
(442, 'What is the location of your project? (Enter Zip Code) *', 126, 1),
(443, 'Where do you need a ramp?', 126, 2),
(444, 'Why do you need a ramp?', 126, 3),
(445, 'Of what material do you want the ramp made?', 126, 4),
(446, 'What is the location of your project? (Enter Zip Code) *', 127, 1),
(447, 'What type of landscaping structure do you want to have built?', 127, 2),
(448, 'What type of design preparation has been done?', 127, 3),
(449, 'What type of project is this?', 127, 4),
(450, 'Of what material will the structure be made?', 127, 5),
(451, 'What is the location of your project? (Enter Zip Code) *', 128, 1),
(452, 'Is this an emergency? *', 128, 2),
(453, 'What is the ramp made of? (Check all that apply)', 128, 3),
(454, 'What needs to be repaired? (Check all that apply)', 128, 4),
(455, 'Where is the ramp? (Check all that apply)', 128, 5),
(456, 'How is the ramp used?', 128, 6),
(457, 'What kind of location is this? *', 128, 7),
(458, 'What is the location of your project? (Enter Zip Code) *', 129, 1),
(459, 'What kind of dock do you want?', 129, 2),
(460, 'What are the conditions where the dock will be installed? (Check all that apply)', 129, 3),
(461, 'What is the location of your project? (Enter Zip Code) *', 130, 1),
(462, 'What is the nature of this project? *', 130, 2),
(463, 'What primary function will the fence serve? (Check all that apply)', 130, 3),
(464, 'What is the location of your project? (Enter Zip Code) *', 131, 1),
(465, 'Select the kind(s) of repair you need.', 131, 2),
(466, 'What is the height of the fence you want to have repaired?', 131, 3);

-- --------------------------------------------------------

--
-- Table structure for table `report_abuse_customers`
--

CREATE TABLE IF NOT EXISTS `report_abuse_customers` (
  `id` int(11) NOT NULL,
  `abuse_from` int(11) NOT NULL,
  `abuse_to` int(11) NOT NULL,
  `abuse_message` text NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_abuse_customers`
--

INSERT INTO `report_abuse_customers` (`id`, `abuse_from`, `abuse_to`, `abuse_message`, `date`, `status`) VALUES
(1, 1, 16, 'this is the test abuse message', '2017-04-25 01:27:02', 0),
(3, 12, 17, 'this is the test abuse message1', '2017-04-25 01:28:03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `report_abuse_providers`
--

CREATE TABLE IF NOT EXISTS `report_abuse_providers` (
  `id` int(11) NOT NULL,
  `abuse_from` int(11) NOT NULL,
  `abuse_to` int(11) NOT NULL,
  `abuse_message` text NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_abuse_providers`
--

INSERT INTO `report_abuse_providers` (`id`, `abuse_from`, `abuse_to`, `abuse_message`, `date`, `status`) VALUES
(1, 26, 11, 'This is the test abuse against customer', '2017-04-25 03:24:06', 0),
(2, 26, 14, 'this is the test abuse against customer1', '2017-04-25 03:25:06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(27, 'Subadmin');

-- --------------------------------------------------------

--
-- Table structure for table `roles_accesses`
--

CREATE TABLE IF NOT EXISTS `roles_accesses` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `accessibility` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles_accesses`
--

INSERT INTO `roles_accesses` (`id`, `role_id`, `accessibility`) VALUES
(12, 27, 'a:6:{i:0;s:10:"managelogo";i:1;s:14:"managesettings";i:2;s:12:"managebanner";i:3;s:5:"leads";i:4;s:11:"reportabuse";i:5;s:3:"faq";}');

-- --------------------------------------------------------

--
-- Table structure for table `serviceareas`
--

CREATE TABLE IF NOT EXISTS `serviceareas` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `serviceareas`
--

INSERT INTO `serviceareas` (`id`, `name`, `status`) VALUES
(1, 'Newtown', '1'),
(2, 'Chingrighata', '1'),
(3, 'Rubi', '1'),
(4, 'Jadavpur', '1'),
(5, 'Garia', '1'),
(6, 'Exide', '1'),
(7, 'Ultadanga', '1'),
(8, 'Airport', '1');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `paypal_email` varchar(255) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_email` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `fav_icon` varchar(256) NOT NULL,
  `site_url` varchar(255) NOT NULL,
  `bannertext` text NOT NULL,
  `bannerimage` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(20) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `set_commission` varchar(256) NOT NULL,
  `featured_provider_vat` varchar(256) NOT NULL,
  `featured_course_vat` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `paypal_email`, `site_name`, `site_email`, `logo`, `fav_icon`, `site_url`, `bannertext`, `bannerimage`, `address`, `phone`, `skype`, `set_commission`, `featured_provider_vat`, `featured_course_vat`) VALUES
(1, 'nits.aprita@gmail.com', 'service pro', 'nits.ishani@gmail.com', '119963895_images.png', '401722763_index.png', 'http://104.131.83.218/team6/service_pro/admin', '', '', 'Webel IT Park, DH Block(Newtown), Action Area 2, Newtown, New Town, West Bengal', '+86-13-7616-98467', '', '20', '8', '5');

-- --------------------------------------------------------

--
-- Table structure for table `settings_BKP`
--

CREATE TABLE IF NOT EXISTS `settings_BKP` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_email` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `site_url` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings_BKP`
--

INSERT INTO `settings_BKP` (`id`, `site_name`, `site_email`, `logo`, `site_url`) VALUES
(1, 'Service Pro', 'nits.subhradip@gmail.com', '', 'http://104.131.83.218/team6/road_friend/admin/');

-- --------------------------------------------------------

--
-- Table structure for table `userresponses`
--

CREATE TABLE IF NOT EXISTS `userresponses` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `service_id` int(255) NOT NULL,
  `lead_id` int(255) NOT NULL,
  `q_id` int(255) NOT NULL,
  `a_id` int(255) NOT NULL,
  `answer` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userresponses`
--

INSERT INTO `userresponses` (`id`, `user_id`, `service_id`, `lead_id`, `q_id`, `a_id`, `answer`) VALUES
(1, 11, 5, 1, 4, 0, 'Birthday party'),
(2, 11, 5, 1, 5, 0, 'Evening'),
(3, 12, 6, 2, 9, 0, 'Flat'),
(4, 12, 6, 2, 10, 0, 'Flat'),
(5, 12, 6, 2, 11, 0, 'Only furniture'),
(6, 28, 8, 3, 13, 0, 'Only marriage day'),
(7, 28, 7, 4, 6, 0, 'Wedding'),
(8, 28, 7, 4, 7, 0, 'Evening'),
(9, 28, 7, 4, 8, 0, 'all type of guests'),
(10, 28, 9, 4, 14, 0, 'Makeup and hair style');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_loggedin` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_available` enum('Y','N') NOT NULL DEFAULT 'N',
  `social_user_id` varchar(25) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(50) DEFAULT NULL COMMENT 'U=User,SP=Service provide,FU=Facebook User,GU=Google user',
  `last_login` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `left_sidebar` varchar(255) DEFAULT NULL,
  `is_admin` int(11) NOT NULL DEFAULT '0',
  `service_id` int(11) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `fcm_token` varchar(255) DEFAULT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `social_type` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `pending` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `mobile_no`, `image`, `is_loggedin`, `is_available`, `social_user_id`, `registration_date`, `type`, `last_login`, `status`, `left_sidebar`, `is_admin`, `service_id`, `gender`, `token`, `fcm_token`, `device_type`, `social_type`, `role`, `pending`) VALUES
(1, 'Subhradip Jana', 'nits.subhradip@gmail.com', 'c33367701511b4f6020ec61ded352059', '07845978123', '58a2f9c0258d2.png', 'N', 'N', NULL, '0000-00-00 00:00:00', 'U', NULL, 1, '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(16, 'Ashis Mondal', 'nits.ashis@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9641047997', '58edc1b2aa49a.png', 'N', 'N', NULL, '0000-00-00 00:00:00', 'SP', NULL, 1, NULL, 0, 5, NULL, NULL, NULL, NULL, NULL, '27', 1),
(11, 'Sudipto Sirkar', 'nits.sudipto@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9641047997', '58be653c39d65.png', 'N', 'N', NULL, '0000-00-00 00:00:00', 'U', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(12, 'Soutik Bose', 'nits.soutik@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9641046789', '58be6567444b2.png', 'N', 'N', NULL, '0000-00-00 00:00:00', 'U', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(13, 'Soumen Bhatt', 'nits.soumen@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9641047997', '58be7d706d527.png', 'N', 'N', NULL, '0000-00-00 00:00:00', 'U', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(14, 'Anirban Singh', 'nits.anirban@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '96410479997', '58be7ddf10881.png', 'N', 'N', NULL, '0000-00-00 00:00:00', 'U', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(17, 'Indranil Gupta', 'nits.indranil@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '96473455434', '58edc21d31151.png', 'N', 'N', NULL, '0000-00-00 00:00:00', 'SP', NULL, 1, NULL, 0, 9, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(21, 'Rony Mitraa', 'nits.rony@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9641047997', '58ee19c42e593.png', 'N', 'N', NULL, '0000-00-00 00:00:00', 'SP', NULL, 1, NULL, 0, 6, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(22, 'Kartik Maiti', 'nits.kartik@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9641047997', '58ee1a389f287.png', 'N', 'N', NULL, '0000-00-00 00:00:00', 'U', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(23, 'Sandeep Tewary', 'nits.sandeeptewary@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9876543210', '58ee26048df84.jpeg', 'N', 'N', NULL, '0000-00-00 00:00:00', 'U', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(24, 'A Kundu123', 'nits.abhishek85@gmail.com', '202cb962ac59075b964b07152d234b70', '9876543210', '58ee2636e3cd0.jpeg', 'N', 'N', NULL, '0000-00-00 00:00:00', 'U', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(25, 'Ritwik patra', 'ritwik.netit@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '09932135511', '58fee0724e34d.jpg', 'N', 'N', NULL, '0000-00-00 00:00:00', 'SP', NULL, 1, NULL, 0, 6, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(27, 'Soumyo', 'nits.soumyo@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '123456', '58ff2ca1bd70c.jpg', 'N', 'N', NULL, '0000-00-00 00:00:00', 'SP', NULL, 1, NULL, 0, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(28, 'Ishani', 'nits.ishani@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '123456', '58ff2ea41ef15.jpg', 'N', 'N', NULL, '0000-00-00 00:00:00', 'U', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(30, 'arjun', 'nits.ashis@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9932135511', NULL, 'N', 'N', NULL, '0000-00-00 00:00:00', 'SP', NULL, 1, NULL, 0, 5, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(31, 'Suman Das', 'nits.suman@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9932135511', NULL, 'N', 'N', 'fb123u9', '0000-00-00 00:00:00', 'U', NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(32, 'Samaresh karak', 'nits.samaresh1@gmail.com', '', '8975481247', 'b.jpg', 'N', 'N', 'fb456fg', '0000-00-00 00:00:00', '', NULL, 0, NULL, 0, NULL, 'male', '1045', NULL, 'Androide', 'fbtype', NULL, 0),
(38, 'Nur Gazi', 'n.gazi143@gmail.com', '', NULL, 'https://graph.facebook.com/1172734086169230/picture?height=500&width=500&migration_overrides=%7Boctober_2012%3Atrue%7D', 'N', 'N', '1172734086169230', '0000-00-00 00:00:00', 'user', NULL, 0, NULL, 0, NULL, NULL, 'EAAJliqoJqLgBAJBTRG2iWhIhdNxLQdCH34cb3IJZB35LdRkzgvSVyqBdK1ZC11iAsbnyAQUp1zatdLMPPJQ2zDx4rz2xaLfkNz1kvIQfs9GPZAeVqAf1JV8d7LroQZBnTAaaZCHtaQyzzutdinLpVBkvk4x0ZB8l4OVVvugJFVZC6gxqjiD1uF8phFvTs8NCzvHRDPGynOVelKfCZBOtdbEh', NULL, 'android', 'facebook', NULL, 0),
(36, 'Satyajit jana', 'nits.satyajitjana@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '4578125874', NULL, 'N', 'N', NULL, '0000-00-00 00:00:00', 'SP', NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(39, 'NURUZZAMAN', 'n.gazi143@gmail.com', '', NULL, 'https://lh3.googleusercontent.com/-I72vuz4PSCU/AAAAAAAAAAI/AAAAAAAABNY/F8syxcINuZ8/photo.jpg', 'N', 'N', '104732023260898457293', '2017-05-16 13:20:00', 'user', NULL, 0, NULL, 0, NULL, NULL, '', NULL, 'android', 'google', NULL, 0),
(40, 'deb das', 'nits.deb@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9932135511', NULL, 'N', 'N', NULL, '2017-05-16 13:37:56', 'SP', NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, 'android', NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blockips`
--
ALTER TABLE `blockips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventnotifications`
--
ALTER TABLE `eventnotifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leftsideactions`
--
ALTER TABLE `leftsideactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `membership_plans`
--
ALTER TABLE `membership_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plants`
--
ALTER TABLE `plants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_images`
--
ALTER TABLE `post_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_abuse_customers`
--
ALTER TABLE `report_abuse_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_abuse_providers`
--
ALTER TABLE `report_abuse_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_accesses`
--
ALTER TABLE `roles_accesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `serviceareas`
--
ALTER TABLE `serviceareas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_BKP`
--
ALTER TABLE `settings_BKP`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userresponses`
--
ALTER TABLE `userresponses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1908;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blockips`
--
ALTER TABLE `blockips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `eventnotifications`
--
ALTER TABLE `eventnotifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `leftsideactions`
--
ALTER TABLE `leftsideactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `membership_plans`
--
ALTER TABLE `membership_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `plants`
--
ALTER TABLE `plants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=132;
--
-- AUTO_INCREMENT for table `post_images`
--
ALTER TABLE `post_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=322;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=467;
--
-- AUTO_INCREMENT for table `report_abuse_customers`
--
ALTER TABLE `report_abuse_customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `report_abuse_providers`
--
ALTER TABLE `report_abuse_providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `roles_accesses`
--
ALTER TABLE `roles_accesses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `serviceareas`
--
ALTER TABLE `serviceareas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings_BKP`
--
ALTER TABLE `settings_BKP`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `userresponses`
--
ALTER TABLE `userresponses`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
