<?php
$app->group('/user', function () use ($app) {
    $app->post('/login', "login");
    $app->post('/normalRegistration', "normalRegistration");
    $app->post('/socialLogin', "socialLogin");
    $app->post('/changePassword', "changePassword");
    $app->post('/homePage', "homePage");
    $app->post('/editProfile', "editProfile");
    $app->post('/profileDetails', "profileDetails");
});

function login() {
    $app        = \Slim\Slim::getInstance();
    
    $data       = array();
    
    $request    = $app->request();
    $getBody    = ($request->getBody());
    $body       = json_decode($getBody);

    $email      = $body->email;
    $password   = $body->password;
    $type       = $body->type;
    
    $db = getConnection();
    
    $sql = "SELECT id,name,email,image FROM `users` WHERE `email` = :email AND `password` = md5(:pass) AND `type` = :typ";
    
    try {
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam('email', $email);
        $stmt->bindParam('pass', $password);
        $stmt->bindParam('typ', $type);
        $stmt->execute();
        
        $rowCount = $stmt->rowCount();
        
        if($rowCount){
            $obj = $stmt->fetchObject();
            // unset($obj->password);
            
            $data['ack'] = 1;
            $data['msg'] = 'Success';
            $data['userDetails'] = $obj;
        } else {
            $data['ack'] = 0;
            $data['msg'] = 'Login Failed';
        }
        
    } catch (PDOException $ex) {
        $data['ack'] = 0;
        $data['msg'] = $ex->getMessage();
    }
    $db = NULL;
    $app->response->write(json_encode($data));
}

function normalRegistration() {
    $app        = \Slim\Slim::getInstance();
    
    $data       = array();
    
    $request    = $app->request();
    $getBody    = ($request->getBody());
    $body       = json_decode($getBody);
    
    $name       = $body->name;
    $email      = $body->email;
    $password   = $body->password;
    $mobile_no  = $body->mobile_no;
    $user_type  = $body->user_type;    
    $device_type= $body->device_type;    
       
    $db         = getConnection();

    $sql = "INSERT INTO `users` "."SET `name`=:nam,`email`=:emal, `password`=md5(:pass), `mobile_no`=:number, `type`=:tpe, `device_type`=:dtype";
       
    try {
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam('nam', $name);
        $stmt->bindParam('emal', $email);
        $stmt->bindParam('pass', $password);
        $stmt->bindParam('number', $mobile_no);
        $stmt->bindParam('tpe', $user_type);
        $stmt->bindParam('dtype', $device_type);
        
        $stmt->execute();
        
        $data['ack'] = 1;
        $data['msg'] = 'Successfully register';
        //echo $sql;
    } 
    catch (PDOException $ex) {
        $data['ack'] = 0;
        $data['msg'] = $ex->getMessage();
    }
    $db = NULL;
    $app->response->write(json_encode($data));
}

function socialLogin() {
    $app        = \Slim\Slim::getInstance();
    
    $data       = array();
    
    $request    = $app->request();
    $getBody    = ($request->getBody());
    $body       = json_decode($getBody);
    
    $id         = $body->id;
    $name       = $body->name;
    $email      = $body->email;
    $token      = $body->token;
    $profile_image   = $body->profile_image;
    $device_type  = $body->device_type;
    $user_type       = $body->user_type;    
    $social_type       = $body->social_type;    
       
    $db         = getConnection();

     $sql = "INSERT INTO `users` "."SET `name`=:nam,`email`=:emal, `token`=:tkn, `image`=:img, `social_user_id`=:sid, `device_type`=:dtpe, `type`=:utype, `social_type`=:stype";
       
    try {
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam('nam', $name);
        $stmt->bindParam('emal', $email);
        $stmt->bindParam('tkn', $token);
        $stmt->bindParam('img', $profile_image);
        $stmt->bindParam('sid', $id);
        $stmt->bindParam('dtpe', $device_type);
        $stmt->bindParam('utype', $user_type);
        $stmt->bindParam('stype', $social_type);
        
        $stmt->execute();
        
        $data['ack'] = 1;
        $data['msg'] = 'Successfully register';
        //echo $sql;
    } 
    catch (PDOException $ex) {
        $data['ack'] = 0;
        $data['msg'] = $ex->getMessage();
    }
    $db = NULL;
    $app->response->write(json_encode($data));
}

function changePassword() {
    $app        = \Slim\Slim::getInstance();
    
    $data       = array();

    $request    = $app->request();
    $getBody    = ($request->getBody());
    $body       = json_decode($getBody);
    
    $id             = $body->id;
    $oldPassword    = $body->oldPassword;
    $newPasword     = $body->newPasword; 
       
    $db         = getConnection();

    
    $resultsql = "SELECT `password` FROM `users` WHERE `id` = '$id'";

    $resultQuery = $db->prepare($resultsql);
    $resultQuery->execute();
    $obj = $resultQuery->fetchObject();

    $passwrd = $obj->password;
   
    if(md5($oldPassword) == $passwrd){

        $sql = "UPDATE `users` SET `password` = md5(:pass) WHERE `id` = '$id'";
  
        try {
             
            $stmt = $db->prepare($sql); 
            $stmt->bindParam('pass', $newPasword);
            
            $stmt->execute();
            
            $data['ack'] = 1;
            $data['msg'] = 'Successfully change Password';
            //echo $sql;
        } 
        catch (PDOException $ex) {
            $data['ack'] = 0;
            $data['msg'] = $ex->getMessage();
        }
    }else{
        $data['ack'] = 2;
        $data['msg'] = 'Enter Wrong Old Password';
    }
        
    
    
    $db = NULL;
    $app->response->write(json_encode($data));
}

function homePage() {
    $app        = \Slim\Slim::getInstance();
    
    $data       = array();

    $request    = $app->request();
    $getBody    = ($request->getBody());
    $body       = json_decode($getBody);
    
    $db         = getConnection();

    $resultsql = "SELECT `logo` FROM `settings` WHERE `id` = 1";

    $resultQuery = $db->prepare($resultsql);
    $resultQuery->execute();
    $obj = $resultQuery->fetchObject();

    $data['logo'] = $obj->logo;
   
        $sql = "SELECT `name` FROM `categories` WHERE `status` = 1";
  
        try {
             
            $stmt = $db->prepare($sql); 

            $stmt->execute();

            while($obj=$stmt->fetchObject()){
            	$data['category'][] = $obj;
            }
            $data['ack'] = 1; 
        } 
        catch (PDOException $ex) {
            $data['ack'] = 0;
            $data['msg'] = $ex->getMessage();
        }
    
    $db = NULL;
    $app->response->write(json_encode($data));
}

function editProfile() {
    $app        = \Slim\Slim::getInstance();
    
    $data       = array();

    $request    = $app->request();
    $getBody    = ($request->getBody());
    $body       = json_decode($getBody);
    
    $id            = $body->id;
    $name          = $body->name;
    $email         = $body->email; 
    $mobile_no     = $body->mobile_no; 
    $profile_image = $body->profile_image; 
       
    $db         = getConnection();

    $sql = "UPDATE `users` SET `name`=:nam, `email`=:eml, `mobile_no`=:num, `image`=:img WHERE `id` = '$id'";

    try {
         
        $stmt = $db->prepare($sql); 
        $stmt->bindParam('nam', $name);
        $stmt->bindParam('eml', $email);
        $stmt->bindParam('num', $mobile_no);
        $stmt->bindParam('img', $profile_image);
        
        $stmt->execute();
        
        $data['ack'] = 1;
        $data['msg'] = 'Successfully profile updated';
        //echo $sql;
    } 
    catch (PDOException $ex) {
        $data['ack'] = 0;
        $data['msg'] = $ex->getMessage();
    }
    
    $db = NULL;
    $app->response->write(json_encode($data));
}

function profileDetails() {
    $app        = \Slim\Slim::getInstance();
    
    $data       = array();

    $request    = $app->request();
    $getBody    = ($request->getBody());
    $body       = json_decode($getBody);
    
    $id            = $body->id;
    
    $db         = getConnection();

    $sql = "SELECT * from `users` WHERE `id` = '$id'";

    try {
         
        $stmt = $db->prepare($sql); 
        
        $stmt->execute();

        $obj = $stmt->fetchObject();

        $data['userDetails'] = $obj;
        
        $data['ack'] = 1;
        //$data['msg'] = 'Successfully profile updated';
        //echo $sql;
    } 
    catch (PDOException $ex) {
        $data['ack'] = 0;
        $data['msg'] = $ex->getMessage();
    }
    
    $db = NULL;
    $app->response->write(json_encode($data));
}