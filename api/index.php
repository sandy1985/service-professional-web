<?php
require 'vendor/autoload.php';
require_once 'config/config.php';

$app = new \Slim\Slim(array(
    'debug' => true
));

require_once 'config/middleware.php';
require_once 'public/user.php';

$app->run();