<?php
App::uses('AppController', 'Controller');
/**
 * CarCategories Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class CategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
public $components = array('Paginator');


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->EmailTemplate->recursive = 0;
		$this->set('emailTemplates', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */


/**
 * add method
 *
 * @return void
 */
	    public function admin_add() {

            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }

            if ($this->request->is('post')) {
            if(!empty($this->request->data['Category']['image']['name'])){
            $pathpart=pathinfo($this->request->data['Category']['image']['name']);
            $ext=$pathpart['extension'];
            $extensionValid = array('jpg','jpeg','png','gif');
            if(in_array(strtolower($ext),$extensionValid)){
            $uploadFolder = "category_images/";
            $uploadPath = WWW_ROOT . $uploadFolder;
            $filename =uniqid().'.'.$ext;
            $full_flg_path = $uploadPath . '/' . $filename;
            move_uploaded_file($this->request->data['Category']['image']['tmp_name'],$full_flg_path);
            }
            else{
             $this->Session->setFlash(__('Invalid image type.'));
            }
            }
           else{
            $filename='';
           }
           $this->request->data['Category']['image'] = $filename;
           $this->Category->create();
            if ($this->Category->save($this->request->data)) {
                    $this->Session->setFlash('The category has been saved.','default', array('class' => 'success'));
                    return $this->redirect(array('action' => 'index'));
            } else {
                    $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
	  }
	 }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->EmailTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid email template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->EmailTemplate->save($this->request->data)) {
				$this->Session->setFlash(__('The email template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email template could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('EmailTemplate.' . $this->EmailTemplate->primaryKey => $id));
			$this->request->data = $this->EmailTemplate->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	    public function admin_delete($id = null) {

                $userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
		if(!isset($is_admin) && $is_admin==''){
		   $this->redirect('/admin');
		}
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid community'));
		}
		//$this->request->onlyAllow('post', 'delete');
		if ($this->Category->delete()) {
		    //$this->UserImage->delete()
			$this->Session->setFlash('The category has been deleted.','default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	    }


            public function admin_index() {

            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }

		$title_for_layout = 'Category List';
		 $this->paginate = array('order' => array('Category.id' => 'desc'));
		//$this->Carcategory->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('carcategory', $this->Paginator->paginate());
		$this->set(compact('title_for_layout'));
	    }

	    public function admin_edit($id = null) {
		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
                if(!isset($is_admin) && $is_admin==''){
                   $this->redirect('/admin');
                }

		if ($this->request->is(array('post', 'put'))) {

                        $this->request->data['Category']['id']=$id;

                        if(!empty($this->request->data['Category']['image']['name'])){
                        $pathpart=pathinfo($this->request->data['Category']['image']['name']);
                        $ext=$pathpart['extension'];
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array(strtolower($ext),$extensionValid)){
                        $uploadFolder = "category_images/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename =uniqid().'.'.$ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['Category']['image']['tmp_name'],$full_flg_path);
                        }
                        else{
                         $this->Session->setFlash(__('Invalid image type.'));
                        }
                        }
                       else{
                        $filename=$this->request->data['Category']['img'];
                       }
                        $this->request->data['Category']['image'] = $filename;

			if ($this->Category->save($this->request->data)) {

                         $this->Session->setFlash('The category has been saved.','default', array('class' => 'success'));
                         return $this->redirect(array('action' => 'index'));

			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
			$car = $this->Category->find('first', $options);
                        //print_r($car);
                        //exit;
                        $this->set(compact('car'));
		}

        }











}
