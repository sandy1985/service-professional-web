<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

	/*function beforeFilter() {
		parent::beforeFilter();
	}*/
/**
 * Components
 *
 * @var array
 */
	public $name = 'Users';
	public $components = array('Session','RequestHandler','Paginator');
	var $uses = array('User','EmailTemplate','Setting','Newsletter','Eventnotification','Leftsideaction','Contact');





	public function admin_index() {

		$title_for_layout = 'Admin Login';
		$this->set(compact('title_for_layout'));
		 $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(isset($is_admin) && $is_admin!=''){
                $this->redirect('/admin/users/edit/'.$userid);
            }
		#$this->User->recursive = 0;
		#$this->set('users', $this->Paginator->paginate());
	}

  public function admin_dashboard() {

	    $userid = $this->Session->read('adminuserid');
      $is_admin = $this->Session->read('is_admin');

      if($userid=='' && $userid==''){
         $this->redirect('/admin');
      }
      $this->loadModel('User');
      $this->loadModel('Category');
      $this->loadModel('Post');
      $users = $this->User->find('all');

      $spusers = $this->User->find('all',array('conditions'=>array('User.type'=>'SP')));

      $cat = $this->Category->find('all');

      $post = $this->Post->find('all');

      $this->set(compact('users','spusers','cat','post'));

	}


	public function admin_fotgot_password(){
            $title_for_layout = 'Forgot Password';
            $this->set(compact('title_for_layout'));

            if ($this->request->is(array('post', 'put'))){
                $options = array('conditions' => array('User.email' => $this->request->data['User']['email'],'User.is_admin' => 1));

                $user = $this->User->find('first', $options);
				//pr($user);
                if($user){
				$contact_email = $this->Setting->find('first', array('conditions' => array('Setting.id' => 1), 'fields' => array('Setting.site_email', 'Setting.site_name')));
				   $site_name = $contact_email['Setting']['site_name'];
                    //$password = $this->User->get_fpassword();
                    $siteurl="http://104.131.83.218/team6/service_pro/";
		   $link='<a href="'.$siteurl.'admin/users/reset_password/'.base64_encode($user['User']['id']).'">Reset Password</a>';
                    //$this->request->data['User']['id'] = $user['User']['id'];
                    //$this->request->data['User']['password'] = $password;
                   	   $adminEmail = 'superadmin@abc.com';
                       $key = Configure::read('SITE_URL');
                       $this->loadModel('EmailTemplate');
                       $EmailTemplate=$this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.id'=>1)));
                       $Subject_mail=$site_name.' Forgot Password';
                       $from='admin@fuga.in';
		       $msg_body =str_replace(array('[USER]','[EMAIL]','[LINK]'),array($user['User']['name'],$user['User']['email'],$link),$EmailTemplate['EmailTemplate']['content']);

		       $this->php_mail($user['User']['email'],$from,$Subject_mail,$msg_body);
                       $this->Session->setFlash('A new password has been sent to your mail. Please check mail.', 'default', array('class' => 'success'));


                }else {
                   $this->Session->setFlash("Invalid email or You are not authorize to access.");
                }
            }
        }


        public function admin_reset_password($userid = null) {
	  $title_for_layout='Reset Password';
           $this->set(compact('userid','title_for_layout'));

            if ($this->request->is(array('post', 'put'))){
		$userid = base64_decode($this->request->data['User']['id']);
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $userid));
            	$user=$this->User->find('first', $options);
		if(count($user) > 0){
		if($this->request->data['User']['new_pass'] == $this->request->data['User']['con_pass']){
                $user_data_auth['User']['id']=$userid;
                $user_data_auth['User']['password']=md5($this->request->data['User']['new_pass']);
                if($this->User->save($user_data_auth)){
                $this->Session->setFlash('Password updated successfully.', 'default', array('class' => 'success'));
                return $this->redirect(array('action' => 'index'));
                }
                    }else {
                            $this->Session->setFlash('Wrong user.', 'default');
                            return $this->redirect(array('action' => 'reset_password'));
                    }
		}


                }
                else {
                    if(!isset($userid) && $userid=='')
                    {
                            $this->Session->setFlash(__('Wrong url for reset.', 'default', array('class' => 'error')));
                            return $this->redirect(array('action' => 'forgot_password'));
                    }
                }

		}





            public function admin_login() {

              $userid = $this->Session->read('adminuserid');
              $is_admin = $this->Session->read('is_admin');

            if(isset($is_admin) && $is_admin!=''){
               $this->redirect('/admin/users/dashboard/');
            }
		if ($this->request->is('post')) {
			$options = array('conditions' => 
			    array('User.email'  => $this->request->data['User']['usernamel'],'User.password' => md5($this->request->data['User']['passwordl']),'User.status'=>1,'OR' => array('User.role !='=> '0','User.is_admin'=>1)));
			$loginuser = $this->User->find('first', $options);
			if(!$loginuser){
				$this->Session->setFlash(__('Invalid email or password, try again', 'default', array('class' => 'error')));
				return $this->redirect(array('action' => 'admin_index'));
			} else {
				$this->Session->write('adminuserid', $loginuser['User']['id']);
                $this->Session->write('is_admin', $loginuser['User']['is_admin']);
				$this->Session->setFlash('You have successfully logged in','default', array('class' => 'success'));
				return $this->redirect(array('action' => 'dashboard'));
			}
		}
	}


	public function admin_logout() {
		#return $this->redirect($this->Auth->logout());
		$this->Session->delete('adminuserid');
        $this->Session->delete('is_admin');
		$this->redirect('/admin');
	}


	public function admin_edit($id = null) {
	    //$this->loadModel('UserImage');
            $userid = $this->Session->read('adminuserid');
           $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }

	    $this->request->data1=array();
		$title_for_layout = 'User Edit';
		$this->set(compact('title_for_layout'));
		//$service_areas=$this->User->ServiceArea->find('list');
		//$this->set(compact('title_for_layout','service_areas'));
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {

			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('The user has been saved.','default', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit',$id));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {

			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}


        public function admin_edit_customer($id = null) {
	    //$this->loadModel('UserImage');
            $userid = $this->Session->read('adminuserid');
           $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }

	    $this->request->data1=array();
		$title_for_layout = 'User Edit';
		$this->set(compact('title_for_layout'));
		//$service_areas=$this->User->ServiceArea->find('list');
		//$this->set(compact('title_for_layout','service_areas'));
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {

                        if(!empty($this->request->data['User']['image']['name'])){
                        $pathpart=pathinfo($this->request->data['User']['image']['name']);
                        $ext=$pathpart['extension'];
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array(strtolower($ext),$extensionValid)){
                        $uploadFolder = "user_images/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename =uniqid().'.'.$ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['User']['image']['tmp_name'],$full_flg_path);
                        }
                        else{
                         $this->Session->setFlash(__('Invalid image type.'));
                        }
                       }
                       else{
                        $filename=$this->request->data['User']['img'];
                       }
                        $this->request->data['User']['image'] = $filename;


			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('The user has been saved.','default', array('class' => 'success'));
				return $this->redirect(array('action' => 'list'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {

			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}



	   function admin_changepwd()
            {
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
             if ($this->request->is(array('post', 'put'))) {

             $this->User->create();
             $this->request->data['User']['password']=md5($this->request->data['User']['password']);
			  $this->request->data['User']['id']=$userid;
             if($this->User->save($this->request->data))
             {
              $this->Session->setFlash('Your password changed successfully.', 'default', array('class' => 'success'));
	      return $this->redirect(array('action' => 'changepwd'));

             }
             else
             {

             }
             }
            }


     public function admin_add() {
     $this->loadModel('UserImage');
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
     //$countries=$this->User->Country->find('list');
     $this->request->data1=array();
     $title_for_layout = 'User Add';

      if ($this->request->is('post')) {
      $options = array('conditions' => array('User.email'  => $this->request->data['User']['email']));
      $emailexists = $this->User->find('first', $options);
      if(!$emailexists)
      {

      if(!empty($this->request->data['User']['image']['name'])){
      $pathpart=pathinfo($this->request->data['User']['image']['name']);
      $ext=$pathpart['extension'];
      $extensionValid = array('jpg','jpeg','png','gif');
      if(in_array(strtolower($ext),$extensionValid)){
      $uploadFolder = "user_images/";
      $uploadPath = WWW_ROOT . $uploadFolder;
      $filename =uniqid().'.'.$ext;
      $full_flg_path = $uploadPath . '/' . $filename;
      move_uploaded_file($this->request->data['User']['image']['tmp_name'],$full_flg_path);
      }
      else{
       $this->Session->setFlash(__('Invalid image type.'));
      }
     }
     else{
      $filename='';
     }
      $this->request->data['User']['image'] = $filename;

      $this->request->data['User']['registration_date'] =date("d-m-Y");
      $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
      $this->request->data['User']['status'] = 1;
      $this->request->data['User']['type'] = "U";
      $this->User->create();

     if ($this->User->save($this->request->data)) {

      $this->Session->setFlash('The user has been saved.','default', array('class' => 'success'));
      return $this->redirect(array('action' => 'list'));
     } else {
      $this->Session->setFlash(__('The user could not be saved. Please, try again.', 'default', array('class' => 'error')));
     }

    } else {
     $this->Session->setFlash(__('Email already exists. Please, try another.', 'default', array('class' => 'error')));
    }
   }


  }

	   public function admin_list() {
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
	    // $countries=$this->User->Country->find('list');
            $title_for_layout = 'User List';
            //$active_user=$this->User->find("count",array('conditions'=>array('User.status'=>1,'User.id !='=>2, 'User.is_admin'=>0)));
            //$inactive_user=$this->User->find("count",array('conditions'=>array('User.status'=>0,'User.id !='=>2, 'User.is_admin'=>0)));
            if ($this->request->is(array('post', 'put'))) {
		      //pr($this->request->data);exit;

            $email=$this->request->data['email'];
            $name=$this->request->data['name'];
            $QueryStr="(User.is_admin ='0' AND User.type ='U')";
            if($name!=''){
                $QueryStr.=" AND (User.name like '%".$name."%')";
            }
            if($email!=''){
                $QueryStr.=" AND (User.email = '".$email."')";
            }


                $options = array('conditions' => array($QueryStr) , 'order' => array('User.id' => 'desc'));
               // pr($options);
                //exit;
                //$Newsearch_is_active=$search_is_active;
            }else{
                $options = array('conditions' => array('User.is_admin'=>0,'User.type'=>'U'), 'order' => array('User.id' => 'desc'));

                $email='';
                $name='';
            }

            //$options = array('User.id !=' => 2);
            //$this->set('user', $this->User->find('first', $options));
            $this->Paginator->settings = $options;
            $this->set('users', $this->Paginator->paginate('User'));
            $this->set(compact('title_for_layout','country','email','name','active_user','inactive_user'));
        }





            public function admin_delete($id = null) {

            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            $this->User->id = $id;
            if (!$this->User->exists()) {
             throw new NotFoundException(__('Invalid user'));
            }
            //$this->request->onlyAllow('post', 'delete');
            if ($this->User->delete()) {
                //$this->UserImage->delete()
                 $this->Session->setFlash('The user has been deleted.','default', array('class' => 'success'));
            } else {
                 $this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
            }

            return $this->redirect(array('action' => 'admin_list'));

            }


            public function admin_delete_service_provider($id = null) {

            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
            $this->User->id = $id;
            if (!$this->User->exists()) {
             throw new NotFoundException(__('Invalid user'));
            }
            //$this->request->onlyAllow('post', 'delete');
            if ($this->User->delete()) {
                //$this->UserImage->delete()
                 $this->Session->setFlash('The user has been deleted.','default', array('class' => 'success'));
            } else {
                 $this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
            }

            return $this->redirect(array('action' => 'admin_list_service_provider'));

            }



	    public function admin_block($id = null) {

		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
		if(!isset($is_admin) && $is_admin==''){
		   $this->redirect('/admin');
		}
		 $this->request->data['User']['id'] = $id;
		 $this->request->data['User']['status'] =0;

		if ($this->User->save($this->request->data)) {
		    //$this->UserImage->delete()
			$this->Session->setFlash('The user has been blocked.','default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The user could not be blocked. Please, try again.'));
		}
		//return $this->redirect(array('action' => 'admin_list'));

                $option1 = array('conditions' => array('User.id' => $id));
                $checktype = $this->User->find('first', $option1);
                //print_r($option1);
                //echo $type=$checktype['User']['type'];
                //exit;
                if($checktype['User']['type']=="SP"){
		return $this->redirect(array('action' => 'admin_list_service_provider'));
                }else{
                return $this->redirect(array('action' => 'admin_list'));

                }


	}

		public function admin_unblock($id = null) {

		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
		if(!isset($is_admin) && $is_admin==''){
		   $this->redirect('/admin');
		}
		 $this->request->data['User']['id'] = $id;
		 $this->request->data['User']['status'] =1;

		if ($this->User->save($this->request->data)) {
		    //$this->UserImage->delete()
			$this->Session->setFlash('The user has been unblocked.','default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The user could not be unblocked. Please, try again.'));
		}
		//return $this->redirect(array('action' => 'admin_list'));
                $option1 = array('conditions' => array('User.id' => $id));
                 $checktype = $this->User->find('first', $option1);
                //print_r($option1);
                //echo $type=$checktype['User']['type'];
                //exit;
                if($checktype['User']['type']=="SP"){
		return $this->redirect(array('action' => 'admin_list_service_provider'));
                }else{
                return $this->redirect(array('action' => 'admin_list'));

                }

	}


        public function active_account($code){

	  $userid = base64_decode($code);

          $data['User']['id']=$userid;
	  $data['User']['status']=1;
            if($this->User->save($data)){
                    $this->Session->setFlash('Your account hasbeen activated.', 'default', array('class' => 'success'));
                    //return $this->redirect(array('action' => 'active_account'));
                    echo "Now your account has been actived";
            }
            else {
                    $this->Session->setFlash('Wrong information.', 'default');
                    //return $this->redirect(array('action' => 'registartion'));
                    echo "Your account is not actived";
            }

            exit;
	 }


		public function admin_reset_pass($userid = null) {
			$title_for_layout='Reset Password';
           $this->set(compact('userid','title_for_layout'));

            if ($this->request->is(array('post', 'put'))){

					if($this->request->data['User']['new_password'] == $this->request->data['User']['confirm_password']){
                        $user_data_auth['User']['id']=$userid;
                        $user_data_auth['User']['password']=md5($this->request->data['User']['new_password']);
                        if($this->User->save($user_data_auth)){
                            $this->Session->setFlash('Password updated successfully.', 'default', array('class' => 'success'));
                            return $this->redirect(array('action' => 'reset_pass'));
                        }
                    }else {
                            $this->Session->setFlash('Password doesnot match.', 'default');
                            return $this->redirect(array('action' => 'reset_pass'));
                    }


              }

             }



    public function admin_add_subadmin() {

    $userid = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if(!isset($is_admin) && $is_admin==''){
       $this->redirect('/admin');
    }

    if ($this->request->is('post')) {
    $options = array('conditions' => array('User.email'  => $this->request->data['User']['email']));
    $emailexists = $this->User->find('first', $options);
    if(!$emailexists)
    {
      $this->request->data['User']['left_sidebar'] = implode(",",$this->request->data['User']['left_sidebar']);
      $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
      $this->request->data['User']['status'] = 1;
      $this->request->data['User']['registration_date'] = date('d-m-Y');
      $this->request->data['User']['is_admin'] = 2;
      $this->User->create();

     if ($this->User->save($this->request->data)) {

      $this->Session->setFlash('The subadmin has been saved.','default', array('class' => 'success'));
      return $this->redirect(array('action' => 'admin_list_subadmin'));
     } else {
      $this->Session->setFlash(__('The subadmin could not be saved. Please, try again.', 'default', array('class' => 'error')));
     }

    } else {
     $this->Session->setFlash(__('Email already exists. Please, try another.', 'default', array('class' => 'error')));
    }
   }
   $leftaction = array('conditions' => array('Leftsideaction.status' => 1),'fields'=>array('Leftsideaction.leftaction'));
   $leftactions = $this->Leftsideaction->find('list', $leftaction);
   $this->set(compact('leftactions'));
  }


		public function admin_list_subadmin() {
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
	    // $countries=$this->User->Country->find('list');
            $title_for_layout = 'Subadmin List';

            $options = array('conditions' => array('User.is_admin =' => 2), 'order' => array('User.id' => 'desc'));
            //$options = array('User.id !=' => 2);
            //$this->set('user', $this->User->find('first', $options));
            $this->Paginator->settings = $options;
            $this->set('subadmins', $this->Paginator->paginate('User'));
            //$this->set(compact('title_for_layout','country','email','name','active_user','inactive_user'));
           }
           public function admin_edit_subadmin($id = null) {
	    //$this->loadModel('UserImage');
           $userid = $this->Session->read('adminuserid');
           $is_admin = $this->Session->read('is_admin');
           if(!isset($is_admin) && $is_admin==''){
           $this->redirect('/admin');
           }

	    $this->request->data1=array();
		$title_for_layout = 'Subadmin Edit';
		$this->set(compact('title_for_layout'));
		//$service_areas=$this->User->ServiceArea->find('list');
		//$this->set(compact('title_for_layout','service_areas'));
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
		     $this->request->data['User']['left_sidebar'] = implode(",",$this->request->data['User']['left_sidebar']);
		     $this->request->data['User']['id']=$id;
		     if ($this->User->save($this->request->data)) {
			$this->Session->setFlash('The subadmin has been saved.','default', array('class' => 'success'));
			return $this->redirect(array('action' => 'list_subadmin'));
			} else {
			$this->Session->setFlash(__('The subadmin could not be saved. Please, try again.'));
			}
		} else {

			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);

			$leftaction = array('conditions' => array('Leftsideaction.status' => 1),'fields'=>array('Leftsideaction.leftaction'));
			$leftactions = $this->Leftsideaction->find('list', $leftaction);
			$this->set(compact('leftactions'));
		    }
	}


          public function admin_delete_subadmin($id = null) {

		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
		if(!isset($is_admin) && $is_admin==''){
		   $this->redirect('/admin');
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		//$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
		    //$this->UserImage->delete()
			$this->Session->setFlash('The subadmin has been deleted.','default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The subadmin could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'admin_list_subadmin'));
	  }

	  public function admin_block_subadmin($id = null) {

		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
		if(!isset($is_admin) && $is_admin==''){
		   $this->redirect('/admin');
		}
		 $this->request->data['User']['id'] = $id;
		 $this->request->data['User']['status'] =0;

		if ($this->User->save($this->request->data)) {
		    //$this->UserImage->delete()
			$this->Session->setFlash('The subadmin has been blocked.','default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The subadmin could not be blocked. Please, try again.'));
		}
		return $this->redirect(array('action' => 'admin_list_subadmin'));
	    }

		public function admin_unblock_subadmin($id = null) {

		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
		if(!isset($is_admin) && $is_admin==''){
		   $this->redirect('/admin');
		}
		 $this->request->data['User']['id'] = $id;
		 $this->request->data['User']['status'] =1;

		if ($this->User->save($this->request->data)) {
		    //$this->UserImage->delete()
			$this->Session->setFlash('The subadmin has been unblocked.','default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The subadmin could not be unblocked. Please, try again.'));
		}
		return $this->redirect(array('action' => 'admin_list_subadmin'));
	        }


     public function admin_add_service_provider() {

     $this->loadModel('UserImage');
     $this->loadModel('Post');
     $userid = $this->Session->read('adminuserid');
     $is_admin = $this->Session->read('is_admin');
     if(!isset($is_admin) && $is_admin==''){
      $this->redirect('/admin');
     }
     //$countries=$this->User->Country->find('list');

     $pst=$this->Post->find('list',array('Post.id','Post.name'));
    
     $this->request->data1=array();
     $title_for_layout = 'User Add';

     if ($this->request->is('post')) {

	     $options = array('conditions' => array('User.email'  => $this->request->data['User']['email']));
	     $emailexists = $this->User->find('first', $options);
	     if(!$emailexists)
	     {

	      if(!empty($this->request->data['User']['image']['name'])){
	      $pathpart=pathinfo($this->request->data['User']['image']['name']);
	      $ext=$pathpart['extension'];
	      $extensionValid = array('jpg','jpeg','png','gif');
	      if(in_array(strtolower($ext),$extensionValid)){
	      $uploadFolder = "user_images/";
	      $uploadPath = WWW_ROOT . $uploadFolder;
	      $filename =uniqid().'.'.$ext;
	      $full_flg_path = $uploadPath . '/' . $filename;
	      move_uploaded_file($this->request->data['User']['image']['tmp_name'],$full_flg_path);
	      }
	      else{
	       $this->Session->setFlash(__('Invalid image type.'));
	      }
	      }
	      else{
	      $filename='';
	      }
	      $this->request->data['User']['image'] = $filename;
	      $this->request->data['User']['type'] = 'SP';
	      $this->request->data['User']['registration_date'] =date("d-m-Y");
	      $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
	      $this->request->data['User']['status'] = 1;
	      $this->User->create();

	      if ($this->User->save($this->request->data)) {

	      $this->Session->setFlash('The service provider  has been saved.','default', array('class' => 'success'));
	      return $this->redirect(array('action' => 'admin_list_service_provider'));
	      } else {
	      $this->Session->setFlash(__('The service provider could not be saved. Please, try again.', 'default', array('class' => 'error')));
	      }

	     } else {
	     $this->Session->setFlash(__('Email already exists. Please, try another.', 'default', array('class' => 'error')));
	     }
	    }
	     $this->set(compact('pst'));
    }


        public function admin_list_service_provider() {

        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if(!isset($is_admin) && $is_admin==''){
           $this->redirect('/admin');
        }
        $title_for_layout = 'Service Provider List';

        if ($this->request->is(array('post', 'put'))) {

        $email=$this->request->data['email'];
        $name=$this->request->data['name'];
        $QueryStr="(User.type ='SP'  AND User.is_admin ='0')";
        if($name!=''){
            $QueryStr.=" AND (User.name like '%".$name."%')";
        }
        if($email!=''){
            $QueryStr.=" AND (User.email = '".$email."')";
        }


            $options = array('conditions' => array($QueryStr) , 'order' => array('User.id' => 'desc'));
           // pr($options);
            //exit;
            //$Newsearch_is_active=$search_is_active;
        }else{
            $options = array('conditions' => array('User.type =' => 'SP', 'User.is_admin'=>0), 'order' => array('User.id' => 'desc'));

            $email='';
            $name='';
        }

        //$options = array('User.id !=' => 2);
        //$this->set('user', $this->User->find('first', $options));
        $this->User->recursive=2;
        $this->Paginator->settings = $options;
        $this->set('users', $this->Paginator->paginate('User'));

        $this->set(compact('title_for_layout','country','email','name','active_user','inactive_user'));
        }

      public function admin_providers($id=null){

        $userid = $this->Session->read('adminuserid');
        $is_admin = $this->Session->read('is_admin');
        if(!isset($is_admin) && $is_admin==''){
           $this->redirect('/admin');
        }
        $title_for_layout = 'Service Provider List';

        $options = array('conditions'=>array('User.service_id'=>$id));
        $this->User->recursive=2;
        $this->Paginator->settings = $options;
        $this->set('users', $this->Paginator->paginate('User'));
      }

        public function admin_edit_service_provider($id = null) {
	    //$this->loadModel('UserImage');
        	$this->loadModel('Post');
           $userid = $this->Session->read('adminuserid');
           $is_admin = $this->Session->read('is_admin');
           if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
           }
           $pst=$this->Post->find('list',array('Post.id','Post.name'));
	   $this->request->data1=array();
           $title_for_layout = 'User Edit';
	   $this->set(compact('title_for_layout','pst'));
		//$service_areas=$this->User->ServiceArea->find('list');
		//$this->set(compact('title_for_layout','service_areas'));

	   

            if (!$this->User->exists($id)) {
                    throw new NotFoundException(__('Invalid user'));
            }
		       if ($this->request->is(array('post', 'put'))) {

                        if(!empty($this->request->data['User']['image']['name'])){
                        $pathpart=pathinfo($this->request->data['User']['image']['name']);
                        $ext=$pathpart['extension'];
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array(strtolower($ext),$extensionValid)){
                        $uploadFolder = "user_images/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename =uniqid().'.'.$ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['User']['image']['tmp_name'],$full_flg_path);
                        }
                        else{
                         $this->Session->setFlash(__('Invalid image type.'));
                        }
                        }
                        else{
                        $filename=$this->request->data['User']['img'];
                        }
                        $this->request->data['User']['image'] = $filename;

			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('The user has been saved.','default', array('class' => 'success'));
				return $this->redirect(array('action' => 'list_service_provider'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {

			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}



        public function admin_setting() {
	    //$this->loadModel('UserImage');
           $userid = $this->Session->read('adminuserid');
           $is_admin = $this->Session->read('is_admin');
           if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
           }





           $options = array('conditions' => array('Setting.' . $this->User->primaryKey =>1));
           $this->request->data = $this->Setting->find('first', $options);

         }






    }
