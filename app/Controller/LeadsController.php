<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class LeadsController extends AppController {

	/*function beforeFilter() {
		parent::beforeFilter();
	}*/
/**
 * Components
 *
 * @var array
 */
	public $name = 'Leads';
	public $components = array('Session','RequestHandler','Paginator');
	var $uses = array('Lead','User','EmailTemplate','Setting');


	public function admin_index() {

    $this->Lead->recursive = 2;
    $options = array('order' => array('Lead.id' =>'ASC'));
    //$activeatt = $this->Booking->find('all', $options);

     $this->Paginator->settings = $options;
     $this->set('leads', $this->Paginator->paginate('Lead'));
     //print_r($activeatt);
    // exit;
	}


  public function admin_view($id = null) {

    $userid = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if(!isset($is_admin) && $is_admin==''){
       $this->redirect('/admin');
    }
  	$this->Booking->recursive = 2;
    $options = array('conditions' => array('Booking.' . $this->Booking->primaryKey => $id));
    $this->set('books', $this->Booking->find('first', $options));
	}

  public function admin_message($id=null){

    $userid = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if(!isset($is_admin) && $is_admin==''){
       $this->redirect('/admin');
    }

    $this->loadModel('Message');
    $this->Message->recursive = 2;
    $options = array('conditions' => array('Message.lead_id' => $id));
    $this->set('messages', $this->Message->find('all', $options));
    
  }

  public function admin_response($id=null){

    $userid = $this->Session->read('adminuserid');
    $is_admin = $this->Session->read('is_admin');
    if(!isset($is_admin) && $is_admin==''){
       $this->redirect('/admin');
    }

    $this->loadModel('Userresponse');
    $this->Userresponse->recursive = 2;
    $options = array('conditions' => array('Userresponse.lead_id' => $id));
    $this->set('responses', $this->Userresponse->find('all', $options));

  }

}
