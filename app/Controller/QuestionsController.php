<?php
App::uses('AppController', 'Controller');
/**
 * CarCategories Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class QuestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $name = 'Questions';

    public $data2='' ;
    public $components = array('Paginator', 'Session');
    var $uses = array('User',  'Post', 'Question','Answer');


/**
 * index method
 *
 * @return void
 */
    
    public function admin_add($service_id) {
           
         if ($this->request->is('post')) {
             

          $this->request->data['Question']['service_id']=$service_id;

                $this->Question->create();

                if ($this->Question->save($this->request->data)) {
                        $this->Session->setFlash('The question has been saved.','default', array('class' => 'success'));
                        $q_id=$this->Question->getLastInsertID();
                        if(isset($this->request->data['answers'])&&!empty($this->request->data['answers']))
                        {
                          foreach($this->request->data['answers'] as $answers)
                          {
                             $this->request->data['Answer']['service_id']=$service_id;
                             $this->request->data['Answer']['q_id']=$q_id;
                             $this->request->data['Answer']['name']=$answers;

                            $this->Answer->create();
                            $this->Answer->save($this->request->data);
                          }
                        }
                        
                        return $this->redirect(array('action' => 'index',$service_id));
                } else {
                        $this->Session->setFlash(__('The service could not be saved. Please, try again.'));
                }
	   }
           
          // $this->set(compact('categories','users'));
	}
        
        public function admin_index($service_id)
        {
            $conditions = array('Question.service_id' => $service_id);
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
           
        $options = array('conditions' => $conditions, 'order' => array('Question.id' => 'ASC'), 'group' => 'Question.id');
        $this->Paginator->settings = $options;
        $title_for_layout = 'Question List';
        //$this->Post->recursive = 1;
        $this->set('posts', $this->Paginator->paginate('Question'));
        $this->set('service_id', $service_id);
        }
        
        public function admin_delete($id = null,$service_id = null) {
		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
		if(!isset($is_admin) && $is_admin==''){
		   $this->redirect('/admin');
		}
		$this->Question->id = $id;
		if (!$this->Question->exists()) {
			throw new NotFoundException(__('Invalid Question'));
		}
		//$this->request->onlyAllow('post', 'delete');
		if ($this->Question->delete()) {
		    //$this->UserImage->delete()
			$this->Session->setFlash('The Question has been deleted.','default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The Question could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index',$service_id));
	}
        
        public function admin_edit($id = null,$service_id = null) {
		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
                if(!isset($is_admin) && $is_admin==''){
                   $this->redirect('/admin');
                }
           
		if ($this->request->is(array('post', 'put'))) {

                        $this->request->data['Question']['id']=$id;

			if ($this->Question->save($this->request->data)) {

                         $this->Session->setFlash('The Question has been saved.','default', array('class' => 'success'));
                         return $this->redirect(array('action' => 'index',$service_id));

			} else {
				$this->Session->setFlash(__('The service could not be saved. Please, try again.'));
			}
		} else {
                    
			$posts = $this->Question->find('first', array('conditions' => array('Question.id' => $id)));
                        
                        $this->request->data=$posts;
                        
                        $answers = $this->Answer->find('list', array('conditions' => array('Answer.q_id' => $id)));
                        //print_r($posts);
                        //exit;
                        
		}
                $this->set(compact('posts','answers'));

        }
        
        public function updateAnswer()
        {
            $this->autoRender = false;
                    $this->layout = false;
                    
                    $userid = $this->Session->read('userid');
                    
                    $ret = array();
                    $html = '';
                     if ($this->request->is('post')) {
                         
                         $this->request->data['Answer']['id'] = $this->request->data['id'];
                            $this->request->data['Answer']['name'] = $this->request->data['ans'];
                           
                            if($this->Answer->save($this->request->data)) 
                            {
                                $ret['ack'] = 1;
                            }
                            else
                            {
                                $ret['ack'] = 0;
                            }
                                               
                     }
                     else
                    {
                        
                        $ret['ack'] = 0;
                    } 
                        
                         echo json_encode($ret);
                        exit;
        }
}