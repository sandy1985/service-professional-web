<?php
App::uses('AppController', 'Controller');
/**
 * CarCategories Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class PostsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $name = 'Posts';

    public $data2='' ;
    public $components = array('Paginator', 'Session');
    var $uses = array('User',  'Post', 'Category');


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->EmailTemplate->recursive = 0;
		$this->set('emailTemplates', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->EmailTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid email template'));
		}
		$options = array('conditions' => array('EmailTemplate.' . $this->EmailTemplate->primaryKey => $id));
		$this->set('emailTemplate', $this->EmailTemplate->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
            $categories = $this->Post->Category->find('list', array('fields' => array('Category.id', 'Category.name')));
            $users = $this->Post->User->find('list', array('fields' => array('User.id', 'User.name'),'conditions' => array('User.type' => 'SP')));
         if ($this->request->is('post')) {
             //print_r($this->request->data['Post']['image']);

            if(!empty($this->request->data['Post']['image']['name'])){
            $pathpart=pathinfo($this->request->data['Post']['image']['name']);
           $ext=$pathpart['extension'];
            $extensionValid = array('jpg','jpeg','png','gif');
            if(in_array(strtolower($ext),$extensionValid)){
            $uploadFolder = "postimg/";
            $uploadPath = WWW_ROOT . $uploadFolder;
            $filename =uniqid().'.'.$ext;
            $full_flg_path = $uploadPath . '/' . $filename;
            move_uploaded_file($this->request->data['Post']['image']['tmp_name'],$full_flg_path);
            }
            else{
             $this->Session->setFlash(__('Invalid image type.'));
            }
           }
           else{
            $filename='';
           }
            $this->request->data['Post']['image'] = $filename;
           
        //for icon img..................................
           if(!empty($this->request->data['Post']['icon']['name'])){
            $pathpart1=pathinfo($this->request->data['Post']['icon']['name']);
            $ext1=$pathpart1['extension'];
            $extensionValid1 = array('jpg','jpeg','png','gif');
            if(in_array(strtolower($ext1),$extensionValid1)){
            $uploadFolder1 = "posticon/";
            $uploadPath1 = WWW_ROOT . $uploadFolder1;
            $filename1 =uniqid().'.'.$ext1;
            $full_flg_path1 = $uploadPath1 . '/' . $filename1;
            move_uploaded_file($this->request->data['Post']['icon']['tmp_name'],$full_flg_path1);
            }
            else{
             $this->Session->setFlash(__('Invalid image type.'));
            }
           }
           else{
            $filename1='';
           }
           
            $this->request->data['Post']['icon'] = $filename1;

                $this->Post->create();

                if ($this->Post->save($this->request->data)) {
                     $service_id=$this->Post->getLastInsertID();
                        $this->Session->setFlash('The service has been saved.','default', array('class' => 'success'));
                        return $this->redirect(array("controller" => "questions",'action' => 'add',$service_id));
                } else {
                        $this->Session->setFlash(__('The service could not be saved. Please, try again.'));
                }
	   }
           
           $this->set(compact('categories','users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->EmailTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid email template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->EmailTemplate->save($this->request->data)) {
				$this->Session->setFlash(__('The email template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email template could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('EmailTemplate.' . $this->EmailTemplate->primaryKey => $id));
			$this->request->data = $this->EmailTemplate->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
		if(!isset($is_admin) && $is_admin==''){
		   $this->redirect('/admin');
		}
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		//$this->request->onlyAllow('post', 'delete');
		if ($this->Post->delete()) {
		    //$this->UserImage->delete()
			$this->Session->setFlash('The post has been deleted.','default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The post could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_index() {
           $this->loadModel('Post');
             $conditions = array();
            $userid = $this->Session->read('adminuserid');
            $is_admin = $this->Session->read('is_admin');
            if(!isset($is_admin) && $is_admin==''){
               $this->redirect('/admin');
            }
           
           $options = array('conditions' => $conditions, 'order' => array('Post.id' => 'desc'), 'group' => 'Post.id');
        $this->Paginator->settings = $options;
        $title_for_layout = 'Post List';
        //$this->Post->recursive = 1;
        $this->set('posts', $this->Paginator->paginate('Post'));
        //$posts = $this->Post->find('all', array('fields' => array('Post.id', 'Post.title')));
        //print_r($posts);
                //echo 1;die;
		$this->set(compact('title_for_layout'));
	}

	    public function admin_edit($id = null) {
		$userid = $this->Session->read('adminuserid');
		$is_admin = $this->Session->read('is_admin');
                if(!isset($is_admin) && $is_admin==''){
                   $this->redirect('/admin');
                }
            $categories = $this->Post->Category->find('list', array('fields' => array('Category.id', 'Category.name')));
            $users = $this->Post->User->find('list', array('fields' => array('User.id', 'User.name'),'conditions' => array('User.type' => 'SP')));
		if ($this->request->is(array('post', 'put'))) {

                        $this->request->data['Post']['id']=$id;



                        if(!empty($this->request->data['Post']['image']['name'])){
                        $pathpart=pathinfo($this->request->data['Post']['image']['name']);
                        $ext=$pathpart['extension'];
                        $extensionValid = array('jpg','jpeg','png','gif');
                        if(in_array(strtolower($ext),$extensionValid)){
                        $uploadFolder = "postimg/";
                        $uploadPath = WWW_ROOT . $uploadFolder;
                        $filename =uniqid().'.'.$ext;
                        $full_flg_path = $uploadPath . '/' . $filename;
                        move_uploaded_file($this->request->data['Post']['image']['tmp_name'],$full_flg_path);
                        }
                        else{
                         $this->Session->setFlash(__('Invalid image type.'));
                        }
                       }
                       else{
                        $filename=$this->request->data['Post']['old_image'];
                       }
                        $this->request->data['Post']['image'] = $filename;
                        
                         //for icon img..................................
                        if(!empty($this->request->data['Post']['icon']['name'])){
                         $pathpart1=pathinfo($this->request->data['Post']['icon']['name']);
                         $ext1=$pathpart1['extension'];
                         $extensionValid1 = array('jpg','jpeg','png','gif');
                         if(in_array(strtolower($ext1),$extensionValid1)){
                         $uploadFolder1 = "posticon/";
                         $uploadPath1 = WWW_ROOT . $uploadFolder1;
                         $filename1 =uniqid().'.'.$ext1;
                         $full_flg_path1 = $uploadPath1 . '/' . $filename1;
                         move_uploaded_file($this->request->data['Post']['icon']['tmp_name'],$full_flg_path1);
                         }
                         else{
                          $this->Session->setFlash(__('Invalid image type.'));
                         }
                        }
                        else{
                         $filename1=$this->request->data['Post']['old_icon'];
                        }

                         $this->request->data['Post']['icon'] = $filename1;

			if ($this->Post->save($this->request->data)) {

                         $this->Session->setFlash('The service has been saved.','default', array('class' => 'success'));
                         return $this->redirect(array('action' => 'index'));

			} else {
				$this->Session->setFlash(__('The service could not be saved. Please, try again.'));
			}
		} else {
			$posts = $this->Post->find('first', array('conditions' => array('Post.id' => $id)));
                        $this->request->data=$posts;
                        //print_r($posts);
                        //exit;
                        
		}
                $this->set(compact('posts','users','categories'));

        }

}
