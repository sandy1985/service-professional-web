<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */

class AppController extends Controller {

	public $components = array('Session');
	var $uses = array('SocialIcon','Footer','BookingRequest');

	public function beforeFilter() {
		$adminRoute = Configure::read('Routing.prefixes');
		#pr($adminRoute);
		if (isset($this->params['prefix']) && in_array($this->params['prefix'], $adminRoute)) {
			$this->layout = 'admin_default';
		} else {
			$this->layout = 'default';
		}
	}


        public function php_mail($to, $from, $subject, $message) {
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            //$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            //$headers .= 'To: '.$to_name.' <'.$to.'>' . "\r\n";
            $headers .= 'From: '.$from . "\r\n";
            mail($to, $subject, $message, $headers);
        }

	function create_slug($string, $ext=''){
		$replace = '-';
		$string = strtolower($string);

		//replace / and . with white space
		$string = preg_replace("/[\/\.]/", " ", $string);
		$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);

		//remove multiple dashes or whitespaces
		$string = preg_replace("/[\s-]+/", " ", $string);

		//convert whitespaces and underscore to $replace
		$string = preg_replace("/[\s_]/", $replace, $string);

		//limit the slug size
		$string = substr($string, 0, 200);

		//slug is generated
		return ($ext) ? $string.$ext : $string;
	}

		/*function _setErrorLayout() {
			if ($this->name == 'CakeError') {
			$this->layout = 'error404';
			}
		}*/


        public function beforeRender() {

		 $this->loadModel('User');
                 $this->loadModel('Setting');
		 $this->loadModel('Blockip');
		 $conditions=array('Conditions'=>array('`Blockip`.`ip_address`'=>$_SERVER['REMOTE_ADDR']));
		 $blockipaddress=$this->Blockip->find('first',$conditions);
		 $this->set('blockipaddress',$blockipaddress);
		 $adminuserid = $this->Session->read('adminuserid');
                 
                 $options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => 1));
        $sitesetting = $this->Setting->find('first', $options);
        $this->set(compact('sitesetting'));
        
        //set sitelogo for the site
        $setting_array = array('conditions' => array('Setting.id' => '1'));
        $Content = $this->Setting->find('first', $setting_array);
		//pr($Content);
        if ($Content['Setting']['logo'] != '') {
            $logo_name = $Content['Setting']['logo'];
        } else {
            $logo_name = 'logo.png';
        }
		  if ($Content['Setting']['fav_icon'] != '') {
            $fav_icon = $Content['Setting']['fav_icon'];
        } else {
            $fav_icon = '104573631_favicon.ico';
        }
        
        $this->set('logo', $this->webroot . 'site_logo/'. $logo_name);
        $this->set('fav_icon', $this->webroot . 'fav_icon/'. $fav_icon);

		 if(isset($userid) && $userid!=''){
		//echo $userid;
                            $options = array('conditions' => array('User.' . $this->User->primaryKey => $userid));
                            $userdetails=$this->User->find('first', $options);
                            $this->set(compact('userdetails'));

				}
			else if(isset($adminuserid) && $adminuserid!='')
			{
			 $options = array('conditions' => array('User.' . $this->User->primaryKey => $adminuserid));
		          $userdetails=$this->User->find('first', $options);
		          $this->set(compact('userdetails'));
			}

			if(isset($adminuserid) && $adminuserid!='')
			{
			 $sidebar = array('conditions' => array('User.id' => $adminuserid));
             $sidebars = $this->User->find('first', $sidebar);
		     $this->set(compact('sidebars'));

			}
			  if (isset($adminuserid) && $adminuserid != '') {
            $roleAccess = $this->getRolesAccess($userdetails['User']['role']);
            if(!empty($roleAccess)) {
                $this->set('roleAccess', unserialize($roleAccess['RolesAccess']['accessibility']));
            }
        }

		}
		    public function getRolesAccess( $role = NULL ) {
        $this->loadModel('RolesAccess');
        return $this->RolesAccess->find( 'first', array(
            'conditions' => array(
                'RolesAccess.role_id' => $role
            )
        ));
    }


}
