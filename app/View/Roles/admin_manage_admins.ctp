<div class="users form">
    <?php echo $this->Form->create('User',array('enctype'=>'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Add Sub Admin'); ?></legend>
        <?php
        echo $this->Form->input('role', array('label' => 'Group','required'=>'required'));
	    echo $this->Form->input('name',array('required' => 'required','style'=>'width:500px;'));
		echo $this->Form->input('email', array('type' => 'email','required' => 'required','style'=>'width:500px;'));
		echo $this->Form->input('password', array('type' => 'password','required' => 'required','style'=>'width:500px;'));
                echo $this->Form->input('mobile_no', array('required' => 'required','style'=>'width:500px;'));
                echo $this->Form->input('image', array('type' => 'file','required' => 'required','style'=>'width:500px;'));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script> -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
<script>
    var autocomplete = new google.maps.places.Autocomplete($("#address")[0], {});

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        console.log(place.address_components);
    });

    $(function(){
        $('#UserCountry').change(function(){
            var self = $(this);
            $.ajax({
                url: '<?php echo $this->webroot . 'states/ajaxStates' ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    c_id: self.val()
                },
                success: function(data) {
                    $('#UserState').html(data.html);
                }
            });
        });

        $('#UserState').change(function(){
            var self = $(this);
            $.ajax({
                url: '<?php echo $this->webroot . 'cities/ajaxCities' ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    c_id: $('#UserCountry').val(),
                    s_id: self.val()
                },
                success: function(data) {
                    $('#UserCity').html(data.html);
                }
            });
        });
    });
</script>