<div class="rolesAccesses index group_member">
    <h2><?php echo __('Group Members'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                <th><?php echo h('Email'); ?></th>
                <th><?php echo h('Status'); ?></th>
                <th><?php echo h('Groups'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($groupMembers as $groupMember): 
		
		?>
                <tr style="height: 40px;">
                    <td><?php echo h($groupMember['User']['id']); ?>&nbsp;</td>
                    <td>
                        <?php echo $groupMember['User']['email']; ?>
                    </td>
                    <td>
                        <?php if($groupMember['User']['pending']==1){ echo 'Active'; }else{echo 'Pending';} ?>
                    </td>
                    <td>
                        <?php echo $groupMember['Role']['name']; ?>
                    </td>
                    
                    <td class="actions">
                        <?php //echo $this->Form->postLink(__('Edit'), array('controller'=>'normal_users','action' => 'admin_edit', $groupMember['User']['id'])); ?>
                        <?php
                        echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), array('controller'=>'users','action' => 'edit_customer', $groupMember['User']['id']), array('class' => 'btn btn-info btn-xs edit_button', 'escape' => false));
                        ?>
                        <?php
                        //echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-times')), array('action' => 'delete', $groupMember['User']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $groupMember['User']['id']));
                        ?>
                        <?php echo $this->Form->postLink(__('Delete'), array( 'action' => 'delete', $groupMember['User']['id']),array(
                                     'class' => 'btn_delete'
                                 ), array('confirm' => __('Are you sure you want to delete # %s?', $groupMember['User']['id']))); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
            ));
        ?>
    </p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>