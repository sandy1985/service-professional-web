<div class="categories index">
	<h2><?php echo __('Email Template'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('subject'); ?></th>
			<th><?php echo $this->Paginator->sort('content'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($emailtemplate as $content): ?>
	<tr>
		<td><?php echo h($content['EmailTemplate']['id']); ?>&nbsp;</td>
		<td><?php echo h($content['EmailTemplate']['subject']);?></td>
		<td><?php echo ($content['EmailTemplate']['content']);?></td>
		<td>

	          <?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $content['EmailTemplate']['id'])); ?>
                  <a href="<?php echo $this->webroot?>admin/email_templates/edit/<?php echo $content['EmailTemplate']['id']?>" class="btn btn-success">Edit</a>&nbsp;&nbsp;
<!--                  <a href="<?php echo $this->webroot?>admin/email_templates/delete/<?php echo $content['EmailTemplate']['id']?>" class="btn btn-danger" onclick="return confirm('are you sure to delete this template?');">Delete</a>&nbsp;&nbsp;-->
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>