<div class="contents form">
<?php echo $this->Form->create('ServiceArea'); ?>
	<fieldset>
		<legend><?php echo __('Edit Service'); ?></legend>
	<?php
	 echo $this->Form->input('parent_id', array('type' => 'hidden', 'value' => $parentid));
	 //pr($parent_id);
     //exit;
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('address');
		echo $this->Form->input('city');
		echo $this->Form->input('state');
		echo $this->Form->input('zip');
		echo $this->Form->input('traffic_time');	
		echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('admin_sidebar'); ?>