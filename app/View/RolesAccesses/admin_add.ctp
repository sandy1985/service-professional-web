<div class="rolesAccesses form">
    <?php echo $this->Form->create('RolesAccess'); ?>
    <fieldset>
        <legend><?php echo __('Add Roles Access'); ?></legend>
        <?php
        echo $this->Form->input('role_id', array('class'=>'selectbox2'));
        //echo $this->Form->input('accessibility');
        echo $this->Form->input('accessibility', array(
            'type' => 'select',
            'multiple' => 'checkbox',
            'options' => array(
                'cms' => 'CMS_Pages',
                'faq' => 'FAQ',
                'banner' => 'Banner',
                'home_slider' => 'Home_Slider',
                'email_template' => 'Email_Template',
                'normal_users' => 'Users',
                'user_location' => 'Add User Location',
                'categories' => 'Categories',
                'course' => 'Course',
                'contact_us' => 'Contact US',
                'seo_keyword'=> 'SEO Keyword',
                'language_management' => 'Language Management',
                'language_resourse' => 'Language Resourse',
                'sitemap' => 'Sitemaps',
                'analytics' => 'Analytics',
                'seo_url' => 'SEO URL',
                'newsletter' => 'Newsletter',
                'sub_admins' => 'Sub Admin'
                // 'setting' => 'Setting',
                // 'home_slider' => 'Home Slider',
                // 'list_slider' => 'List Slider',
                // 'add_slider' => 'Add Slider',
                // 'banner' => 'Banner',
                // 'list_banner' => 'List Banner',
                // 'add_banner' => 'Add Banner',
                // 'partners' => 'Partners',
                // 'list_partners' => 'List Partners',
                // 'add_partners' => 'Add Partners',
                // 'cms' => 'CMS',
                // 'list_cms' => 'List CMS',
                // 'add_cms' => 'Add CMS',
                // 'faq' => 'FAQ',
                // 'list_faq' => 'List FAQ',
                // 'add_faq' => 'Add FAQ',
                // 'categories' => 'Categories',
                // 'list_categories' => 'List Categories',
                // 'add_categories' => 'Add Categories',
                // 'properties' => 'Properties',
                // 'list_properties' => 'List Properties',
                // 'add_properties' => 'Add Properties',
                // 'email_template' => 'Email Template',
                // 'newsletters' => 'Newsletters',
                // 'users' => 'Users',
                // 'list_users' => 'List Users',
                // 'add_users' => 'Add Users',
                // 'contact' => 'Contact',
                // 'list_contact' => 'List Contact',
                // 'social' => 'Social',
                // 'list_social' => 'List Social',
                // 'menu' => 'Menu',
                // 'list_menu' => 'List Menu',
                // 'add_menu' => 'Add Menu',
                // 'header_menu' => 'Header Menu',
                // 'footer_menu' => 'Footer Menu',
                // 'testimonial' => 'Testimonial',
                // 'list_testimonial' => 'List Testimonial',
                // 'add_testimonial' => 'Add Testimonial',
                // 'news' => 'News',
                // 'list_news' => 'List News',
                // 'add_news' => 'Add News',
                // 'news_comments' => 'News Comments',
                // 'membership' => 'Membership',
                // 'plans' => 'Plans',
                // 'users_plan' => 'Users Plan'
            )
        ));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>

<style>
	select {padding:5px;}
</style>