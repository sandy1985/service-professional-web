<div class="rolesAccesses view">
    <h2><?php echo __('Roles Access'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($rolesAccess['RolesAccess']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Role'); ?></dt>
        <dd>
            <?php echo $this->Html->link($rolesAccess['Role']['name'], array('controller' => 'roles', 'action' => 'view', $rolesAccess['Role']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Accessibility'); ?></dt>
        <dd>
            <?php echo h($rolesAccess['RolesAccess']['accessibility']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
