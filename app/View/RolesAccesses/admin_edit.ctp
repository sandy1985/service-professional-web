<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
<style>
.dropdown-menu .open{
        max-height: 50px;
}
</style>
<?php
$accessibility = unserialize($this->request->data['RolesAccess']['accessibility']);
?>
<div class="rolesAccesses form">
    <?php echo $this->Form->create('RolesAccess'); ?>
    <fieldset>
        <legend><?php echo __('Edit Roles Access'); ?></legend>
        <h3>Edit Group Members of <?php echo $this->request->data['Role']['name']; ?> Group</h3>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('role_id',array('hidden'=>'hidden','label'=>false)); 
	if(!empty($users))
	{
	    
	?>
        <div class="input select">
            <label for="PostUserId">User</label>
            <select name="data[User][user_id][]" required="required" id="UserId" class="selectpicker" multiple>
                <?php foreach ($users as $key => $user) {?>
                    <option value="<?php echo $user['User']['id'];?>" <?php if(in_array($user['User']['id'], $members_ids)){ echo 'selected'; }?> ><?php echo $user['User']['name'];?></option>
                <?php } ?>
            </select>
        </div>
        <?php 
	}
        echo $this->Form->input('accessibility', array(
            'type' => 'select',
            'class'=>'bid_pad',
            'multiple' => 'checkbox',
            'selected' => $accessibility,
              'options' => array(
                    'dashboard' => 'Dashboard',
                    'managelogo' => 'Manage Logo',
                    'managesettings' => 'Manage Settings',
                    'managebanner' => 'Manage Banners',
		  'manageblockip'=>'Manage Block Ip Address',
                    'manageaccess' => 'Member Access',
                    'users' => 'Users',
                    'booking' => 'Booking',
                    'categories' => 'Service Category',
                    'post' => 'Service Post',
                    'leads' => 'Leads',
                    'reportabuse'=> 'Report Abuse',
		    'faq'=> 'FAQ',
                    'emailtemplate' => 'Email Template',
                    'contents' => 'Contents',
                    'plan' => 'Membership Plans'
                // 'setting' => 'Setting',
                // 'home_slider' => 'Home Slider',
                // 'list_slider' => 'List Slider',
                // 'add_slider' => 'Add Slider',
                // 'banner' => 'Banner',
                // 'list_banner' => 'List Banner',
                // 'add_banner' => 'Add Banner',
                // 'partners' => 'Partners',
                // 'list_partners' => 'List Partners',
                // 'add_partners' => 'Add Partners',
                // 'cms' => 'CMS',
                // 'list_cms' => 'List CMS',
                // 'add_cms' => 'Add CMS',
                // 'faq' => 'FAQ',
                // 'list_faq' => 'List FAQ',
                // 'add_faq' => 'Add FAQ',
                // 'categories' => 'Categories',
                // 'list_categories' => 'List Categories',
                // 'add_categories' => 'Add Categories',
                // 'properties' => 'Properties',
                // 'list_properties' => 'List Properties',
                // 'add_properties' => 'Add Properties',
                // 'email_template' => 'Email Template',
                // 'newsletters' => 'Newsletters',
                // 'users' => 'Users',
                // 'list_users' => 'List Users',
                // 'add_users' => 'Add Users',
                // 'contact' => 'Contact',
                // 'list_contact' => 'List Contact',
                // 'social' => 'Social',
                // 'list_social' => 'List Social',
                // 'menu' => 'Menu',
                // 'list_menu' => 'List Menu',
                // 'add_menu' => 'Add Menu',
                // 'header_menu' => 'Header Menu',
                // 'footer_menu' => 'Footer Menu',
                // 'testimonial' => 'Testimonial',
                // 'list_testimonial' => 'List Testimonial',
                // 'add_testimonial' => 'Add Testimonial',
                // 'news' => 'News',
                // 'list_news' => 'List News',
                // 'add_news' => 'Add News',
                // 'news_comments' => 'News Comments',
                // 'membership' => 'Membership',
                // 'plans' => 'Plans',
                // 'users_plan' => 'Users Plan'
                    )
        ));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>


<style>
	select {padding:5px;}
</style>

<script type="text/javascript" src="<?php echo $this->webroot; ?>js/bootstrap-select.js"></script>