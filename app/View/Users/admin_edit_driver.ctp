<div class="users form">
<?php echo $this->Form->create('User',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Edit Customer'); ?></legend>
	<?php
		echo $this->Form->input('hidpw', array('type' => 'hidden', 'value' => $this->request->data['User']['password']));
		echo $this->Form->input('id');
		echo $this->Form->input('name',array('style'=>'width:500px;','required'=>'required'));
		//echo $this->Form->input('password',array('value'=>''));
		echo $this->Form->input('email',array('readonly'=>'readonly','style'=>'width:500px;'));
                echo $this->Form->input('mobile_no',array('style'=>'width:500px;','required'=>'required'));


		echo $this->Form->input('img', array('type' => 'hidden','default' =>$this->request->data['User']['image']));
	        echo $this->Form->input('image',array('type'=>'file'));
	?>
		       <div>
                    <?php
                        if(isset( $this->request->data['User']['image']) and !empty( $this->request->data['User']['image']))
                    {
                    ?>
                    <img alt="" src="<?php echo $this->webroot;?>user_images/<?php echo $this->request->data['User']['image'];?>" style=" height:80px; width:80px;">
                    <?php
                    }
                    else{
                    ?>
                   <img alt="" src="<?php echo $this->webroot;?>user_images/default.png" style=" height:80px; width:80px;">

                    <?php } ?>
                </div>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
