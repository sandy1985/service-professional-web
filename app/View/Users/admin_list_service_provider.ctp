<?php //echo '<pre>'; print_r($users); echo '</pre>'; ?>
<div class="users index">
	<h2 style="width:400px;float:left;"><?php echo __('Service Provider'); ?></h2>
        <div>
       <?php //echo $this->Form->create("User");?>
            <form name="Searchuserfrm" method="post" action="" id="Searchuserfrm">
        <table style=" border:none;">
            <tr>

		<td>Email</td>
                <td><input type="email" name="email" value="<?php echo isset($email)?$email:'';?>" placeholder="Email"></td>
                <td>Name</td>
                <td><input type="text" name="name" value="<?php echo isset($name)?$name:'';?>" placeholder="Name"></td>
		<td><input type="submit" name="search" value="Search"></td>
            </tr>
        </table>
            </form>
        <?php //echo $this->Form->end();?>
        </div>

	<table cellpadding="0" cellspacing="0">
	<tr>
            <th>SN<?php //echo $this->Paginator->sort('id'); ?></th>
	    <th><?php echo $this->Paginator->sort('name'); ?></th>
            <th><?php echo $this->Paginator->sort('Image'); ?></th>
      <th><?php echo $this->Paginator->sort('email','Email'); ?></th>
	    <th><?php echo $this->Paginator->sort('Service Name'); ?></th>
            <th><?php echo $this->Paginator->sort('mobile_no','Mobile'); ?></th>
            <th><?php echo $this->Paginator->sort('date','Date'); ?></th>

            <th><?php echo $this->Paginator->sort('status','Status'); ?></th>
	    <th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
        $UserCnt=0;

        foreach ($users as $user):
	    //pr($user);
            $UserCnt++;
            $uploadImgPath = WWW_ROOT.'user_images';
        ?>
	<tr>
          <td><?php echo $UserCnt;//echo h($user['User']['id']); ?>&nbsp;</td>
	  <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
          <td><?php
            $per_profile_img=isset($user['User']['image'])?$user['User']['image']:'';
            if($per_profile_img!='' && file_exists($uploadImgPath . '/' . $per_profile_img)){
                $ImgLink=$this->webroot.'user_images/'.$per_profile_img;
            }else{
                $ImgLink=$this->webroot.'user_images/default.png';
            }
            echo '<img src="'.$ImgLink.'" alt="" height="42px" width="42px"/>';
            ?>&nbsp;
          </td>
          <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
          <td><?php echo h($user['Post']['title']); ?>&nbsp;</td>
          <td><?php echo h($user['User']['mobile_no']); ?>&nbsp;</td>
	  <td><?php echo date('d M Y', strtotime($user['User']['registration_date'])); ?>&nbsp;</td>
          <td>
          <?php
          if($user['User']['status']==0){
          ?>

           <a href="<?php echo $this->webroot?>admin/users/unblock/<?php echo $user['User']['id']?>" class="btn btn-primary" onclick="return confirm('are you sure to unblock this user?');">Unblock</a>

<!--           <p style="font-size:14px;  color:#009900; width:50%; float:right;"><strong><?php echo "Blocked";?></strong><p>-->

          <?php
          }else{
          ?>



          <a href="<?php echo $this->webroot?>admin/users/block/<?php echo $user['User']['id']?>" onclick="return confirm('are you sure to block this user?');" class="btn btn-danger">Block</a>&nbsp;&nbsp;
          <!--<p style="font-size:14px; margin-top:-15px; color:#009900; width:50%; float:right;"><strong><?php echo "Active";?></strong><p>!-->
          <?php
          }
         ?>
        </td>
	<td>

           <!--<a href="<?php //echo $this->webroot?>admin/users/reset_pass/<?php echo $user['User']['id']?>" class="btn btn-primary">Reset Password</a>&nbsp;&nbsp;!-->

           <a href="<?php echo $this->webroot?>admin/users/edit_service_provider/<?php echo $user['User']['id']?>" class="btn btn-success">Edit</a>&nbsp;&nbsp;

           <a href="<?php echo $this->webroot?>admin/users/delete_service_provider/<?php echo $user['User']['id']?>" class="btn btn-danger" onclick="return confirm('are you sure to delete this user?');">Delete</a>&nbsp;&nbsp;
	</td>
	</tr>
       <?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>


<script type="text/javascript">
    $(document).ready(function(){
        $('.UserStatus').click(function(){
            var UserStatus=$(this).attr('id');
            $('#search_is_active').val(UserStatus);
            $('#Searchuserfrm').submit();
            //document.Searchuserfrm.submit();
            //$('#UserAdminListForm')[0].submit();
        });
    });
</script>
<style>
.title a{
    color: #fff !important;
}
</style>
