<div class="users form">
<?php echo $this->Form->create('User',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Add Subadmin'); ?></legend>
	<?php
	//pr($service_areas);
		echo $this->Form->input('name',array('style'=>'width:500px;','required'=>'required'));
		echo $this->Form->input('email', array('type' => 'email','style'=>'width:500px;','required'=>'required'));
		echo $this->Form->input('password', array('type' => 'password','style'=>'width:500px;','required'=>'required'));
        echo $this->Form->input('mobile_no',array('style'=>'width:500px;','required'=>'required'));
		echo $this->Form->input('left_sidebar', array('multiple'=>'multiple', 'lable'=>'Leftside Action', 'options'=>$leftactions,'style'=>'width:500px;','required'=>'required'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php //echo $this->element('admin_sidebar'); ?>
