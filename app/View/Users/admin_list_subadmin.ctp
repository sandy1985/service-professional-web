

<div class="users index">
	<h2 style="width:400px;float:left;"><?php echo __('Sub admins'); ?></h2>


	<table cellpadding="0" cellspacing="0">
	<tr>
		<th>SN<?php //echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('name'); ?></th>

		<th><?php echo $this->Paginator->sort('email','Email'); ?></th>
        <th><?php echo $this->Paginator->sort('mobile_no','Mobile No'); ?></th>
         <th><?php echo $this->Paginator->sort('date','Date'); ?></th>
        <th><?php echo $this->Paginator->sort('status','Status'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
        $UserCnt=0;

        foreach ($subadmins as $user):
	    //pr($user);
            $UserCnt++;?>
	<tr>
		<td><?php echo $UserCnt;//echo h($user['User']['id']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                <td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['mobile_no']); ?>&nbsp;</td>
        <td><?php echo h($user['User']['registration_date']); ?>&nbsp;</td>
        <td>
        <?php
          if($user['User']['status']==0){
          ?>

          <?php //echo $this->Html->link(__('Unblock'), array('action' => 'unblock_subadmin', $user['User']['id']), null, __('Are you sure you want to unblock %s?', $user['User']['name'])); ?>

           <a href="<?php echo $this->webroot?>admin/users/unblock_subadmin/<?php echo $user['User']['id']?>" onclick="return confirm('are you sure to unblock this subadmin?')" class="btn btn-primary">Unblock</a>&nbsp;&nbsp;


          <?php
          }else{
          ?>

          <?php //echo $this->Html->link(__('Block'), array('action' => 'block_subadmin', $user['User']['id']), null, __('Are you sure you want to block %s?', $user['User']['name'])); ?>

           <a href="<?php echo $this->webroot?>admin/users/block_subadmin/<?php echo $user['User']['id']?>" onclick="return confirm('are you sure to block this subadmin?');" class="btn btn-danger">Block</a>&nbsp;&nbsp;

          <?php
          }
         ?>
        </td>
	    <td>

            <a href="<?php echo $this->webroot?>admin/users/edit_subadmin/<?php echo $user['User']['id']?>" class="btn btn-success">Edit</a>&nbsp;&nbsp;


            <a href="<?php echo $this->webroot?>admin/users/delete_subadmin/<?php echo $user['User']['id']?>" onclick="return confirm('are you sure to delete this subadmin?');" class="btn btn-danger">Delete</a>&nbsp;&nbsp;

	    </td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>


<script type="text/javascript">
    $(document).ready(function(){
        $('.UserStatus').click(function(){
            var UserStatus=$(this).attr('id');
            $('#search_is_active').val(UserStatus);
            $('#Searchuserfrm').submit();
            //document.Searchuserfrm.submit();
            //$('#UserAdminListForm')[0].submit();
        });
    });
</script>
<style>
.title a{
    color: #fff !important;
}
</style>