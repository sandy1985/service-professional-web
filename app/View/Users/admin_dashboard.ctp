<!--body wrapper start-->
<style>
    #content {
        overflow: inherit;
    }
        #content {
        overflow: inherit;
    }
.star-ratings-css {unicode-bidi: bidi-override;color: #fff; font-size: 25px;height: 25px;width: 100px; position: relative;padding: 0;text-shadow: 0px 1px 0 #a2a2a2;}
.star-ratings-css-top {color: #fc8675;padding: 0;position: absolute;z-index: 1;display: block;top: 0;left: 0;overflow: hidden;}
.star-ratings-css-bottom {padding: 0;display: block;z-index: 0;}
.star-ratings-sprite { background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/2605/star-rating-sprite.png") repeat-x;
  font-size: 0;height: 21px;line-height: 0;overflow: hidden;width: 110px;}
.star-ratings-sprite-rating {background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/2605/star-rating-sprite.png") repeat-x;background-position: 0 100%;float: left;height: 21px;display: block; }
#chart { width: 100%; height: 350px; margin: 30px auto 0; display: block;}
#chart #numbers { height: 100%; width: 50px; margin: 0; padding: 0;display: inline-block; float: left;}
#chart #numbers li { text-align: right; padding-right: 1em; list-style: none; height: 29px; border-bottom: 1px solid #444;
  position: relative; bottom: 30px;}
#chart #numbers li:last-child { height: 30px; }
#chart #numbers li span { color: #eee; position: absolute; bottom: 0; right: 10px;}
#chart #bars { display: inline-block; background: rgba(0, 0, 0, 0.2); width: calc(100% - 160px); height: 300px; padding: 0;
  margin: 0; box-shadow: 0 0 0 1px #444;}
#chart #bars li { display: table-cell; width: 130px; height: 300px; margin: 0; text-align: center; position: relative;}
#chart #bars li .bar { display: block; width: 110px; margin-left: 15px; background: #49E; position: absolute; bottom: 0;}
#chart #bars li .bar:hover { background: #5AE; cursor: pointer; transform: scale(1.1) }
#chart #bars li .bar:hover:before { color: white; content: data-name; position: relative;  bottom: 20px;}
#chart #bars li span { color: #eee; width: 100%; position: absolute; bottom: -44px; left: 0; text-align: center;}
.deep-purple-box
{
    background: #49586e none repeat scroll 0 0;
    box-shadow: 0 5px 0 #424f63;
    color: #fff;
}
</style>
        <div class="wrapper">
            <div class="row">
                <div class="col-md-12">
                    <!--statistics start-->
			<div class="row state-overview deep-purple-box">
		
			    <div class="col-md-3 col-sm-3 ">
				<div id="graph-donut_one" class="revenue-graph"></div>
				<center>User<br><h4>New Users Vs Total Users</h4></center>
			    </div>
			    <div class="col-md-3 col-sm-3">
				<div id="graph-donut_two" class="revenue-graph"></div>
				 <center>Provider<br><h4>New Providers Vs Total Providers</h4></center>
			    </div>
			    <div class="col-md-3 col-sm-3 ">
				<div id="graph-donut_three" class="revenue-graph"></div>
				 <center>Category<br><h4>New Service Category Vs Total Service Category</h4></center>
				</div>

				<div class="col-md-3 col-sm-3">
				<div id="graph-donut_four" class="revenue-graph"></div>
				 <center>Post<br><h4>New Service Post Vs Total Service Post</h4></center>
				</div>
                        </div>
				<div class="row state-overview" style="margin-top:20px;">
                        <div class="col-md-6 col-xs-12 col-sm-6" onclick="location.href='http://104.131.83.218/team6/service_pro/admin/users/list'" style="cursor: pointer">
                            <div class="panel purple">
                                <div class="symbol">
                                    <i class="fa fa-gavel"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php if(count($users)>0){ echo count($users); } ?></div>
                                    <div class="title">User</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6" onclick="location.href='http://104.131.83.218/team6/service_pro/admin/users/list_service_provider'" style="cursor: pointer">
                            <div class="panel red">
                                <div class="symbol">
                                    <i class="fa fa-tags"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php if(count($spusers)>0){ echo count($spusers); } ?></div>
                                    <div class="title">Service Provider</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--statistics end-->
                </div>
                <div class="col-md-6">
                  <div class="row state-overview">
                        <div class="col-md-6 col-xs-12 col-sm-6" onclick="location.href='http://104.131.83.218/team6/service_pro/admin/categories/index'" style="cursor: pointer">
                            <div class="panel blue">
                                <div class="symbol">
                                    <i class="fa fa-money"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php if(count($cat)>0){ echo count($cat); } ?></div>
                                    <div class="title">Service Category</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6" onclick="location.href='http://104.131.83.218/team6/service_pro/admin/posts/index'" style="cursor: pointer">
                            <div class="panel green">
                                <div class="symbol">
                                    <i class="fa fa-eye"></i>
                                </div>
                                <div class="state-value">
                                    <div class="value"><?php if(count($post)>0){ echo count($post); } ?></div>
                                    <div class="title">Service Post</div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
            <div class="row">

                <div class="col-md-8">
                    <div class="panel">
                        <header class="panel-heading">
                            Booking Details
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                             </span>
                        </header>
                        <div class="panel-body">
                            <ul class="goal-progress">
                                <li>

                                    <div class="details">
                                        <div class="title">







                                        </div>

                                    </div>

                                </li>

                            </ul>
<!--                            <div class="text-center"><a href="#">View all Review</a></div>-->
                        </div>
                    </div>
                </div>
            </div>




        </div>
<script>

$(function(){


   var draftParsv =-1;
    var approvedPars =70;
    
    var inreviewPars =30;
  var donut = Morris.Donut({
    element: 'graph-donut_one',
    data: [

        {value: draftParsv, label: 'Total User', formatted: '22' },
        {value: approvedPars, label: 'New User', formatted: '10' },
        {value: inreviewPars, label: 'Total User', formatted: '22' }
        
        
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (y,data) {

     return data.formatted; 
 }

    }); 
   
 donut.select(0);



    var data = [];

    

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };
 
});
  $("#graph-donut_one").mouseout(function(){
   var draftParsv =-1;
    var approvedPars =70 ;
    
    var inreviewPars =30;
  var donut = Morris.Donut({
    element: 'graph-donut_one',
    data: [

        {value: draftParsv, label: 'Total User', formatted: '22' },
        {value: approvedPars, label: 'New User', formatted: '10' },
        {value: inreviewPars, label: 'Total User', formatted: '22' }
        
        
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (y,data) {

     return data.formatted; 
 }

    }); 
   
 donut.select(0);



    var data = [];

    

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };
    });
</script>
<script type="text/javascript">
// $( "#graph-donut_one" ).focusout(function() {
//     alert("hii");
//     });
// </script>
<script>
$(function(){

    var draftParsv =-1;
    var approvedPars =70;
    
    var inreviewPars =30;
  var donut = Morris.Donut({
    element: 'graph-donut_two',
    data: [

       {value: draftParsv, label: 'Total Provider', formatted: '9' },
        {value: approvedPars, label: 'New Provider', formatted: '3' },
        {value: inreviewPars, label: 'Total Provider', formatted: '9' }
        
        
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (y,data) {

     return data.formatted; 
 }

    }); 
    
 donut.select(0);


    var data = [];

    

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };
 
});
$("#graph-donut_two").mouseout(function(){
    var draftParsv =-1;
    var approvedPars =70;
    
    var inreviewPars =30;
  var donut = Morris.Donut({
    element: 'graph-donut_two',
    data: [

        {value: draftParsv, label: 'Total Provider', formatted: '9' },
        {value: approvedPars, label: 'New Provider', formatted: '3' },
        {value: inreviewPars, label: 'Total Provider', formatted: '9' }
        
        
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (y,data) {

     return data.formatted; 
 }

    }); 
    
 donut.select(0);


    var data = [];

    

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };
    });
</script>
<script>
$(function(){

    var draftParsv =-1;
    var approvedPars =75;
    
    var inreviewPars =25;
  var donut = Morris.Donut({
    element:'graph-donut_three',
    data: [

        {value: draftParsv, label:'Total Service Category', formatted: '27' },
        {value: approvedPars, label:'New Service Category', formatted: '14' },
        {value: inreviewPars, label:'Total Service Category', formatted: '27' }
        
        
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (y,data) {

     return data.formatted; 
 }

    }); 
    
 donut.select(0);


    var data = [];

    

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };
 
});
$("#graph-donut_three").mouseout(function(){
   var draftParsv =-1;
    var approvedPars =75;
    
    var inreviewPars =25;
  var donut = Morris.Donut({
    element:'graph-donut_three',
    data: [

        {value: draftParsv, label:'Total Service Category', formatted: '27' },
        {value: approvedPars, label: 'New Service Category', formatted: '14' },
        {value: inreviewPars, label: 'Total Service Category', formatted: '27' }
        
        
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (y,data) {

     return data.formatted; 
 }

    }); 
    
 donut.select(0);


    var data = [];

    

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };
    });
</script>
<script>
$(function(){

    var draftParsv =-1;
    var approvedPars = 75;
    
    var inreviewPars =25;
  var donut = Morris.Donut({
    element:'graph-donut_three',
    data: [

        {value: draftParsv, label:'Total Service Category', formatted: '27' },
        {value: approvedPars, label: 'New Service Category', formatted: '14' },
        {value: inreviewPars, label: 'Total Service Category', formatted: '27' }
        
        
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (y,data) {

     return data.formatted; 
 }

    }); 
    
 donut.select(0);


    var data = [];

    

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };
 
});
$("#graph-donut_three").mouseout(function(){
   var draftParsv =-1;
    var approvedPars =75;
    
    var inreviewPars =25;
  var donut = Morris.Donut({
    element:'graph-donut_three',
    data: [

        {value: draftParsv, label:'Total Service  Category', formatted: '27' },
        {value: approvedPars, label: 'New Service  Category', formatted: '14' },
        {value: inreviewPars, label: 'Total Service  Category', formatted: '27' }
        
        
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (y,data) {

     return data.formatted; 
 }

    }); 
    
 donut.select(0);


    var data = [];

    

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };
    });
</script>
<script>
$(function(){

    var draftParsv =-1;
    var approvedPars =75;
    
    var inreviewPars =25;
  var donut = Morris.Donut({
    element:'graph-donut_four',
    data: [

        {value: draftParsv, label:'Total Service Post', formatted: '124' },
        {value: approvedPars, label: 'New Service Post', formatted: '14' },
        {value: inreviewPars, label: 'Total Service Post', formatted: '124' }
        
        
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (y,data) {

     return data.formatted; 
 }

    }); 
    
 donut.select(0);


    var data = [];

    

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };
 
});
$("#graph-donut_three").mouseout(function(){
   var draftParsv =-1;
    var approvedPars =75;
    
    var inreviewPars =25;
  var donut = Morris.Donut({
    element:'graph-donut_four',
    data: [

        {value: draftParsv, label:'Total Service Post', formatted: '124' },
        {value: approvedPars, label: 'New Service Post', formatted: '14' },
        {value: inreviewPars, label: 'Total Service Post', formatted: '124' }
        
        
    ],
    backgroundColor: false,
    labelColor: '#fff',
    colors: [
        '#4acacb','#6a8bc0','#5ab6df','#fe8676'
    ],
    formatter: function (y,data) {

     return data.formatted; 
 }

    }); 
    
 donut.select(0);


    var data = [];

    

    var options = {
        grid: {
            backgroundColor:
            {
                colors: ["#ffffff", "#f4f4f6"]
            },
            hoverable: true,
            clickable: true,
            tickColor: "#eeeeee",
            borderWidth: 1,
            borderColor: "#eeeeee"
        },
        
        tooltip: true,
        tooltipOpts: {
            content: "%s X: %x Y: %y",
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        },
        legend: {
            labelBoxBorderColor: "#000000",
            container: $("#main-chart-legend"), //remove to show in the chart
            noColumns: 0
        },
        series: {
            stack: true,
            shadowSize: 0,
            highlightColor: 'rgba(000,000,000,.2)'
        },
        
        points: {
            show: true,
            radius: 3,
            symbol: "circle"
        },
        colors: ["#5abcdf", "#ff8673"]
    };
    });
</script>