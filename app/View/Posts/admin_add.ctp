<div class="emailTemplates form">
<?php echo $this->Form->create('Post',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Add Service'); ?></legend>
	<?php
		echo $this->Form->input('cat_id',array('empty' => '(choose any category)','options' => $categories,'class'=>'selectbox2','label'=>'Category'));
                echo $this->Form->input('title',array('required' => 'required'));
                echo $this->Form->input('image',array('type'=>'file'));
                echo $this->Form->input('icon',array('type'=>'file'));
               
           
                //echo $this->Form->input('user_id',array('empty' => '(choose any category)','options' => $users,'class'=>'selectbox2'));
                echo $this->Form->input('post_order', array('default' => 0));
                 echo $this->Form->label('Feature Service');
                
                echo $this->Form->checkbox('is_feature');
                 echo '<br>';
                echo $this->Form->label('Popular Service');
                 
                echo $this->Form->checkbox('is_popular');

	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

