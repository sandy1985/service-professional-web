<?php echo '<pre>'; //print_r($messages); echo '</pre>'; ?>

<div class="categories index">
	<h2><?php echo __(' List Conversation'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
		
            <th><?php echo $this->Paginator->sort('From'); ?></th>
            <th><?php echo $this->Paginator->sort('To'); ?></th>
            <th>Message</th>
            <th>Date</th>

	</tr>
	<?php
        $d=1;
        foreach ($messages as $post):
        ?>
	<tr>
		<td><?php echo $d; ?>&nbsp;</td>
		
                <td><?php echo h($post['FromUser']['name']);?><br>
                <?php if($post['FromUser']['type']=='U') { echo "(Customer)"; }else{ echo "(Provider)";}?>
                </td>
                <td><?php echo h($post['ToUser']['name']);?><br>
                <?php if($post['ToUser']['type']=='U') { echo "(Customer)"; }else{ echo "(Provider)";}?></td>
       <td><?php echo h($post['Message']['msg']);?></td>
        <td><?php echo h($post['Message']['dt_pub_date']);?></td>
	</tr>
<?php
$d++;
endforeach;

?>
	</table>
<!--	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>-->
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>