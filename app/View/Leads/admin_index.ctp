<?php //echo '<pre>'; print_r($leads); echo '</pre>'; ?>
<div class="categories index">
	<h2><?php echo __('Lead Generated'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('Customer Name'); ?></th>
			<th><?php echo $this->Paginator->sort('Service Name'); ?></th>
            <th><?php echo $this->Paginator->sort('Address'); ?></th>
            <th><?php echo $this->Paginator->sort('State'); ?></th>
            <th><?php echo $this->Paginator->sort('Start Date'); ?></th>
            <th><?php echo $this->Paginator->sort('End Date'); ?></th>
            <th><?php echo $this->Paginator->sort('Status'); ?></th>
            <th><?php echo $this->Paginator->sort('Action'); ?></th>
			
	</tr>
	<?php foreach ($leads as $lead): ?>
	<tr>
		<td><?php echo h($lead['Lead']['id']); ?>&nbsp;</td>
		<td><?php echo h($lead['User']['name']);?></td>
		<td><?php echo ($lead['Post']['title']);?></td>
        <td><?php echo ($lead['Lead']['address']);?></td>
        <td><?php echo ($lead['Lead']['state']);?></td>
        <td><?php echo date('d M Y', strtotime($lead['Lead']['start_date']));?></td>
        <td><?php echo date('d M Y', strtotime($lead['Lead']['end_date']));?></td>
        <td><?php if($lead['Lead']['status']==0) { echo 'Pending'; }else { echo 'Accept'; } ?></td>
        <td>
                <a href="<?php echo $this->webroot?>admin/leads/message/<?php echo $lead['Lead']['id']?>" class="btn btn-primary">Conversation</a>
                <a href="<?php echo $this->webroot?>admin/leads/response/<?php echo $lead['Lead']['id']?>" class="cmn btn btn-primary">questionnaire</a>&nbsp;&nbsp;
		</td>
	</tr>
       <?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<style type="text/css">
	.cmn{
		margin-top: 5px;
	}
</style>
<?php //echo $this->element('admin_sidebar'); ?>