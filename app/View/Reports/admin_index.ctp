<?php //echo '<pre>'; print_r($abuses); echo '</pre>'; ?>
<div class="categories index">
	<h2><?php echo __('Abuse List For Customer'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('Abuse From Customer'); ?></th>
			<th><?php echo $this->Paginator->sort('Abuse To Provider'); ?></th>
            <th><?php echo $this->Paginator->sort('Abuse Details'); ?></th>
            <th><?php echo $this->Paginator->sort('Date'); ?></th>
            <th><?php echo $this->Paginator->sort('Action'); ?></th>
			
	</tr>
	<?php foreach ($abuses as $abuse): ?>
	<tr>
		<td><?php echo h($abuse['ReportAbuseCustomer']['id']); ?>&nbsp;</td>
		<td><?php echo h($abuse['User']['name']);?></td>
		<td><?php echo ($abuse['User1']['name']);?></td>
        <td><?php echo ($abuse['ReportAbuseCustomer']['abuse_message']);?></td>
        <td><?php echo date('d M Y', strtotime($abuse['ReportAbuseCustomer']['date']));?></td>
        <td>
            <a href="<?php echo $this->webroot?>admin/reports/provider_list/<?php echo $abuse['User1']['id']?>" class="btn btn-primary">View Provider</a>&nbsp;&nbsp;
		</td>
	</tr>
       <?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>