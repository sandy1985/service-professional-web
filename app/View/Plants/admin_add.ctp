<?php ?>
<script>
    // $(document).ready(function () {
    //     $("#BlogAdminAddForm").validationEngine();
    // });
</script>
<style>
.error-modal {
    background: red;
 }
</style>
<div class="blogs form">
<?php echo $this->Form->create('Plant', array('enctype' => 'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Add Plan'); ?></legend>
        
        <?php
        echo $this->Form->input('name',array('required'=>'required'));?>
        <?php
        echo $this->Form->input('description',array('label' => 'Description', 'id' => 'banner_desc'));
        //echo $this->Form->input('status');?>
        <?php
        echo $this->Form->input('duration', array('label' => 'Duration','style' => 'width:50%','default' => 0,'id'=>'duration'));
        ?>
       
         <?php
        //echo $this->Form->input('start', array('label' => 'Purchase From','id' => 'start_from','class'=>'datepicker'));
        ?>


         <?php
        //echo $this->Form->input('end', array('label' => 'Purchase To','id' => 'start_to','class'=>'datepicker'));
        ?>
        <?php
        echo $this->Form->input('request', array('label' => 'Request','style' => 'width:50%','default' => 0));
        ?>
        <?php
        echo $this->Form->input('job', array('label' => 'Job posted','style' => 'width:50%','default' => 0));
        ?>

        <?php
         echo $this->Form->input('price',array('id'=>'price','required'=>'required'));
         ?>
        <?php
        //echo $this->Form->input('image', array('type' => 'file'));?>
       <!--  <div class="input file" >
            <span style="color:red;font-size:12px;">* Image Type Should be .JPG,.JPEG,.PNG,.GIF.</span><br>
            <span style="color:red;font-size:12px;">**The Image Resolution Should be 1366x554.</span>
        </div> -->
	
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="modal fade" id="alertModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header error-modal">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Image Verification Error</h4>
        </div>
        <div class="modal-body">
          <p id="alertMessage"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo $this->webroot;?>ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot;?>ckfinder/ckfinder_v1.js"></script>
<script type="text/javascript">
     CKEDITOR.replace('banner_desc',
            {
                width: "95%"
            });

var _URL = window.URL || window.webkitURL;    

function isSupportedBrowser() {
    return window.File && window.FileReader && window.FileList && window.Image;
}


function getSelectedFile() {
    var fileInput = document.getElementById("PlantImageNew");
    var fileIsSelected = fileInput && fileInput.files && fileInput.files[0];
    if (fileIsSelected)
        return fileInput.files[0];
    else
        return false;
}

function isGoodImage(file) {
    var deferred = jQuery.Deferred();
    var image = new Image();
    
    image.onload = function() {
        // Check if image is bad/invalid
        if (this.width + this.height === 0) {
            this.onerror();
            return;
        }
        
        // Check the image resolution
        if (this.width == 1366 && this.height == 554) {
            deferred.resolve(true);
        } else {
            //alert("The Image Resolution Should be 1366x554.");
            $('#alertMessage').text("The Image Resolution Should be 1366x554.");
            deferred.resolve(false);
        }
    };
    
    image.onerror = function() {
        //alert("Invalid image. Please select an image file.");
        $('#alertMessage').text("Invalid image. Please select an image file.");
        deferred.resolve(false);
    }
    
    image.src = _URL.createObjectURL(file);
    
    return deferred.promise();
}

$("#start_from").onclick(function(event) {
    alert('hii');
}
$("#PlantImageNew").change(function(event) {
    var form = this;
    
    if (isSupportedBrowser()) {
        event.preventDefault(); //Stop the submit for now

        var file = getSelectedFile();
        if (!file) {
            alert("Please select an image file.");
            return;
        }
        
        isGoodImage(file).then(function(isGood) {
            if (!isGood)
                //form.submit();
            $("#PlantImageNew").val('');
            $('#alertModal').modal('show');
            return;
        });
    }
});

// function validateImageBeforeUpload(el) {
//     var types = ['image/jpeg', 'image/gif', 'image/png']
//     var imageFile = el;
    
//     if(types.indexOf(imageFile.files[0].type) != -1) {
//         var fileSize = imageFile.files[0].size;
//         fileSize = fileSize / (1024*1024);
//         if (fileSize > 2) {
//             imageFile.value = '';
//             alert('Maximum filesize is 2MB');
//         }
//     } else {
//         imageFile.value = '';
//         alert('Invalid image type');
//     }
// }
    
</script>
<link href="<?php echo $this->webroot; ?>adminFiles/css/jquery-ui-timepicker-addon.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript">
    $(document).ready(function(){
   $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });
    });
    </script>
    
    

