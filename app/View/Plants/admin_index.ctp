<style>
.success-modal {
    background: green;
 }
</style>
<div class="faqs index">
    <h2><?php echo __('Plan List'); ?></h2>
    <table style="width:100%;border:0px solid red;">
        <tr>
            <td style="width:70%;border:0px solid red;">&nbsp;</td>
            <td style="border:0px;"><a href="<?php echo($this->webroot);?>admin/Plants/add" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New Plan</a>  
        </tr></td>
        </tr>
       
        <table cellpadding="0" cellspacing="0">
            <tr>
                
                <th><?php echo $this->Paginator->sort('id'); ?></th>
                <!-- <th><?php echo $this->Paginator->sort('image'); ?></th> -->
                <th><?php echo $this->Paginator->sort('Name'); ?></th>
                <th><?php echo $this->Paginator->sort('Description'); ?></th>
                 <th><?php echo $this->Paginator->sort('Duration'); ?></th> 
                 <th><?php echo $this->Paginator->sort('Job posting'); ?></th>
                 <th><?php echo $this->Paginator->sort('Request'); ?></th>
                <th><?php echo $this->Paginator->sort('Price'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
	<?php
        if(!empty($Plants)) :
            foreach ($Plants as $key => $Plant) :
            //print_r($Plant);
        ?>
            <tr>
                 
                <td width="4%">
                    <?php
                    echo ++$key;
                    ?>
                </td>
                <!-- <td width="11%">
                    <?php
                    $uploadFolder = "Banner/";
                    $uploadPath = $this->webroot . $uploadFolder;
                    ?>
                    <img src="/team4/Zoebiz/banner/137211232_sap-banner.jpg" width="100" />
                </td> -->
                <td width="20%">
                    <?php
                    echo h($Plant['Plant']['name']);
                    ?>
                </td>
                <td width="30%">
                    <?php
                    echo strip_tags($Plant['Plant']['description']);
                    ?>
                </td>
                <!-- <td>
                    <?php
                    //echo ($Plant['Plant']['status'] == 1) ? 'Active' : 'Deactive';
                    ?>
                </td> -->
                <td>
                <?php
                    echo h($Plant['Plant']['duration']).'months';
                    ?>
                </td>
                <td>
                <?php
                    echo h($Plant['Plant']['request']);
                    ?>
                </td>
                <td>
                <?php
                    echo h($Plant['Plant']['job']);
                    ?>
                </td>
                <td>
                    <?php
                    echo '$'.h($Plant['Plant']['price']);
                    ?>
                </td>
                <td >
                            <?php //echo $this->Html->link(__('View'), array('action' => 'view', $Plant['Plant']['id'])); ?>
                            <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')),
                        array('action' => 'edit', $Plant['Plant']['id']),
                        array('class' => 'btn btn-info btn-xs', 'escape'=>false)); ?>
                            
                            <?php echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'fa fa-times')),
                        array('action' => 'delete', $Plant['Plant']['id']),
                        array('class' => 'btn btn-danger btn-xs', 'escape'=>false),
                        __('Are you sure you want to delete # %s?', $key)); ?>
                </td>
            </tr>
        <?php
            endforeach;
        else :
        ?>
            <tr>
                <td colspan="7"><?php echo __('No Plant found'); ?></td>
            </tr>
        <?php
        endif;
        ?>
        </table>
        <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
        <div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
        </div>
</div>

<div class="modal fade" id="alertModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header success-modal">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Set Plant Image</h4>
        </div>
        <div class="modal-body">
          <p id="ajaxResponse"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
<?php //echo($this->element('admin_sidebar'));?>
<script>
$(document).ready(function() {
    $(".Plant-class").click(function(){
        b_id = $(this).val();
        $.ajax({
                url: "<?php echo $this->webroot;?>Plants/ajaxPlantSelect",
                data:{Plant_id:b_id},
                dataType:'json',
                type: 'POST',     
                success: function(result){
                    //console.log(result);
                    if(result.Ack==1){
                        $('#ajaxResponse').text(result.res);
                        $('#alertModal').modal('show');
                    }
                    else{
                        $('#ajaxResponse').text(result.res);
                        $('#alertModal').modal('show');
                    }
                }
        });
    });
});


</script>