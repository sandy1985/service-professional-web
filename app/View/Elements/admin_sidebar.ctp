<?php
    $uploadLogoFolder = "site_logo";
    $uploadLogoPath = WWW_ROOT . $uploadLogoFolder;
    $LogoName = $sitesetting['Setting']['logo'];
    if(file_exists($uploadLogoPath . '/' . $LogoName) && $LogoName!=''){
        $LogoLink=$this->webroot.'site_logo/'.$LogoName;        
    }else{
        $LogoLink=$this->webroot.'adminFiles/images/logo.png';  
    }
?>

<!-- left side start-->
	        <div class="left-side sticky-left-side">

		<!--logo and iconic logo start-->
		<div class="logo">
        <a href="javascript:void(0);"><img src="<?php echo $LogoLink;?>" alt="" style="height: 41px;"></a>
    </div>

    <div class="logo-icon text-center">
        <a href="javascript:void(0);"><img src="<?php echo $LogoLink;?>" style="height: 60px;" alt=""></a>
    </div>
		<!--logo and iconic logo end-->

		<div class="left-side-inner">

		    <!-- visible to small devices only -->
		    <div class="visible-xs hidden-sm hidden-md hidden-lg">
		        <div class="media logged-user">
		            <div class="media-body">
		                <h4><a href="<?php echo $this->webroot?>admin/users/edit/<?php echo $userdetails['User']['id']; ?>"><?php echo($userdetails['User']['name'])?></a></h4>
		            </div>
		        </div>

		        <h5 class="left-nav-title">Account Information</h5>
		        <ul class="nav nav-pills nav-stacked custom-nav">
		          <li><a href="<?php echo $this->webroot?>admin/users/edit/<?php echo $userdetails['User']['id']; ?>"><i class="fa fa-user"></i> <span>Profile</span></a></li>
			  <li><a href="<?php echo $this->webroot?>admin/users/changepwd"><i class="fa fa-user"></i> <span>Manage Password</span></a></li>
		          <li><a href="<?php echo $this->webroot?>admin/users/logout"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
		        </ul>
		    </div>

		    <!--sidebar nav start-->
		    <ul class="nav nav-pills nav-stacked custom-nav">
<!--		<li class="menu-list"><a href=""><i class="fa fa-cog"></i> <span>Setting</span></a>
	           <ul class="sub-menu-list">
                     <li><a href="<?php echo $this->webroot?>admin/settings/edit/1">Side Settings</a></li>

		   </ul>
		</li>-->


            <li class="<?php echo ($this->params['controller'] == 'users' && $this->params['action'] == 'dashboard') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/users/dashboard"><i class="fa fa-tachometer"></i> <span>Dashboard</span></a></li>


            <li class="<?php echo ($this->params['controller'] == 'settings' && $this->params['action'] == 'admin_sitelogo') ? 'active': ''; ?>">
                <a href="<?php echo $this->webroot?>admin/settings/sitelogo/1"><i class="fa fa-upload"></i> <span>Manage Logo</span></a>
            </li>
            
            <li>
                <a href="<?php echo $this->webroot?>admin/settings/edit/1"><i class="fa fa-cog"></i> <span>Manage Settings</span></a>
            </li>

            <li>
                <a href="<?php echo $this->webroot?>admin/banners"><i class="fa fa-picture-o"></i> <span>Manage Banners</span></a>
            </li>
	     <li class="menu-list <?php echo ($this->params['controller'] == 'blockips') ? 'active': ''; ?>"><a href=""><i class="fa fa-tags"></i> <span>Manage Ip Address</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?php echo ($this->params['controller'] == 'blockips' && $this->params['action'] == 'index') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/blockips/index">List Ip Address</a></li>
                        <li class="<?php echo ($this->params['controller'] == 'blockips' && $this->params['action'] == 'add') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/blockips/add">Add Ip Address</a></li>
                    </ul>
            </li>
            <li class="menu-list <?php echo ($this->params['controller'] == 'roles' || $this->params['controller'] == 'roles_accesses') ? 'nav-active': ''; ?>">
		<?php 
		
		?>
                <a href=""><i class="fa fa-user"></i> <span>Member Access</span></a>
                <ul class="sub-menu-list">
                    <!--<li class="<?php //echo ($this->params['controller'] == 'roles_accesses' && $this->params['action'] == 'admin_index') ? 'active' : ''; ?>">
                        <a href="<?php //echo $this->webroot; ?>admin/roles_accesses/index">List Member Access</a>
                    </li>
                    <li class="<?php //echo ($this->params['controller'] == 'roles_accesses' && $this->params['action'] == 'admin_add') ? 'active' : ''; ?>">
                        <a href="<?php //echo $this->webroot; ?>admin/roles_accesses/add">Add Member Access</a>
                    </li>-->
                    <li class="<?php echo ($this->params['controller'] == 'roles' && $this->params['action'] == 'admin_group_members') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot?>admin/roles/group_members">Role Members</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'roles' && $this->params['action'] == 'admin_group_n_permission') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot?>admin/roles/group_n_permission">Role &amp; Permissions</a>
                    </li>
                    <li class="<?php echo ($this->params['controller'] == 'roles' && $this->params['action'] == 'admin_manage_admins') ? 'active' : ''; ?>">
                        <a href="<?php echo $this->webroot?>admin/roles/manage_admins">Add Sub Admin</a>
                    </li>
                </ul>
            </li>
            <li class="menu-list <?php echo ($this->params['controller'] == 'users' && $this->params['action'] != 'dashboard') ? 'active': ''; ?>"><a href=""><i class="fa fa-users"></i> <span>Users</span></a>
                <ul class="sub-menu-list">
                <li class="<?php echo ($this->params['controller'] == 'users' && $this->params['action'] == 'list_service_provider') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/users/list_service_provider">List Service Provider</a></li>
                <li class="<?php echo ($this->params['controller'] == 'users' && $this->params['action'] == 'add_service_provider') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/users/add_service_provider">Add Service Provider</a></li>
                <li class="<?php echo ($this->params['controller'] == 'users' && $this->params['action'] == 'list') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/users/list">List User</a></li>
                <li class="<?php echo ($this->params['controller'] == 'users' && $this->params['action'] == 'add') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/users/add">Add USer</a></li></ul>

            </li>

             <li class="menu-list <?php echo ($this->params['controller'] == 'Bookings') ? 'active': ''; ?>"><a href=""><i class="fa fa-book"></i> <span>Booking</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?php echo ($this->params['controller'] == 'Bookings' && $this->params['action'] == 'index') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/Bookings/index">List Booking</a></li>

                    </ul>
            </li>


            <li class="menu-list <?php echo ($this->params['controller'] == 'categories') ? 'active': ''; ?>"><a href=""><i class="fa fa-tags"></i> <span>Service Category</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?php echo ($this->params['controller'] == 'categories' && $this->params['action'] == 'index') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/categories/index">List Service Category</a></li>
                        <li class="<?php echo ($this->params['controller'] == 'categories' && $this->params['action'] == 'add') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/categories/add">Add Service Category</a></li>
                    </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'posts') ? 'active': ''; ?>"><a href=""><i class="fa fa-tasks"></i> <span>Service Post</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'index') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/posts/index">List Service Post</a></li>
                    <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'add') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/posts/add">Add Service Post</a></li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'leads') ? 'active': ''; ?>"><a href=""><i class="fa fa-exchange"></i> <span>Leads</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'posts' && $this->params['action'] == 'index') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/leads/index">Leads List</a></li>
                </ul>
            </li>

            <li class="menu-list <?php echo ($this->params['controller'] == 'reports') ? 'active': ''; ?>"><a href=""><i class="fa fa-plus"></i> <span>Report Abuse</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'reports' && $this->params['action'] == 'index') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/reports/index">Customer Abuse List</a></li>
                    <li class="<?php echo ($this->params['controller'] == 'reports' && $this->params['action'] == 'abuse_provider_list') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/reports/abuse_provider_list">Provider Abuse List</a></li>
                </ul>
            </li>


            <li class="menu-list <?php echo ($this->params['controller'] == 'faqs') ? 'active': ''; ?>"><a href=""><i class="fa fa-question"></i> <span>FAQ</span></a>
                    <ul class="sub-menu-list">
                        <li class="<?php echo ($this->params['controller'] == 'faqs' && $this->params['action'] == 'index') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/faqs/index">List FAQ</a></li>
                        <li class="<?php echo ($this->params['controller'] == 'faqs' && $this->params['action'] == 'add') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/faqs/add">Add FAQ</a></li>

                    </ul>
            </li>


            <li class="menu-list <?php echo ($this->params['controller'] == 'email_templates') ? 'active': ''; ?>"><a href=""><i class="fa fa-envelope"></i> <span>Email Template</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'email_templates' && $this->params['action'] == 'index') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/email_templates/index">List Email Template</a></li>
                    <li class="<?php echo ($this->params['controller'] == 'email_templates' && $this->params['action'] == 'add') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/email_templates/add">Add Email Template</a></li>

                </ul>
            </li>


            <li class="menu-list <?php echo ($this->params['controller'] == 'cms_pages') ? 'active': ''; ?>"><a href=""><i class="fa fa-file-text"></i> <span>Contents</span></a>
                    <ul class="sub-menu-list">
                     <li class="<?php echo ($this->params['controller'] == 'cms_pages' && $this->params['action'] == 'index') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/cms_pages/index"> List Contents</a></li>
                     <li class="<?php echo ($this->params['controller'] == 'cms_pages' && $this->params['action'] == 'add') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/cms_pages/add"> Add Contents</a></li>
                    </ul>
             </li>



            
            <li class="menu-list <?php echo ($this->params['controller'] == 'plants') ? 'active': ''; ?>"><a href=""><i class="fa fa-building-o"></i> <span>Membership Plans</span></a>
                <ul class="sub-menu-list">
                    <li class="<?php echo ($this->params['controller'] == 'plants' && $this->params['action'] == 'index') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/plants/index">List Plans</a></li>
                    <li class="<?php echo ($this->params['controller'] == 'plants' && $this->params['action'] == 'add') ? 'active': ''; ?>"><a href="<?php echo $this->webroot?>admin/plants/add">Add Plans</a></li>
                </ul>
            </li>
            
           
		    </ul>
		    <!--sidebar nav end-->

		</div>
	    </div>
	    <!-- left side end-->
