<div class="contents view">
<!--<h2><?php echo __('Booking Deatails'); ?></h2>-->
	<dl>
            <h5 style="font-weight: bold; font-size: 18px;"><?php echo __('User Details'); ?></h5>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($books['User']['name']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($books['User']['email']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Mobile Number'); ?></dt>
		<dd>
			<?php echo h($books['User']['mobile_no']); ?>
			&nbsp;
		</dd>

                <h5 style="font-weight: bold; font-size: 18px; margin-top: 30px;"><?php echo __('Service Provider Details'); ?></h5>

		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($books['User1']['name']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($books['User1']['email']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Mobile Number'); ?></dt>
		<dd>
			<?php echo h($books['User1']['mobile_no']); ?>
			&nbsp;
		</dd>

                <h5 style="font-weight: bold; font-size: 18px; margin-top: 30px;"><?php echo __('Booking Details'); ?></h5>

                <dt><?php echo __('Category Name'); ?></dt>
		<dd>
			<?php echo $books['Category']['name']; ?>
			&nbsp;
		</dd>

                 <dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo $books['Booking']['amount']." "."$"; ?>
			&nbsp;
		</dd>

                <dt><?php echo __('Start Date'); ?></dt>
		<dd>
			<?php echo date('d M Y', strtotime($books['Booking']['start_date'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End Date'); ?></dt>
		<dd>
			<?php echo date('d M Y', strtotime($books['Booking']['start_date'])); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($books['Booking']['address']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($books['Booking']['state']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Zip'); ?></dt>
		<dd>
			<?php echo h($books['Booking']['zip']); ?>
			&nbsp;
		</dd>
                <dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php
                        if($books['Booking']['status']=="A"){

                            echo "Accept";

                        }else{

                            echo "Reject";

                        }

                        ?>
			&nbsp;
		</dd>

	</dl>
</div>
<?php //echo $this->element('admin_sidebar'); ?>