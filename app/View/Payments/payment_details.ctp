<?php
//pr($details);

	
?>
<section class="category-body-area">
    	<div class="container">
        	<div class="row">            	
				<?php
					echo $this->element('user_sidebar');
				?>				
                 
				<div class="col-md-9">
                	<div class="right-dashboard">
                		<h3>Booking Details</h3>
                		<div class="table-responsive">
                			<table class="table  table-striped book-table">
							<?php
								if(count($manicure)>0){
									
							?>
                				<tr>
									<td width="30%">Manicure Name: </td>
                                    <td width="70%"><?php echo $manicure['StyleService']['name'];?></td>
								</tr>                                
                                <tr>
									<td width="30%">Manicure Image: </td>
                                    <td width="70%"><img src="<?php echo $this->webroot.'style_images/'.$manicure['StyleService']['image'];?>" alt="" width="150"></td>
								</tr>
								<tr><td colspan="2">&nbsp;</td></tr>
								<?php
								}
								?>
								<?php
								if(count($pedicure)>0){
									
							?>
                				<tr>
									<td width="30%">Pedicure Name: </td>
                                    <td width="70%"><?php echo $pedicure['StyleService']['name'];?></td>
								</tr>                                
                                <tr>
									<td width="30%">Pedicure Image: </td>
                                    <td width="70%"><img src="<?php echo $this->webroot.'style_images/'.$pedicure['StyleService']['image'];?>" alt="" width="150"></td>
								</tr>
								<tr><td colspan="2">&nbsp;</td></tr>
								<?php
								}
								?>
								
                                <tr>
									<td width="30%">Treatment Name: </td>
                                    <td width="70%"><?php echo $details['Treatment']['name'];?></td>
								</tr>                                
                                <tr>
									<td width="30%">Treatment Image: </td>
                                    <td width="70%"><img src="<?php echo $this->webroot.'treatment/'.$details['Treatment']['image'];?>" alt="" width="150"></td>
								</tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                 <tr>
									<td width="30%">Booking Price: </td>
                                    <td width="70%">$<?php echo $details['Booking']['booking_price'];?></td>
								</tr>                                
                                 <tr>
									<td width="30%">Booking Date: </td>
                                    <td width="70%"><?php echo $details['Booking']['booking_date'];?></td>
								</tr>                               
                                <tr>
									<td width="30%">Booking Time: </td>
                                    <td width="70%"><?php echo date('g:i A', strtotime($details['Booking']['booking_time']));?></td>
								</tr>                                
                               
                                <tr><td colspan="2">&nbsp;</td></tr>
                                <tr>
									<td width="30%">Address: </td>
                                    <td width="70%"><?php echo $details['UserAddresse']['address'];?>, <?php echo $details['UserAddresse']['city'];?>, <?php echo $details['UserAddresse']['state'];?>, <?php echo $details['UserAddresse']['zip'];?></td>
								</tr>
                                <tr><td colspan="2">&nbsp;</td></tr>
                                
                			</table>
							
                		</div>
						<p>
						
						</div>
                	</div>
                </div>
            </div>
        </div>
    </section>