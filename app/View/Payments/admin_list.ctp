<?php
	//pr($Payments);
?>
<div class="faqs index">
	<h2><?php echo __('Booking'); ?></h2>
	<table style="width:100%;border:0px solid red;">
	
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th width="5%"><?php echo $this->Paginator->sort('id'); ?></th>
			<th width="30%"><?php echo __('Date'); ?></th>
            <th width="30%"><?php echo __('Stylist price'); ?></th>
            <th width="10%"><?php echo __('Admin price'); ?></th>
            <th width="10%"><?php echo __('Total price'); ?></th>
			<th class="actions" width="10%"><?php echo __('Actions'); ?></th>
	</tr>
	<?php 
        $ServiceCnt=0;
        foreach ($Payments as $Payment): 
            $ServiceCnt++;?>
	<tr>
		<td width="5%" style="text-align:left"><?php echo $ServiceCnt;?>&nbsp;</td>
		<td width="30%" style="text-align:left"><?php echo date("j M, Y, g:i a", strtotime($Payment['Payment']['payment_date']));?>&nbsp;</td>
        <td width="30%" style="text-align:left"><?php echo h($Payment['Payment']['stylist_price']); ?></td>
        <td width="10%" style="text-align:left"><?php echo h($Payment['Payment']['admin_price']); ?></td> 
        <td width="10%" style="text-align:left"><?php echo h($Payment['Payment']['total_price']); ?>&nbsp;</td>     
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'payment_details', $Payment['Payment']['id'])); ?>			
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo($this->element('admin_sidebar'));?>