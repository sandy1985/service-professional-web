<div class="contents index">
	<h2 style="width:400px;float:left;"><?php echo __('Contents'); ?></h2>
<!--	<a href="<?php echo $this->webroot.'admin/cms_page/add'; ?>" style="float:right">Content Add</a>-->
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('page_name'); ?></th>
			<th><?php echo $this->Paginator->sort('Description'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>

	<?php

         $d=0;
        foreach ($contents as $content):

            $d++;
       ?>

	<tr>
        <td><?php echo $d; ?>&nbsp;</td>
        <td style="text-align:left"><?php echo h($content['CmsPage']['page_title']);?></td>
        <td style="text-align:left"><?php echo $content['CmsPage']['page_description'];?></td>
        <td>

         <a href="<?php echo $this->webroot?>admin/cms_pages/view/<?php echo $content['CmsPage']['id']?>" class="btn btn-primary">View</a>&nbsp;&nbsp;

         <a href="<?php echo $this->webroot?>admin/cms_pages/edit/<?php echo $content['CmsPage']['id']?>" class="btn btn-success">Edit</a>&nbsp;&nbsp;

         <a href="<?php echo $this->webroot?>admin/cms_pages/delete/<?php echo $content['CmsPage']['id']?>" onclick="return confirm('are you sure to delete this content?');" class="btn btn-danger">Delete</a>&nbsp;&nbsp;
        </td>
	</tr>
<?php

endforeach;
$d++;
?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php //echo $this->element('admin_sidebar'); ?>