<?php

App::uses('AppModel', 'Model');

/**
 * Question Model
 *
 * @property User $User
 * @property BlogComment $BlogComment
 */
class Question extends AppModel {

    public $belongsTo = array(

       'Post'=>array(
       'className'=>'Post',
       'foreignKey'=>'service_id'
       )


  );
}
